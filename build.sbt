import sbt._
import sbt.Keys._
import sbt.Resolver
import Dependencies._
import CmsRelease.{ settings => releaseSettings }
import de.heikoseeberger.sbtheader.license.Apache2_0
import de.heikoseeberger.sbtheader.HeaderPattern
import de.heikoseeberger.sbtheader.HeaderKey.createHeaders

lazy val commonSettings = releaseSettings ++ Seq(
  organization := "ch.insign",

  scalaVersion := "2.11.8",

  resolvers += Resolver.sonatypeRepo("releases"),
  resolvers += Resolver.bintrayRepo("insign", "play-cms"),

  routesGenerator := InjectedRoutesGenerator,

  // Prevent "play test" to fork in a new Process. With this it's possible to debug tests
  fork in Test := false,

  // Change max-filename-length for scala-Compiler. Longer filenames (not sure what the threshold is) causes problems
  // with encrypted home directories under ubuntu
  scalacOptions ++= Seq("-Xmax-classfile-name", "100"),

  licenses += ("Apache-2.0", url("https://www.apache.org/licenses/LICENSE-2.0.html")),
  bintrayRepository := "play-cms",
  bintrayOrganization := Some("insign"),
  publishMavenStyle := true,
  headers := Map(
    "java"  -> Apache2_0("2017", "insign gmbh"),
    "scala" -> Apache2_0("2017", "insign gmbh")
  ),
  excludeFilter.in(unmanagedSources.in(createHeaders)) := HiddenFileFilter || "*parola*"
)

lazy val lessSettings = Seq(
  LessKeys.rootpath := "/",
  LessKeys.relativeUrls := true,
  LessKeys.compress in Assets := true
)


lazy val cms = (project in file("."))
  .enablePlugins(PlayJava)
  .enablePlugins(BuildInfoPlugin)
  .enablePlugins(GitVersioning)
  .dependsOn(commons, auth, metronic)
  .aggregate(commons, auth, metronic)
  .settings(commonSettings: _*)
  .settings(lessSettings: _*)
  .settings(
    name := "play-cms",
    libraryDependencies ++= cmsDeps,

    buildInfoKeys := Seq[BuildInfoKey](name, version, scalaVersion, sbtVersion),
    buildInfoPackage := "ch.insign.cms"
)

lazy val commons = (project in file("modules/play-commons"))
  .enablePlugins(PlayJava)
  .enablePlugins(GitVersioning)
  .settings(commonSettings: _*)
  .settings(
    name := "play-commons",
    libraryDependencies ++= commonsDeps
  )

lazy val auth = (project in file("modules/play-auth"))
  .enablePlugins(PlayJava)
  .enablePlugins(GitVersioning)
  .settings(commonSettings: _*)
  .settings(
    name := "play-auth",
    libraryDependencies ++= authDeps
  )

lazy val metronic = (project in file("modules/play-theme-metronic"))
  .enablePlugins(PlayJava, SbtWeb)
  .enablePlugins(GitVersioning)
  .settings(commonSettings: _*)
  .settings(lessSettings: _*)
  .settings(
    name := "play-theme-metronic",
    libraryDependencies ++= metronicDeps
  )
