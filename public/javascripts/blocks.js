jQuery(function($) {
    // show modal on delete page click
    $('.deleteBlock').click(function(){
        var $form;

        if ($(this).data('form-id')) {
            $form = $("#" + $(this).data('form-id'))
        } else {
            $form = $(this).parents('form');
        }

        var blockName = $(this).data('deleted-block-name');
        $('#confirm-delete-block').data('deleteForm', $form.attr('id'));
        $('#confirm-delete-block').modal('show');
        $('#deletedBlockName').html(blockName);
        $('#inboundLinks').load('/admin/cms/block/inboundLinks?id='+$form.attr('id').substr('delete-block-form-'.length));
        return false;
    });

    // submits form after confirmation clicked
    $('#confirmDelete').click(function(e){
        e.preventDefault();
        var formId = $('#confirm-delete-block').data('deleteForm');
        $('#' + formId).submit();
    });

    // bind primary button click handler when restrict/unrestrict confirm dialog is shown
    $('#confirm-restrict-block, #confirm-unrestrict-block').on('show.bs.modal', function(e) {
        var $this = $(this),
            $btn = $(e.relatedTarget),
            url = $btn.data('actionUrl');

        $('.block-name', $this).html($btn.data('blockName'));

        $this.on('click.restriction', '.btn-primary', function(e) {
            e.preventDefault();
            window.location.href = url;
        });
    });

    // unbind primary button click handler when restrict/unrestrict confirm dialog is hidden
    $('#confirm-restrict-block, #confirm-unrestrict-block').on('hide.bs.modal', function(e) {
        $(this).off('.restriction');
    });
});
