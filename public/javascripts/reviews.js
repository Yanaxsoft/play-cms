jQuery(function($) {
	function hideShowMoreBtn($container) {
		$container.find(".review-more-btn").hide();
	}

	function displayShowMoreBtn($container) {
		$container.find(".review-more-btn").show();
	}

	function hideSpinner($container) {
		$container.find(".spinner").hide();
	}

	function showSpinner($container) {
		$container.find(".spinner").show();
	}

	function refreshSubmissionsRating() {
		// Display Editable Rating Stars in submissions list
		$(".review-submission-item .ratingEditable").rating({
			step: 1.0,
			size: 'xs',
			showCaption: false,
			theme: 'krajee-fa'
		});
		$('.review-submission-item .ratingEditable').on('rating.change', function(event, value, caption) {
			var rating = $(this).val(),
				submissionId = $(this).data('id');
			var url = jsRoutes.ch.insign.cms.blocks.reviewblock.controllers.ReviewFrontendController.addRatingToOldSubmission(submissionId, rating).url;
			$.ajax({
				type: 'POST',
				url: url,
				dataType: 'json',
				cache: false,
				success: function (response) {
					if (response.status == "ok") {
					}
				}
			});

			$(this).rating('refresh', {displayOnly: true});
		});
	}

	function loadSubmissions(reviewId, sorting, page, $container) {
		$.ajax({
			type: 'GET',
			url: jsRoutes.ch.insign.cms.blocks.reviewblock.controllers.ReviewFrontendController.list(reviewId, page, sorting).url,
			dataType: 'json',
			cache: false,
			beforeSend: function () {
				showSpinner($('#productRatingDetails .section-header'));
			},
			complete: function (jqXHR, status, errorThrown) {
				hideSpinner($('#productRatingDetails .section-header'));
			},
			success: function (data, status, jqXHR) {
				if (data.status == "ok") {
					$container.find(".review-more-btn").before(data.content);
					if (!data.hasMorePages) {
						hideShowMoreBtn($container);
					} else {
						displayShowMoreBtn($container);
					}

					$container.data("page", page + 1);
					refreshSubmissionsRating();
				}
			}
		});
	}

	$('.review-more-btn a').click(function() {
		$reviewContainer = $(this).parents('.review-submission-container');
		var reviewId = $reviewContainer.data("review_id"),
			sorting = $reviewContainer.data("sorting"),
			page = $reviewContainer.data("page");

		loadSubmissions(reviewId, sorting, page, $reviewContainer);
	});

	$('.review-sortBy').click(function() {
		$reviewContainer = $(this).parents('.review-container').find('.review-submission-container');
		var reviewId = $reviewContainer.data("review_id"),
			currentSorting = $reviewContainer.data("sorting"),
			page = $reviewContainer.data("page"),
			newSorting = $(this).data("sorting"),
			title = $(this).html(),
			titleTarget = $(this).data('target');

		if (currentSorting !== newSorting) {
			$reviewContainer.find('.review-submission-item').remove();
			$reviewContainer.data("sorting", newSorting);
			$reviewContainer.data("page", 1);
			$(titleTarget).html(title);
			loadSubmissions(reviewId, newSorting, 1, $reviewContainer);
		}
	});

	// loads review submissions at startup
	$(".review-submission-container").each(function() {
			var reviewId = $(this).data("review_id"),
					sorting = $(this).data("sorting"),
					page = $(this).data("page"),
					$this = $(this);

			loadSubmissions(reviewId, sorting, page, $this);
	});

	/**
	 * review form handling
 	 */
	$(".review-form").each(function(){
		$this = $(this);

		$this.ajaxForm({
			dataType: 'json',
			beforeSubmit: function () {
				showSpinner($this);
				$this.find('.help-block').remove();
				$this.find('.has-error').removeClass('has-error');
			},
			success: function (response) {
				if (response.status === 'ok') {
					$this.siblings('.success-message').show();
					$this.remove();
					if (response.reviewStatus === 'approved') {
						window.location.reload();
					}
				} else if (response.status === 'error') {
					$("form .error.help-block").remove();

					$.each(response.errors, function(field, message) {
						var $errorField = $('[name="' + field + '"]'),
							$parentBlock = $errorField.parent();

						$parentBlock.addClass("has-error");
						$parentBlock.append('<div class="error help-block text-error">' + message[0] + '</div>');
					});
				}
			},
			complete: function (jqXHR, status, errorThrown) {
				hideSpinner($this);
			}
		});
	});


	/**
	 * Thumbs up/down click
	 */
	$(document).on("click", ".ratings-thumbs a", function(e) {
		e.preventDefault();
		$this = $(this);

		$.ajax({
			type: 'POST',
			url: $this.data('url'),
			dataType: 'json',
			cache: false,
			beforeSend: function () {
				$this.toggleClass("selected");
				$this.siblings().removeClass("selected");
			},
			success: function (response) {
				if (typeof response.answerRating !== "undefined") {
					$this.parents(".review-submission-item").find(".answer-rating").html(response.answerRating);
					var $answerRatingBlock = $this.parents(".review-submission-item").find(".ratings-helpfulness");
					if (response.answerRating > 0) {
						$answerRatingBlock.show();

						if (response.answerRating == 1) {
							$answerRatingBlock.find(".answer-rating-single").show();
							$answerRatingBlock.find(".answer-rating-multiple").hide();
						} else {
							$answerRatingBlock.find(".answer-rating-single").hide();
							$answerRatingBlock.find(".answer-rating-multiple").show();
						}
					} else {
						$answerRatingBlock.hide();
					}
				}
			}
		});

		return false;
	});

	/**
	 * Show more button
	 */
	$(document).on("click", ".show-more-btn", function(e) {
		e.preventDefault();
		var target = $(this).data("target");
		$(target).show();
		$(this).hide();
		return false;
	});

	/**
	 * Clear name input after tap on mobile devices
	 */
	$(document).on("touchend", "#review_displayName", function(event) {
		$("#review_displayName").val("");
	});

});
