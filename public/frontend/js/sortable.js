jQuery(function($){
    $(".sortContainer").sortable({
        helper: 'clone',
        appendTo: 'body',
        zIndex: 10000,
        connectWith : ".sortContainer",
        handle : ".portlet-header",
        cancel : ".portlet-toggle",
        placeholder : "sortable-box-placeholder round-all",
        start: function( event, ui ) {
            $(".sortable-box-placeholder").height(
                $(ui.item).height() -27
            );
            if (isGridMode($(ui.item).parents(".sortContainer"))) {
                $(".sortable-box-placeholder").width(
                    $(ui.item).width()
                );
                $(".sortable-box-placeholder").css('float', 'left');
            } else {
                $(".sortable-box-placeholder").css('float', 'none');
            }

            $(".sortContainer").addClass("landing-zone");
        },
        stop: function( event, ui ) {
            $(".sortContainer").removeClass("landing-zone");
            $(".sortable-box-placeholder").css('width', 'auto');

            var $block = $(ui.item)
            adjustBlockForHorizontalCollection($block);

            var itemId = $(ui.item).data("block_id");
            var aboveItemId = $(ui.item).prev().data("block_id");
            var parentId = $(ui.item).parents(".sortContainer").data("block_id");

            var jsRouter;
            if (typeof jsRoutes !== 'undefined') {
                jsRouter = jsRoutes;
            }
            if (typeof jsRoutesCMS !== 'undefined') {
                jsRouter = jsRoutesCMS;
            }

            var url = jsRouter.ch.insign.cms.controllers.BlockController.savePosition(itemId, parentId, aboveItemId).url;

            $.post(
                url,
                function(response) {
                    if(response.status == 'ok') {}
                }
            );
        }
    });

    $(".sortContainer").disableSelection();

    function isGridMode(container) {
        var $children = $(container).children('.portlet');
        if ($children.length == 0) {
            return false;
        }
        if ( ($(container).width() / $children.first().width()) > 2 ) {
            return true;
        }
        return false;
    }

    /**
     * Either add or remove "col-md-x" class for block
     * if it was moved from or to a horizontal-collection block
     *
     * @param $block
     */
    function adjustBlockForHorizontalCollection($block) {
        var $parentBlock = $block.parents(".sortContainer").first();

        if ($block.data("col-width") !== undefined) {
            if ($parentBlock.hasClass("horizontal-collection")) {
                if (!$block.hasClass($block.data("col-width"))) {
                    $block.addClass($block.data("col-width"));
                }
            } else {
                $block.removeClass($block.data("col-width"));
            }
        }
    }
});