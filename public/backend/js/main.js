// Translation-Keys provided for backend
Messages = {};

/**
 create object-hirarchy from namespace string
 */
function namespacedKeyValue(ns, value, baseObject) {
    var nsArray = ns.split('.'),
        obj = baseObject
    counter = 0;
    $.each(nsArray, function() {
        if(counter === nsArray.length -1) {
            obj[this] = value
        } else {
            if(typeof obj[this] === "undefined") {
                obj[this] = {}
            }
        }
        obj = obj[this];
        counter++;
    });
    return baseObject;
}

$(function(){


    // clear date input after click on X button
    $(".date-reset" ).click(function(){
        jQuery(this ).parents('.form-group' ).find('input' ).val('');
        return false;
    });

    $(".chosen-select").chosen({});

    $(window).scroll( function(){
       if($(this).scrollTop() > 100){
           $("body").addClass("scroll-down");
       }else{
           $("body").removeClass("scroll-down");
       }
    });


    // Mark language tab with error if one of the mstring elements contains errors
    $("div[mstring-lang] .has-error").each(function(){
        var lang = $(this).parents("div[mstring-lang]").first().attr('mstring-lang');
        $tab = $(".form-body li[mstring-tab=" + lang + "]");
        $tab.addClass('tab-error');
    });

    // edit field should support both: 'ftp' and 'manual' upload
    window.cleanFtpInput = function cleanFtpInput(elementId, domains) {
        var output = $("#" + elementId).val();

        $.each(domains, function(key, domain) {
            if (output.substring(0, domain.length) == domain) {
                $("#" + elementId).val(output.substring(domain.length));
            }
        });
    }

	// RNGR-1653 - Add editLang parameter
	$('input[name="mstringActiveLang"]').on('change', function() {
		var lang = $(this).val();
		$("a.add-edit-lang").each(function(index, element) {
			var href = $(this).attr("href").replace(/(\?|&)editLang=\w{2}/, "");
			href += (href.indexOf("?") === -1) ? "?" : "&";
			href += "editLang=" + lang;
			$(this).attr("href", href);
		});
	}).trigger('change');

    // blocks onSave and onSaveAndStay
    $('.block-save-btn').click(function(e){
        e.preventDefault();
        $("#form_edit_block").submit();
    });

    $('.block-saveAndStay-btn').click(function(e){
        e.preventDefault();
        $("#saveAndEdit").val("true");
        $("#form_edit_block").submit();
    });

    // moximanager browse button
    if (typeof moxman != "undefined") {
        $(document).on('click', '.moxman-btn-browse', function(e) {
            e.preventDefault();
            var inputId = $(this).data("input_id");
            moxman.browse (
                {
                    fields : inputId,
                    no_host: true
                });
        });
    }

    // handle tagsinput and mstring tagsinput
    $('.mstring-tagsinput, .input-tagsinput').each(function(){
        var params = $(this).data('params');
        var isTypeAheadParametersPresent = ($(this).data('is_typeahead_params') == 1) ? true : false;
        var typeaheadOptions = $(this).data('typeahead_options');
        var $input = $(this);

        $input.tagsinput(params);
        if (isTypeAheadParametersPresent) {
            $input.tagsinput('input').typeahead(typeaheadOptions);

            $input.tagsinput('input').bind('typeahead:selected', $.proxy(function (obj, datum) {
                this.tagsinput('add', datum.value);
                this.tagsinput('input').typeahead('setQuery', '');
            }, $input ));
        }

				$input.on('itemAdded', function(e) {
					// manually remove the current input from input.tt-query -> otherwise it will cause a missleading view
					var $ttQuery = $(e.target).next().find('.tt-query');
					$ttQuery.val('');
					setTimeout(function() {
						$('.tt-query').val('')
					}, 150);
				});

    });


    // Init libraries

    App.init();
    UIExtendedModals.init();
    TableAdvanced.init();

    $('.wysihtml5').wysihtml5({
        "stylesheets": [jsconfig.wysihtml5.tableAdvancedStylesheets]
    });

    tinymce.init({
        relative_urls: false,
        paste_as_text: true,
        //                paste_auto_cleanup_on_paste : true,
        //                paste_remove_styles: true,
        //                paste_remove_styles_if_webkit: true,
        selector:'.tinymce-simple',
        height: '300px',
        language: jsconfig.tinyMce.defaultLang,  // TinyMCE silently crashes if the language is not found - no fallback
        language_url: jsconfig.tinyMce.language_url,
        toolbar: jsconfig.tinyMce.simpleLayout.toolbar,
        plugins: jsconfig.tinyMce.simpleLayout.plugins,
        menubar: false,
        statusbar: false,
        external_plugins: jsconfig.tinyMce.externalPlugins,
        extended_valid_elements: "+a[*],+i[*],+em[*],+li[*],+span[*],+div[*]"
    });

    tinymce.init({
        relative_urls: true,
        paste_as_text: true,
        selector:'.tinymce-default',
        height: '600px',
        language: jsconfig.tinyMce.defaultLang,
        language_url: jsconfig.tinyMce.language_url,
        theme: "modern",
        plugins: jsconfig.tinyMce.defaultLayout.plugins,
        toolbar1: jsconfig.tinyMce.defaultLayout.toolbar1,
        toolbar2: jsconfig.tinyMce.defaultLayout.toolbar2,
        image_advtab: true,
        table_default_attributes: jsconfig.tinyMce.defaultLayout.table_default_attributes,
        table_class_list: jsconfig.tinyMce.defaultLayout.table_class_list,
        setup: jsconfig.tinyMce.defaultLayout.setup_table_responsive,
        table_toolbar: jsconfig.tinyMce.defaultLayout.table_toolbar,
        templates: jsconfig.tinyMce.templates,
        external_filemanager_path: jsconfig.tinyMce.defaultLayout.external_filemanager_path,
        external_plugins: jsconfig.tinyMce.externalPlugins,
        extended_valid_elements: "+a[*],+i[*],+em[*],+li[*],+span[*],+div[*]"
    });

    $('.date-picker').datetimepicker({
        pickTime: false,
        language: jsconfig.cms.currentLanguage
    });

    $(".form_datetime").datetimepicker({
        todayBtn: true,
        pickerPosition: "top-left",
        language: jsconfig.cms.currentLanguage
    });

    $(".make-switch input:checkbox").bootstrapSwitch();
});
