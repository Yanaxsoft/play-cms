var UINestable = function () {

    var initNestable = function(nestableSelector, outputSelector, options) {
        if(options == undefined) {
            options = { group: 1, maxDepth: 1 };
        }
        var nestable = $(nestableSelector);
        var output = $(outputSelector);
        var handleChange = function() {
            output.val(window.JSON.stringify(nestable.nestable('serialize')));
        };
        nestable.nestable(options).on('change', handleChange);
        handleChange();
    };

    return {
        init: function () {
            initNestable('#slider-entities', '#slider-entry-order');
        }
    };

}();