jQuery(document).ready(function($) {
    var $form = $('#image-upload-form');
    var $loader = $('#image-upload-loader'); // loder.gif image
    var $confirmButton = $('#confirmUpload');
    var $previewArea = $('#image-upload-modal .preview'); // preview area

    $('.image-upload').click(function(e){
        e.preventDefault();
        $previewArea.html('');
        $confirmButton.addClass('disabled');
        $confirmButton.data('inputId', $(this ).prev().attr('id'));
        $('#image-upload-modal').modal('show');

    });

    $('#uploadImage').change(function(){
        // implement with ajaxForm Plugin
        $form.ajaxForm({
            beforeSend: function(){
                $loader.show();
                $previewArea.fadeOut();
            },
            success: function(e){
                $loader.hide();
                $form.resetForm();
                $confirmButton.removeClass('disabled');
                $previewArea.html( '<img src="' + e.filename + '">' ).fadeIn();
                $confirmButton.data('filename', e.filename);
            },
            error: function(e){
                $previewArea.html(e).fadeIn();
            }
        });
        $form.submit();
    });

    $('#confirmUpload' ).click(function() {
        var imputId = $confirmButton.data('inputId');
        $('#'+imputId).val($confirmButton.data('filename'));
        $('#image-upload-modal').modal('hide');
    });

});
