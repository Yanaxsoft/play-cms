$(function(){
    if (!jQuery().dataTable) {
        return;
    }

    jsconfig.datatables.sAjaxSource = $('#datatableUrl').val();
    // jsconfig.datatables.aoColumnDefs = [{
    //     'bSortable': false,
    //     'aTargets': [2]
    // }];
    // jsconfig.datatables.aaSorting = [[0,'asc']];

    oTable = $('#user_list').dataTable(jsconfig.datatables);
    
    
    // modify table search input
    jQuery('#user_list_wrapper .dataTables_filter').addClass("form-group pull-left");
    jQuery('#user_list_wrapper .dataTables_length select').addClass("form-control input-small ");
    jQuery('#user_list_wrapper .dataTables_length select').css({ display: "block" });
    jQuery('#user_list_wrapper .dataTables_length').addClass("pull-right");

    $('.input_search').append('<div class="input-group"><input type="text" id="term_input_search" aria-controls="user_list" class="form-control" placeholder="' + Messages.backend.datatables.user.search.possible.terms + '"><span class="input-group-btn"><button id="search_btn" type="submit" class="btn btn-primary"><i class="fa fa-search"></i></button></span></div>');

    $('#search_btn').click( function () {
        oTable.fnFilter($('#term_input_search').val());
    } );

    $("#term_input_search").keyup(function(event){
        if(event.keyCode == 13){
            oTable.fnFilter($('#term_input_search').val());
        }
    });
});
