$(function() {
    $.ajax({
        type: "GET",
        cache: false,
        url: $('#permissions-block-container').data('url'),
        success: function (response) {
            $('#permissions-block-container').html(response);
            $('.save-block-buttons').removeClass('disabled');
        }
    });

    $(window).off('beforeunload');
});