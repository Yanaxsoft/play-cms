$(function () {
    $('#site-select').change(function (event) {
        // Handle changes in global site selector(widget) and replace host with chosen one
        window.location = "//" + $(this).val();
    })
});