$(function() { "use strict";

    // AuthzSwitch definition

    var AuthzSwitch = function (element) {
        this.$element = $(element)
    }

    AuthzSwitch.prototype.toggle = function () {
        var $parent = this.$element.closest('[data-toggle="authz-switch"]'),
            $input = this.$element.find('input'),
            active = this.$element.hasClass('active');

        $parent.find('.active').removeClass('active');

        if (active) {
            $parent.removeClass("selected");
            $parent.addClass("not-selected");
            $parent.find('.fallback input').prop('checked', true).trigger('change');
        } else {
            $parent.removeClass("not-selected");
            $parent.addClass("selected");
            $input.prop('checked', true).trigger('change');
            this.$element.addClass('active');
        }

    }

    AuthzSwitch.toggle = function (btn) {
        var $btn = $(btn);
        if (!$btn.hasClass('btn')) $btn = $btn.closest('.btn');
        var data = $btn.data('ch.insign.cms.authz.switch');
        if (!data) $btn.data('ch.insign.cms.authz.switch', (data = new AuthzSwitch($btn)));
        data.toggle();

        return btn;
    }

    // Event handlers

    $(document).on('click.ch.insign.cms.authz.switch', '[data-toggle^=authz-switch]', function (e) {
        AuthzSwitch.toggle(e.target);
        e.preventDefault();
    });

    $('.authz-switch-all input').change(function(e) {
        var $this = $(this),
            $btn = $this.closest('.btn'),
            $perm = $this.closest('.permission-input'),
            $group = $perm.closest(".permission-group"),
            value = $this.val();

        if (value.slice(0, 5) === 'allow') {
            $group.removeClass('restricted').addClass('unrestricted');
        } else if (value.slice(0, 4) === 'deny') {
            $group.removeClass('unrestricted').addClass('restricted');
        } else if (value.slice(0, 7) === 'inherit') {
            if ($perm.data("isPermittedByDefault")) {
                $group.removeClass('restricted').addClass('unrestricted');
            } else {
                $group.removeClass('unrestricted').addClass('restricted');
            }
        }
    });
});