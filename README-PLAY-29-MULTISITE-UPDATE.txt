
Notes
===

* The "Root nodes" (Top level visible nodes, connected to the root parent (which is never displayed) define site roots.
Below in the tree, all nodes are assumed to have the same site key as the parent node.

* Nodes that contain the wildcard "*" as site key will be visible in all sites

* In the backend, like in the frontend, only the site's nodeds are visible. Not all sites.

* Moving pages from one site to another is officially not supported (experimental, it's possible but can have side-effects)


SQL Updates
===
CREATE UNIQUE INDEX cms_blocks_KEY_SITE_uindex ON `play-cms-demo`.cms_blocks (`KEY`, SITE);

UPDATE cms_blocks SET SITE='*' WHERE `KEY`='_root'
UPDATE cms_blocks SET SITE='*' WHERE `KEY`='_backend'


