/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.playauth.views

import scala.language.implicitConversions

package object helper {

  import play.twirl.api.Html
  import ch.insign.playauth.party.Party
  import ch.insign.playauth.authz.DomainPermission
  import ch.insign.playauth.PlayAuth.{isPermitted, isAuthenticated, isAnonymous, isImpersonated, isRemembered, getCurrentParty, getPreviousParty}

  /**
   * Implicitly converts DomainPermission[T] to a RichDomainPermission[T]
   */
  implicit def toRichDomainPermission[T](p: DomainPermission[T]): RichDomainPermission[T] = {
    new RichDomainPermission[T](p.domain(), p.name(), p.target())
  }

  /**
   * @param f a function to apply if party is authenticated
   * @return an option value containing result of `f` or `None`
   */
  def authenticated(f: Party => Html): Option[Html] = {
    if(isAuthenticated) Option(f(getCurrentParty)) else None
  }

  /**
   * @param content a value to return if party is not authenticated
   * @return an option value containing `content` or `None`
   */
  def notAuthenticated(content: Html): Option[Html] = {
    if(!isAuthenticated) Some(content) else None
  }

  /**
   * @param f a function to apply if party is remembered
   * @return an option value containing result of `f` or `None`
   */
  def remembered(f: Party => Html): Option[Html] = {
    if(isRemembered) Option(f(getCurrentParty)) else None
  }

  /**
   * @param content a value to return if party is not remembered
   * @return an option value containing `content` or `None`
   */
  def notRemembered(content: Html): Option[Html] = {
    if(!isRemembered) Some(content) else None
  }

  /**
   * @param content a value to return if party is anonymous
   * @return an option value containing `content` or `None`
   */
  def anonymous(content: Html): Option[Html] = {
    if(isAnonymous) Some(content) else None
  }

  /**
   * @param f a function to apply if party is not anonymous, i.e. it is authenticated or remembered
   * @return an option value containing result of `f` or `None`
   */
  def notAnonymous(f: Party => Html): Option[Html] = {
    if(!isAnonymous) Option(f(getCurrentParty)) else None
  }

  /**
   * @param f a function to apply if current `Party` is impersonated, the first arg is current `Party` and
   *          the second arg is the previous (original) `Party`
   * @return an option value containing result of `f` or `None`
   */
  def impersonated(f: (Party, Party) => Html): Option[Html] = {
    if(isImpersonated) Option(f(getCurrentParty, getPreviousParty)) else None
  }

  /**
   * @param ps assumed allowed permissions
   * @param content a value to return if party is allowed at least one of given permissions `ps`
   * @return an option value containing `content` or `None`
   */
  def hasAnyPermission(ps: DomainPermission[_]*)(content: Html): Option[Html] = {
    ps.find(isPermitted).map(_ => content)
  }

  /**
   * @param ps assumed not allowed permissions
   * @param content a value to return if party is not allowed at least one of given permissions `ps`
   * @return an option value containing `content` or `None`
   */
  def lacksAnyPermission(ps: DomainPermission[_]*)(content: Html): Option[Html] = {
    ps.find(!isPermitted(_)).map(_ => content)
  }

  /**
   * @param ps assumed allowed permissions
   * @param content a value to return if party is allowed all of given permissions `ps`
   * @return an option value containing `content` or `None`
   */
  def hasAllPermissions(ps: DomainPermission[_]*)(content: Html): Option[Html] = {
    if(ps.forall(isPermitted)) Option(content) else None
  }

  /**
   * @param ps assumed not allowed permissions
   * @param content a value to return if party is not allowed all of given permissions `ps`
   * @return an option value containing `content` or `None`
   */
  def lacksAllPermissions(ps: DomainPermission[_]*)(content: Html): Option[Html] = {
    if(ps.forall(!isPermitted(_))) Option(content) else None
  }

  /**
   * @param p assumed allowed permission
   * @param content a value to return if party is allowed the given permission `p`
   * @return an option value containing `content` or `None`
   */
  def hasPermission(p: DomainPermission[_])(content: Html): Option[Html] = {
    if(isPermitted(p)) Some(content) else None
  }

  /**
   * @param p assumed not allowed permission
   * @param content a value to return if party is not allowed the given permission `p`
   * @return an option value containing `content` or `None`
   */
  def lacksPermission(p: DomainPermission[_])(content: Html): Option[Html] = {
    if(!isPermitted(p)) Some(content) else None
  }

}