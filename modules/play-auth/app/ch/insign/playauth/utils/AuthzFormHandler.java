/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.playauth.utils;

import ch.insign.playauth.PlayAuth;
import ch.insign.playauth.authz.DomainPermission;
import ch.insign.playauth.authz.ObjectIdentity;
import ch.insign.playauth.authz.PermissionManager;
import ch.insign.playauth.authz.SecurityIdentity;
import ch.insign.playauth.permissions.AclPermission;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.crypto.hash.Sha1Hash;

import java.util.*;
import java.util.stream.Stream;

import static ch.insign.playauth.PlayAuth.getAccessControlManager;
import static ch.insign.playauth.PlayAuth.getPermissionManager;
import static ch.insign.playauth.authz.SecurityIdentity.UNKNOWN;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toSet;
import static org.apache.commons.lang3.StringUtils.substringAfter;

public class AuthzFormHandler {
	private final Optional<ObjectIdentity> target;
	private final Set<SecurityIdentity> sids;

	public static void save(SecurityIdentity targetSid, Object targetResource, Map<String, String> formData) {
		AuthzFormHandler.save(Arrays.asList(targetSid), targetResource, formData);
	}

	public static void save(List<SecurityIdentity> targetSids, Object targetResource, Map<String, String> formData) {
		new AuthzFormHandler(targetSids, targetResource).save(formData);
	}

	public void save(Map<String, String> formData) {
		getDefinedPermissions().forEach(permission -> sids.forEach(sid -> save(sid, permission, formData)));
	}

	public static String generateAuthzInputName(SecurityIdentity sid, String permissionName) {
		return generateAuthzInputName(Optional.ofNullable(sid), permissionName, true);
	}

	public static String generateAuthzInputName(Optional<SecurityIdentity> maybeSid, String permissionName) {
		return generateAuthzInputName(maybeSid, permissionName, true);
	}

	private static String generateAuthzInputName(Optional<SecurityIdentity> maybeSid, String permissionName, boolean unique) {
		Objects.requireNonNull(maybeSid);
		Objects.requireNonNull(permissionName);

		return "authz-" +
				Optional.of(maybeSid.orElse(UNKNOWN)).map(sid -> sid.getType() + "-" + sid.getIdentifier() + "-").orElse("") +
				permissionName +
				(unique ? "-" + new Sha1Hash(UUID.randomUUID().toString().getBytes()) : "");
	}

	public AuthzFormHandler(List<SecurityIdentity> targetSids) {
		this(targetSids, null);
	}

	public AuthzFormHandler(List<SecurityIdentity> targetSids, Object targetResource) {
		Objects.requireNonNull(targetSids);

		sids = new HashSet<>(targetSids);
		this.target = Optional.ofNullable(targetResource).map(PlayAuth::getObjectIdentity);
	}

	public static void saveForAllRoles(Object targetResource, Map<String, String> formData) {
		final List<SecurityIdentity> targetSids = Stream.concat(
				Stream.of(SecurityIdentity.ALL),
				PlayAuth.getPartyRoleManager().findAll().stream().map(PlayAuth::getSecurityIdentity))
				.collect(toList());

		AuthzFormHandler.save(targetSids, targetResource, formData);
	}

	private void save(SecurityIdentity sid, DomainPermission<?> permission, Optional<ObjectIdentity> altTarget, boolean isAllowSelected, boolean isDenySelected, boolean isInheritSelected) {
		// skip further processing if permission is not available within the form data
		if (!isAllowSelected && !isDenySelected && !isInheritSelected) return;

		final String pid = getPermissionManager().getQualifiedName(permission);

		DomainPermission<?> p;
		if (altTarget.isPresent()) {
			p = altTarget.map(target -> getPermissionManager().applyTarget(permission, target)).get();
		} else {
			p = permission;
		}

		boolean isDirectlyAllowed = getPermissionManager().getAllowedPermissions(sid, p.target())
				.stream()
				.map(getPermissionManager()::getQualifiedName)
				.anyMatch(id -> id.equals(pid));

		boolean isDirectlyDenied = getPermissionManager().getDeniedPermissions(sid, p.target())
				.stream()
				.map(getPermissionManager()::getQualifiedName)
				.anyMatch(id -> id.equals(pid));

		boolean isAllowedByDefault = getAccessControlManager().isPermittedByDefault(sid, p);
		boolean isDeniedByDefault = !isAllowedByDefault;

		boolean isSelected = isAllowSelected || isDenySelected;
		boolean isNotSelected = !isSelected;
		boolean isSet = isDirectlyAllowed || isDirectlyDenied;
		boolean isNotSet = !isSet;

		boolean changedToDefault = isNotSelected && isSet;
		boolean changedToAllow = isAllowSelected && (isDirectlyDenied || isNotSet);
		boolean changedToDeny = isDenySelected && (isDirectlyAllowed || isNotSet);

		if (changedToDefault) {
			PlayAuth.requirePermission(AclPermission.UPDATE_ACL, p.target());
			getAccessControlManager().removePermission(sid, p);
		} else if (changedToAllow) {
			PlayAuth.requirePermission(AclPermission.UPDATE_ACL, p.target());
			getAccessControlManager().allowPermission(sid, p);
		} else if (changedToDeny) {
			PlayAuth.requirePermission(AclPermission.UPDATE_ACL, p.target());
			getAccessControlManager().denyPermission(sid, p);
		}
	}

	private void save(SecurityIdentity sid, DomainPermission<?> permission, Map<String, String> formData) {
		final String pid = getPermissionManager().getQualifiedName(permission);

		boolean isAllowSelected = false;
		boolean isDenySelected = false;
		boolean isInheritSelected = false;
		Optional<ObjectIdentity> altTarget = Optional.empty();

		// Entry key format: authz-{sidType}-{sidIdentifier}-{permissionName}-{sha1Random}
		// Entry value format: (allow|deny)[:{oidType}-{oidIdentifier}]
		for (Map.Entry<String, String> entry : formData.entrySet()) {
			if (authzInputNameMatches(entry.getKey(), sid, pid)) {
				isAllowSelected = entry.getValue().startsWith("allow");
				isDenySelected = entry.getValue().startsWith("deny");
				isInheritSelected = entry.getValue().startsWith("inherit");
				altTarget = Optional.of(entry.getValue())
						.map(val -> substringAfter(val, ":"))
						.filter(StringUtils::isNotEmpty)
						.map(val -> val.split("-"))
						.map(Arrays::asList)
						.filter(parts -> parts.size() == 2)
						.map(parts -> new ObjectIdentity(parts.get(0), parts.get(1)));

				save(sid, permission, altTarget, isAllowSelected, isDenySelected, isInheritSelected);
			}
		}
	}

	private boolean authzInputNameMatches(String name, SecurityIdentity sid, String pid) {
		return name.startsWith(generateAuthzInputName(Optional.ofNullable(sid), pid, false)) ||
				name.startsWith(generateAuthzInputName(Optional.of(UNKNOWN), pid, false));
	}

	private Set<DomainPermission<?>> getDefinedPermissions() {
		PermissionManager pm = getPermissionManager();

		return target.isPresent()
				? target.map(pm::getDefinedPermissions)
				.orElse(Collections.emptySet())
				.stream()
				.map(p -> pm.applyTarget(p, target.get()))
				.collect(toSet())
				: pm.getDefinedPermissions();
	}

}
