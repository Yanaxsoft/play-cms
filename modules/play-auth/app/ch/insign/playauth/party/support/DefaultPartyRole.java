/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.playauth.party.support;


import ch.insign.playauth.party.Party;
import ch.insign.playauth.party.PartyRole;
import play.data.validation.Constraints;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "auth_default_party_role")
public class DefaultPartyRole implements PartyRole {

    @Id
    @GeneratedValue
    private long id;

    /*
     * Field required to cascade deleting the role.
     */
    @ManyToMany(mappedBy = "partyRoles")
    private Set<DefaultParty> parties = new HashSet<>();

    /*
     * Predefined Roles
     */
    public static final String ROLE_SUPERUSER = "Superuser";
    public static final String ROLE_ADMIN = "Admin";
    public static final String ROLE_USER = "User";

    @Constraints.Required
    @Column(name = "name", nullable = false, unique = true)
    private String name;

    public DefaultPartyRole() {
    }

    public DefaultPartyRole(String name) {
        this.name = name;
    }

    /**
     * Get the string representation of the id.
     *
     * @return
     */
    public String getId() {
        if (id == 0) return null;
        return String.valueOf(id);
    }

    @Override
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    Set<DefaultParty> getParties() {
        return parties;
    }

    @PreRemove
    private void removeRoleFromParties() {
        for (Party p : parties) {
            p.removeRole(this);
        }
    }

    @Override
    public String toString() {
        return getName();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 17;
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        return result;
    }

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		DefaultPartyRole that = (DefaultPartyRole) o;
		return name.equals(that.name);
	}
}
