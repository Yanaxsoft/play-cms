/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.playauth.party.support;

import ch.insign.playauth.PlayAuth;
import ch.insign.playauth.party.Party;
import ch.insign.playauth.party.PartyIdentifier;
import ch.insign.playauth.party.PartyManager;
import ch.insign.playauth.party.address.EmailAddress;
import com.uaihebert.factory.EasyCriteriaFactory;
import com.uaihebert.model.EasyCriteria;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.subject.SimplePrincipalCollection;
import play.data.validation.Constraints;
import play.db.jpa.JPA;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

public class DefaultPartyManager implements PartyManager {

    @Override
    public Class<? extends Party> getPartyClass() {
        return DefaultParty.class;
    }

    @Override
    public Party find(Object primaryKey) {
        if (!isPrimaryKey(primaryKey)) {
            return null;
        }

        final Long id = Long.parseLong(String.valueOf(primaryKey));
        return em().find(getPartyClass(), id);
    }

    @Override
    public Party findOneByPrincipal(Object principal) {
        if (isPrimaryKey(principal)) {
            return find(principal);
        }

        if (isEmail(principal)) {
            return findOneByEmail(principal);
        }

        return null;
    }

    @Override
    public Party findOneByPrincipals(PrincipalCollection principals) {
        Party foundParty;
        PartyIdentifier identifier = principals.oneByType(PartyIdentifier.class);
        if (identifier != null) {
            foundParty = findOneByPrincipal(identifier);
            if (foundParty != null )
                return foundParty;
        }

        EmailAddress email = principals.oneByType(EmailAddress.class);
        if (email != null) {
            foundParty = findOneByPrincipal(email);
            if (foundParty != null )
                return foundParty;
        }

        return findOneByPrincipal(principals.getPrimaryPrincipal());
    }

    @Override
    public Collection<Party> findAll() {
        Collection<Party> parties = new ArrayList<>();
        try {
            List<? extends Party> results = query().getResultList();
            parties.addAll(results);
            return parties;
        } catch (NoResultException e) {
            return null;
        }
    }

    @Override
    public Party create(String name, AuthenticationInfo authInfo) {
        final PartyName partyName = new PartyName(name);
        final PrincipalCollection principals = authInfo.getPrincipals();
        final Object credentials = authInfo.getCredentials();
        final EmailAddress email = principals.oneByType(EmailAddress.class);

        if (email == null) {
            throw new IllegalArgumentException("EmailAddress principal is required.");
        }

        if (credentials == null) {
            throw new IllegalArgumentException("Credentials is required.");
        }

        Party party = null;
        try {
            party = getPartyClass().newInstance();
        } catch (InstantiationException | IllegalAccessException e) {
            throw new RuntimeException("Can't create instance of a given party type.");
        }

        if (party instanceof DefaultParty) {
            DefaultParty defaultParty = (DefaultParty) party;
            defaultParty.setName(partyName.getFullName());
            defaultParty.setEmail(email.getAddress());
            defaultParty.setCredentials(PlayAuth.getPasswordService().encryptPassword(credentials));
        }

        return getOrCreate(party);
    }

    @Override
    public Party create(String name, Collection<Object> principals, Object credentials) {
        AuthenticationInfo authInfo = new SimpleAuthenticationInfo(
                new SimplePrincipalCollection(principals, "default"),
                credentials);

        return create(name, authInfo);
    }

    @Override
    public Party create(String name, Object credentials, Object... principals) {
        return create(name, Arrays.asList(principals), credentials);
    }

    @Override
    public Party create(String name, PrincipalCollection principals, Object credentials) {
        AuthenticationInfo authInfo = new SimpleAuthenticationInfo(principals, credentials);

        return create(name, authInfo);
    }

    @Override
    public void delete(Party party) {
        party = find(party.getId());
        if (party != null) {
            em().remove(party);
        }
    }

    @Override
    public void save(Party party) {
        if (em().contains(party)) {
            em().merge(party);
        } else {
            em().persist(party);
        }
    }


    /*
     * HELPERS
     */

    private EntityManager em() {
        return JPA.em();
    }

    private boolean isPrimaryKey(Object value) {
        try {
            return Long.parseLong(String.valueOf(value)) >= 0;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    private boolean isEmail(Object value) {
        String email = String.valueOf(value);
        return Constraints.email().isValid(email);
    }

    private Party findOneByEmail(Object email) {
        if (!isEmail(email)) {
            return null;
        }

        try {
            return em().createQuery("SELECT u FROM " + getPartyClass().getSimpleName() + " u WHERE u.email = :email", getPartyClass())
                    .setParameter("email", String.valueOf(email))
                    .getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }

    private Party getOrCreate(Party p) {
        if (em().contains(p)) {
            return p;
        }

        Party party = findOneByPrincipals(p.getPrincipals());
        if (party != null) {
            //TODO: merge with Party p
            return party;
        } else {
            em().persist(p);
            return p;
        }
    }

    private static class PartyName {
        private String[] names;

        public PartyName(String fullName) {
            names = fullName.split("\\s");
        }

        public String getFirstName() {
            return names[0];
        }

        public String getLastName() {
            if (names.length > 1) {
                return names[names.length - 1];
            }
            return null;
        }

        public String getFullName() {
            return String.join(" ", names);
        }
    }

    public EasyCriteria<? extends Party> query() {
        return EasyCriteriaFactory.createQueryCriteria(JPA.em(), getPartyClass());
    }

}
