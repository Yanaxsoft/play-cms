/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.playauth.party;

import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.subject.PrincipalCollection;

import java.util.Collection;

/**
 * TODO: refactor create(..) methods
 * TODO: add possibility to manage subtypes of Party.class
 */
public interface PartyManager {

    Party find(Object identifier);

    Party findOneByPrincipal(Object principal);

    Party findOneByPrincipals(PrincipalCollection principals);

    Collection<Party> findAll();

    Class<? extends Party> getPartyClass();

    Party create(String name, AuthenticationInfo authInfo);

    Party create(String name, Collection<Object> principals, Object credentials);

    Party create(String name, Object credentials, Object... principals);

    Party create(String name, PrincipalCollection principals, Object credentials);

    void delete(Party party);

    void save(Party party);
}
