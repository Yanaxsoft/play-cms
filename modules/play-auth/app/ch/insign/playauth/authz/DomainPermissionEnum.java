/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.playauth.authz;

/**
 * Default implementation of the {@code DomainPermission} interface to
 * allow clean and unobtrusive definition of the {@code DomainPermission} enum constants.
 *
 *<pre>
 * {@code
  *     public enum FilePermission implements DomainPermissionEnum<File> {
 *         READ, WRITE, EXECUTE, DELETE;
 *     }
 * }
 * </pre>
 */
@FunctionalInterface
public interface DomainPermissionEnum<T> extends DomainPermission<T> {

	String name();

	@Override
	default String domain() {
		return DomainPermissionUtils.getTargetType(this)
				.map(Class::getName)
				.orElseThrow(() -> new UnknownDomainException("Cannot resolve permission domain."));
	}

	@Override
	default ObjectIdentity target() {
		return ObjectIdentity.ALL;
	}

	/**
	 * Signals that the target type {@code Class<T>} represented by the type argument 'T'
	 * could not be retrieved because of the Java type erasure or some other reason.
	 */
	class UnknownDomainException extends RuntimeException {
		public UnknownDomainException(String message) {
			super(message);
		}
	}
}
