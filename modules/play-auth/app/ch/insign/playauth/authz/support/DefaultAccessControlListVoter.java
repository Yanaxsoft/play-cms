/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.playauth.authz.support;

import ch.insign.playauth.authz.*;
import ch.insign.playauth.event.AuthorizationEvent;
import ch.insign.playauth.event.EventDispatcher;
import ch.insign.playauth.event.PermissionAuthorizationEvent;
import ch.insign.playauth.event.RoleAuthorizationEvent;
import ch.insign.playauth.party.Party;
import ch.insign.playauth.party.PartyRole;
import ch.insign.playauth.permissions.GlobalDomainPermission;
import net.sf.ehcache.search.Attribute;
import net.sf.ehcache.search.Result;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.io.Serializable;
import java.util.*;
import java.util.stream.Stream;

import static ch.insign.playauth.utils.CacheUtils.cache;
import static ch.insign.playauth.utils.CacheUtils.cached;
import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.mapping;
import static java.util.stream.Collectors.toSet;


@Singleton
public class DefaultAccessControlListVoter implements AccessControlListVoter {

    private final static Logger logger = LoggerFactory.getLogger(DefaultAccessControlListVoter.class);

    private static final String CACHE_NAME_VOTE = Authorizer.class.getName();
    private static final String CACHE_NAME_INHERITED_VOTE = Authorizer.class.getName() + ".inheritedVote";

    private final PermissionManager pm;
    private final AccessControlList acl;
    private final SecurityIdentityRetrievalStrategy sidrs;
    private final ObjectIdentityRetrievalStrategy oidrs;

    @Inject
    public DefaultAccessControlListVoter(
            EventDispatcher dispatcher,
            PermissionManager pm,
            AccessControlList acl,
            SecurityIdentityRetrievalStrategy sidrs,
            ObjectIdentityRetrievalStrategy oidrs
    ) {

        this.pm = pm;
        this.acl = acl;
        this.sidrs = sidrs;
        this.oidrs = oidrs;

        // clear cached when ACL is being updated
        dispatcher.addListener(AuthorizationEvent.class, event -> {
            Optional<SecurityIdentity> someSid = Optional.empty();

            if (event instanceof RoleAuthorizationEvent) {
                someSid = Optional.of(sidrs.getSecurityIdentity(((RoleAuthorizationEvent) event).getParty()));
            } else if (event instanceof PermissionAuthorizationEvent) {
                someSid = Optional.of(((PermissionAuthorizationEvent) event).getSid());
            }

            someSid.ifPresent(sid -> {
                Stream.of(cache(CACHE_NAME_VOTE), cache(CACHE_NAME_INHERITED_VOTE)).forEach(cache -> {
                    if (sid.equals(SecurityIdentity.ALL) || sid.getSource().filter(r -> r instanceof PartyRole).isPresent()) {
                        // clear cache for all users
                        cache.removeAll();
                        logger.debug("Cleared all entries from cache \"{}\"", cache.getName());
                    } else {
                        // clear cache for single user
                        Attribute<String> sidType = cache.getSearchAttribute("sidType");
                        Attribute<String> sidIdentifier = cache.getSearchAttribute("sidIdentifier");

                        List<Result> entries = cache.createQuery()
                                .includeKeys()
                                .addCriteria(sidType.eq(sid.getType()).and(sidIdentifier.eq(sid.getIdentifier())))
                                .execute()
                                .all();

                        entries.forEach(r -> cache.remove(r.getKey()));

                        logger.debug("Cleared {} entries from cache \"{}\" for SID({})", entries.size(), cache.getName(), sid);
                    }
                });
            });
        });
    }

    @Override
    public Optional<Vote> vote(Object authority, DomainPermission<?> permission) {
        return cached(CACHE_NAME_VOTE, new AuthorizerCacheKey(sidrs.getSecurityIdentity(authority), permission, pm.getQualifiedName(permission)), () -> {

            Optional<Vote> specificVote = Optional.empty();
            Optional<Vote> commonVote = vote(SecurityIdentity.ALL, permission);

            // skip other checks if the permission is allowed for anyone: it makes no sense to protect
            // resources from authenticated parties if anonymous parties have access to the resource.
            if (commonVote.map(Vote::allowed).orElse(false)) {
                return commonVote;
            }

            if (authority instanceof Party) {
                Set<Optional<Vote>> votes = new HashSet<>();

                votes.add(Optional.of((Party) authority)
                        .map(sidrs::getSecurityIdentity)
                        .flatMap(sid -> vote(sid, permission)));


                // include votes for Party's roles
                votes.addAll(Optional.of((Party) authority)
                        .map(Party::getRoles)
                        .map(Collection::stream)
                        .orElse(Stream.empty())
                        .map(sidrs::getSecurityIdentity)
                        .map(sid -> vote(sid, permission))
                        .collect(toSet()));

                // select the most specific and preferably positive vote
                specificVote = reduce(votes
                        .stream()
                        .filter(Optional::isPresent)
                        .map(Optional::get)
                        .collect(toSet()));
            } else if (authority instanceof PartyRole) {
                specificVote = Optional.of((PartyRole) authority)
                        .map(sidrs::getSecurityIdentity)
                        .flatMap(sid -> vote(sid, permission));
            } else if (authority instanceof SecurityIdentity && !authority.equals(SecurityIdentity.ALL)) {
                specificVote = vote((SecurityIdentity) authority, permission);
            }

            return specificVote.isPresent() ? specificVote : commonVote;
        });
    }

    /**
     * Optionally returns a {@link Vote vote} telling if the given {@link SecurityIdentity authority}
     * is allowed or denied the given {@link DomainPermission permission} by default,
     * without taking into account the ACE record for the given {@link SecurityIdentity authority}
     * and target {@link ObjectIdentity resource}.
     */
    public Optional<Vote> inheritedVote(SecurityIdentity sid, DomainPermission<?> permission) {
        return cached(CACHE_NAME_INHERITED_VOTE, new AuthorizerCacheKey(sid, permission, pm.getQualifiedName(permission)), () ->
                acl.find(sid, permission.target())
                        .filter(ace -> pm.allows(ace, permission) && !ace.contains(permission))
                        .map(ace -> Optional.of(new Vote(ace, permission, pm.allows(ace, permission))))
                        .filter(Optional::isPresent)
                        .orElseGet(() -> withParentTarget(permission)
                                .map(p -> vote(sid, p))
                                .filter(Optional::isPresent)
                                .orElseGet(() -> withExpandedTarget(permission)
                                        .map(p -> vote(sid, p))
                                        .filter(Optional::isPresent)
                                        .orElseGet(() -> withExpandedAction(permission)
                                                .map(p -> vote(sid, p))
                                                .filter(Optional::isPresent)
                                                .orElseGet(() -> impliedVote(sid, permission))))));
    }

    /**
     * Optionally returns a vote with the most specific target.
     * The allowing vote is preferred if there are allowing and denying votes with equally specific targets.
     */
    @Override
    public Optional<Vote> reduce(Collection<Vote> votes) {
        Set<Vote> bestVotes = votes
                .stream()
                .collect(groupingBy(this::getVoteSpecificness, TreeMap::new, mapping(vote -> vote, toSet())))
                .entrySet()
                .stream()
                .findFirst()
                .map(Map.Entry::getValue)
                .orElse(Collections.<Vote>emptySet());

        Optional<Vote> allowingVote = bestVotes.stream().filter(Vote::allowed).findAny();

        // prefer allowing vote
        return allowingVote.isPresent() ? allowingVote : bestVotes.stream().findAny();
    }

    /**
     * Optionally returns a {@link Vote vote} telling if the given {@link SecurityIdentity authority}
     * is allowed or denied the given {@link DomainPermission permission}.
     */
    private Optional<Vote> vote(SecurityIdentity sid, DomainPermission<?> permission) {
        Optional<Vote> result = acl.find(sid, permission.target())
                .filter(ace -> pm.implies(ace, permission))
                .map(ace -> new Vote(ace, permission, pm.allows(ace, permission)));

        if(!result.isPresent()) {
            result = withParentTarget(permission).flatMap(p -> vote(sid, p));
        }

        if (!result.isPresent()) {
            result = withExpandedTarget(permission).flatMap(p -> vote(sid, p));
        }

        if (!result.isPresent()) {
            result = impliedVote(sid, permission);
        }

        return result;
    }

    /**
     * Returns a Vote that is implied by other than given permission.
     *
     * The {@link DefaultPermissionManager} is used to manage implication rules.
     * @see DefaultPermissionManager#addAllowingRule
     */
    private Optional<Vote> impliedVote(SecurityIdentity sid, DomainPermission<?> permission) {
        return pm.resolveAllowingPermissions(permission)
                .stream()
                .map(allowingPerm -> pm.isTargetApplicable(allowingPerm, permission.target())
                        ? pm.applyTarget(allowingPerm, permission.target())
                        : allowingPerm)
                .<Optional<Vote>>map(allowingPerm -> vote(sid, allowingPerm))
                .filter(Optional::isPresent)
                .map(Optional::get)
                .filter(vote -> pm.allows(vote.permission(), permission))
                .findFirst();
    }

    /**
     * Returns the priority level for the given Vote.
     * The vote with the most specific target will get the highest priority.
     */
    private int getVoteSpecificness(Vote vote) {
        final ObjectIdentity oid = vote.permission().target();
        if (oid.equals(ObjectIdentity.ALL)) {
            return 2; // LOW
        } else if (oid.isClassIdentity()) {
            return 1; // MID
        } else {
            return 0; // HIGH
        }
    }


    /**
     * Returns the given permission with the target replaced by its parent.
     */
    private Optional<DomainPermission<?>> withParentTarget(DomainPermission<?> permission) {
        return permission.target()
                .getSource()
                .flatMap(src -> permission
                        .target()
                        .getMetadata(Hierarchy.class)
                        .map(h -> h.getParent(src)))
                .flatMap(parent -> parent
                        .map(oidrs::getObjectIdentity))
                .map(oid -> pm.applyTarget(permission, oid));
    }

    /**
     * Returns a permission with the less specific target.
     */
    private Optional<DomainPermission<?>> withExpandedTarget(DomainPermission<?> p) {
        if (p.target().equals(ObjectIdentity.ALL)) {
            return Optional.empty();
        } else if(p.target().isClassIdentity()) {
            return Optional.of(pm.applyTarget(p, ObjectIdentity.ALL));
        } else {
            return Optional.of(pm.applyTarget(p, p.target().asClassIdentity()));
        }
    }

    private Optional<DomainPermission<?>> withExpandedAction(DomainPermission<?> p) {
        if (p.domain().equals(GlobalDomainPermission.ALL.domain()) && p.name().equals(GlobalDomainPermission.ALL.name())) {
            return Optional.empty();
        } else if(p.name().equals(GlobalDomainPermission.ALL.name())) {
            return Optional.of(new DefaultDomainPermission<>(GlobalDomainPermission.ALL.domain(), GlobalDomainPermission.ALL.name(), p.target()));
        } else {
            return Optional.of(new DefaultDomainPermission<>(p.domain(), GlobalDomainPermission.ALL.name(), p.target()));
        }
    }

    private static class AuthorizerCacheKey implements Serializable {
        public final SecurityIdentity sid;
        public final ObjectIdentity oid;
        public final String action;

        AuthorizerCacheKey(SecurityIdentity sid, DomainPermission<?> permission, String action) {
            Objects.requireNonNull(sid);
            Objects.requireNonNull(permission);
            this.sid = sid;
            this.oid = permission.target();
            this.action = action;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            AuthorizerCacheKey that = (AuthorizerCacheKey) o;

            return sid.equals(that.sid) && oid.equals(that.oid) && action.equals(that.action);
        }

        @Override
        public int hashCode() {
            int result = sid.hashCode();
            result = 31 * result + oid.hashCode();
            result = 31 * result + action.hashCode();
            return result;
        }
    }
}
