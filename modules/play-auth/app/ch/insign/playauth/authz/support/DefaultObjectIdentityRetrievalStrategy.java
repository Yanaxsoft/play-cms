/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.playauth.authz.support;

import ch.insign.playauth.authz.*;
import ch.insign.playauth.utils.Clearable;
import com.google.common.base.Throwables;
import org.apache.commons.beanutils.BeanUtils;
import play.inject.Injector;

import javax.inject.Inject;
import javax.inject.Provider;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Stream;

public class DefaultObjectIdentityRetrievalStrategy implements ObjectIdentityRetrievalStrategy, Clearable {

	private static final List<String> IDENTIFIER_PROPERTIES = Arrays.asList("id", "identifier");
	private final Map<Class<?>, ObjectIdentityMetadata> metadata = new ConcurrentHashMap<>();
	private final Provider<PermissionManager> pm;
	private final Injector injector;

	@Inject
	public DefaultObjectIdentityRetrievalStrategy(Provider<PermissionManager> pm, Injector injector) {
		this.pm = pm;
		this.injector = injector;
	}

	@Override
    public ObjectIdentity getObjectIdentity(Object target) {

        if (target == null) {
            return ObjectIdentity.ALL;
        }

        if (target instanceof ObjectIdentity) {
            return (ObjectIdentity) target;
        }

	    String type = resolveType(target);
	    String identifier = resolveIdentifier(target);
	    Map<Class<?>, Object> metadata = resolveMetadata(target);

	    return new ObjectIdentity(type, identifier, target, metadata);
    }

	@Override
	public Optional<ObjectIdentityMetadata> getMetadata(Class<?> type) {
		return Optional.ofNullable(metadata.get(type));
	}

	@Override
	public void setMetadata(Class<?> type, ObjectIdentityMetadata metadata) {
		this.metadata.put(type, metadata);
	}

	@Override
	public void clear() {
		metadata.clear();
	}

	private String resolveType(Object target) {
		if (DomainPermission.class.isAssignableFrom(getDomainClassOf(target))) {
			return DomainPermission.class.getName();
		} else {
			return getDomainClassOf(target).getName();
		}
	}

	private String resolveIdentifier(final Object target) {
		return getMetadata(getDomainClassOf(target))
				.map(m -> m.get(Identifier.class))
				.orElseGet(() -> Optional.ofNullable(getDomainClassOf(target).getAnnotation(WithIdentifier.class))
						.map(WithIdentifier::value)
						.map(injector::instanceOf))
				.flatMap(identifier -> identifier.getIdentifier(target))
				.filter(Objects::nonNull)
				.orElseGet(() -> {
					if (target instanceof Class<?>) {
						return ObjectIdentity.CLASS_IDENTIFIER;
					}

					if (target instanceof DomainPermission) {
						return pm.get().getQualifiedName((DomainPermission) target);
					} else {
						return IDENTIFIER_PROPERTIES.stream()
								.map(propName -> {
									try {
										return BeanUtils.getProperty(target, propName);
									} catch (NoSuchMethodException e) {
										return null;
									} catch (Throwable t) {
										throw Throwables.propagate(t);
									}
								})
								.filter(Objects::nonNull)
								.findFirst()
								.orElseThrow(() -> new NullPointerException("Could not extract identity from object " + target));
					}
				});
	}


	private Map<Class<?>, Object> resolveMetadata(final Object target) {
		Map<Class<?>, Object> data = new HashMap<>();

		getMetadata(getDomainClassOf(target))
				.map(m -> m.get(Authorizer.class))
				.orElseGet(() -> resolveAuthorizer(target))
				.ifPresent(authorizer -> data.put(Authorizer.class, authorizer));

		getMetadata(getDomainClassOf(target))
				.map(m -> m.get(Hierarchy.class))
				.orElseGet(() -> resolveHierarchy(target))
				.ifPresent(hierarchy -> data.put(Hierarchy.class, hierarchy));

		return data;
	}

	private Optional<Authorizer> resolveAuthorizer(final Object target) {
		return Optional.ofNullable(getDomainClassOf(target).getAnnotation(WithAuthorizer.class))
				.map(WithAuthorizer::value)
				.map(injector::instanceOf);
	}

	private Optional<Hierarchy> resolveHierarchy(final Object target) {
		return Optional.ofNullable(getDomainClassOf(target).getAnnotation(WithHierarchy.class))
				.map(WithHierarchy::value)
				.map(injector::instanceOf);
	}

	private Class<?> getDomainClassOf(Object target) {
		Class<?> cls = target instanceof Class<?> ? (Class<?>) target : target.getClass();

		if (cls.isPrimitive() || cls.isArray()) {
			throw new IllegalArgumentException("Primitives and Arrays cannot be used as domain objects");
		}

		if (isLambda(cls)) {
			return cls.getInterfaces()[0];
		}

		if (cls.isAnonymousClass()) {
			Optional<Class<?>> maybeSuperclass = Optional.<Class<?>>ofNullable(cls.getSuperclass()).filter(c -> !c.equals(Object.class));
			Optional<Class<?>> maybeInterface = Stream.<Class<?>>of(cls.getInterfaces()).findFirst();

			return maybeSuperclass.orElseGet(() -> maybeInterface.orElse(Object.class));
		}

		return cls;
	}

	private static boolean isLambda(Class<?> cls) {
		return cls.isSynthetic() && !cls.isAnonymousClass() && !cls.isLocalClass();
	}
}
