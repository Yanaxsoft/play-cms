/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.playauth.authz.support;

import ch.insign.playauth.authz.*;
import ch.insign.playauth.permissions.AclPermission;
import ch.insign.playauth.utils.Clearable;
import com.google.common.base.Throwables;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.reflections.Reflections;
import org.reflections.scanners.SubTypesScanner;
import org.reflections.scanners.TypeAnnotationsScanner;
import org.reflections.util.ConfigurationBuilder;
import play.Environment;
import play.api.Play;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.lang.reflect.Field;
import java.net.URL;
import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toMap;
import static java.util.stream.Collectors.toSet;
import static org.reflections.util.ClasspathHelper.forPackage;

@Singleton
public class DefaultPermissionManager implements Clearable, PermissionManager {

	private Set<TypedDomainPermission<?>> permissions = Collections.synchronizedSet(new HashSet<>());
	private Map<String, Set<String>> allowsRules = Collections.synchronizedMap(new HashMap<>());

	private final AccessControlList acl;
	private final ObjectIdentityRetrievalStrategy oidrs;

	private Environment playEnv;

	@Inject
	public DefaultPermissionManager(AccessControlList acl, ObjectIdentityRetrievalStrategy oidrs, Environment playEnv) {
		this(acl, oidrs, Arrays.asList("ch.insign", "models", "controllers", "permissions"), playEnv);
	}

	public DefaultPermissionManager(AccessControlList acl,
									ObjectIdentityRetrievalStrategy oidrs,
									List<String> domainPermissionsSearchPackages,
	                                Environment playEnv) {
		this.acl = acl;
		this.oidrs = oidrs;
		this.playEnv = playEnv;

		if (domainPermissionsSearchPackages != null) {
			registerDomainPermissions(domainPermissionsSearchPackages);
		}
	}

	@Override
	public void addAllowingRule(DomainPermission<?> left, DomainPermission<?> right) {
		allowsRules
				.computeIfAbsent(getQualifiedName(left), k -> new HashSet<>())
				.add(getQualifiedName(right));
	}

	@Override
	public Set<DomainPermission<?>> resolveAllowingPermissions(DomainPermission<?> permission) {
		String rightPid = getQualifiedName(permission);
		return allowsRules
				.entrySet()
				.stream()
				.filter(e -> e.getValue().contains(rightPid))
				.map(Map.Entry::getKey)
				.flatMap(leftPid -> permissions.stream().filter(p -> getQualifiedName(p).equals(leftPid)))
				.collect(toSet());
	}

	@Override
	public Set<DomainPermission<?>> resolvePermissionsAllowedBy(DomainPermission<?> permission) {
		String leftPid = getQualifiedName(permission);
		return allowsRules
				.getOrDefault(leftPid, Collections.emptySet())
				.stream()
				// Exclude AclPermission.UPDATE_ACL
				//TODO: generalize this special case for AclPermission.UPDATE_ACL
				.filter(p -> !p.equals(getQualifiedName(AclPermission.UPDATE_ACL)))
				.flatMap(rightPid -> permissions.stream().filter(p -> getQualifiedName(p).equals(rightPid)))
				.collect(toSet());
	}

	@Override
	public boolean implies(DomainPermission<?> left, DomainPermission<?> right) {
		final String WILDCARD_TOKEN = "*";

		for(Pair<String, String> pair : Arrays.asList(
				Pair.of(left.domain(), right.domain()),
				Pair.of(left.name(), right.name()),
				Pair.of(left.target().getType(), right.target().getType()),
				Pair.of(left.target().getIdentifier(), right.target().getIdentifier()))) {

			if (!pair.getLeft().equals(WILDCARD_TOKEN) && !pair.getLeft().equals(pair.getRight())) {
				return false;
			}
		}

		return true;
	}

	@Override
	public boolean allows(DomainPermission<?> left, DomainPermission<?> right) {
		return allowsRules
				.getOrDefault(getQualifiedName(left), Collections.emptySet())
				.contains(getQualifiedName(right))
				//TODO: generalize this special case for AclPermission.UPDATE_ACL
				&& (!getQualifiedName(right).equals(getQualifiedName(AclPermission.UPDATE_ACL))
					|| (!right.target().equals(ObjectIdentity.ALL) && isTargetApplicable(left, right.target())));
	}

	@Override
	public <T> DomainPermission<T> definePermission(Class<T> domainType, String name) {
		TypedDomainPermission<T> p = new TypedDomainPermission<>(domainType, name);
		permissions.add(p);
		return p;
	}

	@Override
	public Set<DomainPermission<?>> getDefinedPermissions() {
		return Collections.unmodifiableSet(permissions);
	}

	/**
	 * Returns set of {@link DomainPermission permissions} for which the given {@link ObjectIdentity oid} is applicable
	 * target ({@link DefaultPermissionManager#applyTarget})
	 */
	@Override
	public Set<DomainPermission<?>> getDefinedPermissions(ObjectIdentity oid) {
		Predicate<DomainPermission<?>> isApplicable = isTargetApplicablePredicate(oid);

		return permissions
				.stream()
				.filter(isApplicable)
				.collect(toSet());
	}

	@Override
	@SuppressWarnings("unchecked")
	public <T> Set<DomainPermission<T>> getDefinedPermissions(Class<T> targetType) {
		return permissions
				.stream()
				.filter(p -> p.targetType().isAssignableFrom(targetType))
				.map(p -> (DomainPermission<T>) p)
				.collect(toSet());
	}

	@Override
	public Set<DomainPermission<?>> getAllowedPermissions(SecurityIdentity sid, ObjectIdentity oid) {
		Objects.requireNonNull(sid);
		Objects.requireNonNull(oid);

		return acl.find(sid, oid)
				.map(AccessControlEntry::allowedPermissions)
				.orElse(Collections.emptySet());
	}

	@Override
	public Set<DomainPermission<?>> getAllowedPermissions(SecurityIdentity sid) {
		Objects.requireNonNull(sid);

		return acl.findBySid(sid)
				.stream()
				.flatMap(ace -> ace.allowedPermissions().stream())
				.collect(toSet());
	}

	@Override
	public Set<DomainPermission<?>> getDeniedPermissions(SecurityIdentity sid, ObjectIdentity oid) {
		Objects.requireNonNull(sid);
		Objects.requireNonNull(oid);

		return acl.find(sid, oid)
				.map(AccessControlEntry::deniedPermissions)
				.orElse(Collections.emptySet());
	}

	@Override
	public Set<DomainPermission<?>> getDeniedPermissions(SecurityIdentity sid) {
		Objects.requireNonNull(sid);

		return acl.findBySid(sid)
				.stream()
				.flatMap(ace -> ace.deniedPermissions().stream())
				.collect(toSet());
	}

	@Override
	public DomainPermission<?> resolveDomainPermission(String domain, String action, ObjectIdentity target) {
		Objects.requireNonNull(domain);
		Objects.requireNonNull(action);
		Objects.requireNonNull(target);

		return permissions
				.stream()
				.filter(p -> p.domain().equals(domain))
				.findAny()
				.map(p -> (DomainPermission) new TypedDomainPermission<>(domain, action, target, p.targetType()))
				.orElseGet(() -> (DomainPermission) new DefaultDomainPermission<>(domain, action, target));
	}

	/**
	 * Returns a new DomainPermission with the given target applied.
	 */
	@Override
	public <T, V extends T> DomainPermission<T> applyTarget(DomainPermission<T> p, V target) {
		return applyTarget(p, oidrs.getObjectIdentity(target));
	}

	/**
	 * Returns a new DomainPermission with the given target applied.
	 */
	@Override
	public <T, V extends T> DomainPermission<T> applyTarget(DomainPermission<T> p, Class<V> target) {
		return applyTarget(p, oidrs.getObjectIdentity(target));
	}

	/**
	 * Returns a new DomainPermission with the given target applied.
	 */
	@Override
	public <T> DomainPermission<T> applyTarget(DomainPermission<T> p, ObjectIdentity target) {

		if (!isTargetApplicable(p, target)) {
			throw new IllegalArgumentException(
					"The target " + target + " cannot be applied to the permission " + getQualifiedName(p));
		}

		return new DefaultDomainPermission<>(p.domain(), p.name(), target);
	}

	/**
	 * Returns true if the given target can be applied to the given permission.
	 */
	@Override
	public boolean isTargetApplicable(DomainPermission<?> p, ObjectIdentity target) {
		return isTargetApplicablePredicate(target).test(p);
	}

	/**
	 * Returns the qualified name of the underlying DomainPermission.
	 *
	 * The qualified name is composed of the 'qualifier' and the 'local name' which are separated with dot (".").
	 */
	@Override
	public String getQualifiedName(DomainPermission<?> p) {
		Objects.requireNonNull(p);
		return String.format("%s.%s", p.domain(), p.name());
	}

	@Override
	public void clear() {
		permissions.clear();
		allowsRules.clear();
	}

	@Override
	public boolean allows(AccessControlEntry ace, DomainActionEntry action) {
		return anyImplies(ace.getAllow(), ace.getOid(), action);
	}

	@Override
	public boolean allows(AccessControlEntry ace, DomainPermission<?> p) {
		return anyImplies(ace.getAllow(), ace.getOid(), p);
	}

	@Override
	public boolean denies(AccessControlEntry ace, DomainActionEntry action) {
		return anyImplies(ace.getDeny(), ace.getOid(), action);
	}

	@Override
	public boolean denies(AccessControlEntry ace, DomainPermission<?> p) {
		return anyImplies(ace.getDeny(), ace.getOid(), p);
	}

	@Override
	public boolean implies(AccessControlEntry ace, DomainActionEntry action) {
		return allows(ace, action) || denies(ace, action);
	}

	@Override
	public boolean implies(AccessControlEntry ace, DomainPermission<?> p) {
		return allows(ace, p) || denies(ace, p);
	}

	private Predicate<DomainPermission<?>> isTargetApplicablePredicate(ObjectIdentity oid) {
		return oid.equals(ObjectIdentity.ALL)
				// 1) yes if oid is ObjectIdentity.ALL
				? (p) -> true
				// 2) yes if oid has bound source object and permission applies to that object
				: oid.getSource()
				.map(this::isTargetApplicablePredicate)
				// 3) yes if a Class of the source object can be reflectively resolved and permission applies to that Class
				.orElseGet(() -> {
					try {
						Class<?> targetType = Class.forName(oid.getType(), true, playEnv.classLoader());
						return isTargetApplicablePredicate(targetType);
					} catch (ClassNotFoundException e) {
						return (p) -> false;
					}
				});
	}

	private Predicate<DomainPermission<?>> isTargetApplicablePredicate(Object target) {
		Class<?> targetType = target instanceof Class<?> ? (Class<?>) target : target.getClass();
		return (p) -> {
			if (p instanceof TypedDomainPermission) {
				return ((TypedDomainPermission<?>) p).targetType().isAssignableFrom(targetType);
			} else {
				try {
					Class<?> domainType = Class.forName(p.domain(), true, playEnv.classLoader());
					return domainType.isAssignableFrom(domainType);
				} catch (ClassNotFoundException e) {
					return false;
				}
			}
		};
	}

	private void registerDomainPermissions(List<String> domainPermissionsSearchPackages) {
		Map<DomainPermission<?>, Class<?>> definitions = findDomainPermissionEnums(domainPermissionsSearchPackages)
				.stream()
				.flatMap(e -> Stream.<DomainPermission<?>>of(e.getEnumConstants()))
				.map(p -> DomainPermissionUtils.getTargetType(p).map(t -> new ImmutablePair<>(p, t)))
				.filter(Optional::isPresent)
				.map(Optional::get)
				.collect(toMap(ImmutablePair::getLeft, ImmutablePair::getRight));

		definitions.forEach((permission, domain) -> definePermission(domain, permission.name()));

		definitions.forEach((permission, domain) -> {
			try {
				Field left = permission.getClass().getDeclaredField(permission.name());
				if (left.isAnnotationPresent(Allows.class)) {
					Allows annotation = left.getAnnotation(Allows.class);
					for (String name : Arrays.asList(annotation.value())) {
						if (name.contains(".")) {
							// name is fully qualified
							getDefinedPermissions()
									.stream()
									.filter(p -> getQualifiedName(p).equals(name))
									.findAny()
									.ifPresent(allowed -> addAllowingRule(permission, allowed));
						} else {
							// name is local
							getDefinedPermissions(domain)
									.stream()
									.filter(p -> p.name().equals(name))
									.findAny()
									.ifPresent(allowed -> addAllowingRule(permission, allowed));
						}
					}
				}
			} catch (Throwable throwable) {
				throw Throwables.propagate(throwable);
			}
		});
	}

	private <T> Set<Class<? extends DomainPermissionEnum<T>>> findDomainPermissionEnums(List<String> searchPackages) {
		Collection<URL> urls = searchPackages.stream()
				.flatMap(pkg -> forPackage(pkg).stream())
				.collect(toSet());

		ConfigurationBuilder config = new ConfigurationBuilder()
				.addUrls(urls)
				.setScanners(new SubTypesScanner(), new TypeAnnotationsScanner());

		Reflections reflections = new Reflections(config);

		Set<? extends Class<?>> types = reflections.getSubTypesOf(DomainPermissionEnum.class);

		return types.stream()
				.filter(Class::isEnum)
				.map(t -> (Class<? extends DomainPermissionEnum<T>>) t)
				.collect(toSet());
	}

	private boolean anyImplies(Set<DomainActionEntry> actions, ObjectIdentity target, DomainActionEntry right) {
		return anyImplies(actions, target, resolveDomainPermission(right.domain(), right.name(), target));
	}

	private boolean anyImplies(Set<DomainActionEntry> actions, ObjectIdentity target, DomainPermission right) {
		return actions.stream()
				.map(a -> resolveDomainPermission(a.domain(), a.name(), target))
				.anyMatch(left -> implies(left, right));
	}

}
