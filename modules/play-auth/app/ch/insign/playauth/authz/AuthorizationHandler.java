/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.playauth.authz;

import org.apache.shiro.authz.AuthorizationException;
import play.mvc.Http;
import play.mvc.Result;

/**
 * AuthorizationHandler handles authorization and authentication exceptions.
 */
public interface AuthorizationHandler {

    /**
     * Event is triggered when operation or action is not allowed.
     *
     * @param ctx Current Http context
     * @param e Authorization exception
     * @return
     */
    Result onUnauthorized(Http.Context ctx, AuthorizationException e);

    /**
     * Event is triggered when attempting to execute an authorization action when a successful
     * authentication hasn't yet occurred.
     *
     * @param ctx Current Http context
     * @param e Authorization exception
     * @return
     */
    Result onUnauthenticated(Http.Context ctx, AuthorizationException e);
}
