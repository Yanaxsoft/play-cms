/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.playauth.authz;

import java.util.Objects;


public class DefaultDomainPermission<T> implements DomainPermission<T> {
	private final String domain;
	private final String name;
	private final ObjectIdentity target;

	public DefaultDomainPermission(String domain, String name, ObjectIdentity target) {
		Objects.requireNonNull(domain);
		Objects.requireNonNull(name);
		Objects.requireNonNull(target);
		this.domain = domain;
		this.name = name;
		this.target = target;
	}

	@Override
	public String domain() {
		return domain;
	}

	@Override
	public String name() {
		return name;
	}

	@Override
	public ObjectIdentity target() {
		return target;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof DomainPermission)) return false;

		DomainPermission that = (DomainPermission) o;

		if (!domain.equals(that.domain())) return false;
		if (!name.equals(that.name())) return false;
		if (!target.equals(that.target())) return false;

		return true;
	}

	@Override
	public int hashCode() {
		int result = domain.hashCode();
		result = 31 * result + name.hashCode();
		result = 31 * result + target.hashCode();
		return result;
	}
}
