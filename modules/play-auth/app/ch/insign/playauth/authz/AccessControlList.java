/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.playauth.authz;

import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

/**
 * A repository of {@link AccessControlEntry}
 */
public interface AccessControlList {

    /**
     * Optionally returns an {@link AccessControlEntry} for the specified sid/oid pair.
     */
    Optional<AccessControlEntry> find(SecurityIdentity sid, ObjectIdentity oid);

    /**
     * Optionally returns an {@link AccessControlEntry} with the given identifier.
     */
    Optional<AccessControlEntry> find(AccessControlEntry.Identifier id);

    /**
     * Returns a list of all {@link AccessControlEntry}.
     */
    List<AccessControlEntry> findAll();

    /**
     * Returns a stream of all {@link AccessControlEntry}.
     */
    Stream<AccessControlEntry> streamAll();

    /**
     * Returns a list of {@link AccessControlEntry} for the specified {@link SecurityIdentity sid}.
     */
    List<AccessControlEntry> findBySid(SecurityIdentity sid);

    /**
     * Returns a list of {@link AccessControlEntry} for the specified {@link ObjectIdentity oid}.
     */
    List<AccessControlEntry> findByOid(ObjectIdentity oid);

    /**
     *  Returns true if the ACL contains the specified {@link AccessControlEntry ace}.
     */
    boolean contains(AccessControlEntry ace);

    /**
     * Put a given ACE to the ACL, an existing entry will be overridden.
     */
    AccessControlEntry put(AccessControlEntry ace);

    /**
     * Remove {@link AccessControlEntry ace} from ACL.
     */
    void remove(AccessControlEntry ace);

    /**
     * Remove {@link AccessControlEntry} from ACL by the given identifier.
     */
    void remove(AccessControlEntry.Identifier id);
}
