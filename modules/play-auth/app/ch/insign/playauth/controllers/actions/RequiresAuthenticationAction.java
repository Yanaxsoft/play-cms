/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.playauth.controllers.actions;

import ch.insign.playauth.PlayAuthApi;
import org.apache.shiro.authz.UnauthenticatedException;
import play.mvc.Http;
import play.mvc.Result;

import javax.inject.Inject;
import java.util.concurrent.CompletionStage;

/**
 * Ensures the calling subject is {@link ch.insign.playauth.PlayAuth#isAuthenticated() authenticated}.
 */
public class RequiresAuthenticationAction extends WithSubjectAction<RequiresAuthentication> {

	@Inject
	private PlayAuthApi playAuthApi;

	@Override
	public CompletionStage<Result> doCall(Http.Context ctx) {
		if (!playAuthApi.isAuthenticated()) {
			throw new UnauthenticatedException("The current Subject is not authenticated. Access denied.");
		}

		return this.delegate.call(ctx);
	}
}
