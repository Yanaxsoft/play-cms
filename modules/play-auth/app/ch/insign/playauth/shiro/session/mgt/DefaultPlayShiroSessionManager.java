/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.playauth.shiro.session.mgt;

import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

import ch.insign.playauth.shiro.session.PlayShiroSession;
import org.apache.shiro.authz.AuthorizationException;
import org.apache.shiro.session.Session;
import org.apache.shiro.session.SessionException;
import org.apache.shiro.session.mgt.DefaultSessionManager;
import org.apache.shiro.session.mgt.SessionContext;
import org.apache.shiro.session.mgt.SessionKey;
import play.mvc.Http;

import java.util.HashMap;

public class DefaultPlayShiroSessionManager extends DefaultSessionManager implements PlayShiroSessionManager {
	private final static Logger logger = LoggerFactory.getLogger(DefaultPlayShiroSessionManager.class);

    public DefaultPlayShiroSessionManager() {
    }

    @Override
    public Session start(SessionContext context) throws AuthorizationException {
        if (!ch.insign.playauth.shiro.util.HttpContextUtils.isHttpContextSource(context)) {
            super.start(context);
        }

        return createSession((ch.insign.playauth.shiro.util.HttpContextSource) context);
    }

    @Override
    public Session getSession(SessionKey key) throws SessionException {
        if (!ch.insign.playauth.shiro.util.HttpContextUtils.isHttpContextSource(key)) {
            return super.getSession(key);
        }

        return createSession((ch.insign.playauth.shiro.util.HttpContextSource) key);
    }

    protected Session createSession(ch.insign.playauth.shiro.util.HttpContextSource httpContextSource) {
        if (ch.insign.playauth.shiro.util.HttpContextUtils.hasHttpContext(httpContextSource)) {
            Http.Context httpContext = httpContextSource.getHttpContext();
            return new PlayShiroSession(httpContext.session(), getHost(httpContextSource));
        } else {
            return new PlayShiroSession(new Http.Session(new HashMap<String, String>()), null);
        }
    }

    protected String getHost(ch.insign.playauth.shiro.util.HttpContextSource httpContextSource) {
        String host = null;

        if (httpContextSource instanceof SessionContext) {
            host = ((SessionContext) httpContextSource).getHost();
        }

        if (host == null && ch.insign.playauth.shiro.util.HttpContextUtils.hasHttpContext(httpContextSource)) {
            host = httpContextSource.getHttpContext().request().host();
        }

        return host;
    }
}
