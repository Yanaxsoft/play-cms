package ch.insign.playauth.authz.utils;

import org.junit.rules.ExternalResource;
import play.db.Database;
import play.db.Databases;
import play.db.jpa.JPA;
import play.db.jpa.JPAApi;

public class TestDatabase extends ExternalResource {
    public static final String DATASOURCE_IDENTIFIER = "defaultPersistenceUnit";
    public Database database;
    public JPAApi jpa;

    public void execute(final String sql) {
        database.withConnection(connection -> {
            connection.createStatement().execute(sql);
        });
    }

    @Override
    public void before() {
        database = Databases.inMemoryWith("jndiName", "DefaultDS");
        jpa = JPA.createFor(DATASOURCE_IDENTIFIER);
    }

    @Override
    public void after() {
        jpa.shutdown();
        database.shutdown();
    }
}
