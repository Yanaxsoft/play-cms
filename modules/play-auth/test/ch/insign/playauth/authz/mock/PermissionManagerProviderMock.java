package ch.insign.playauth.authz.mock;

import ch.insign.playauth.authz.PermissionManager;

import javax.inject.Provider;

public class PermissionManagerProviderMock implements Provider<PermissionManager> {

    private PermissionManager pm;

    public PermissionManagerProviderMock set(PermissionManager pm) {
        this.pm = pm;
        return this;
    }

    @Override
    public PermissionManager get() {
        return pm;
    }
}
