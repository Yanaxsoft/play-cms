package ch.insign.playauth.authz.mock;

import ch.insign.playauth.authz.AccessControlEntry;
import ch.insign.playauth.authz.AccessControlList;
import ch.insign.playauth.authz.ObjectIdentity;
import ch.insign.playauth.authz.SecurityIdentity;

import java.util.*;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;

/**
 * Created by serhii on 7/19/16.
 */
public class AccessControlListMock implements AccessControlList {

    private Set<AccessControlEntry> aces = new HashSet<>();

    @Override
    public Optional<AccessControlEntry> find(SecurityIdentity sid, ObjectIdentity oid) {
        return aces.stream()
                .filter(ace -> ace.getSid().equals(sid))
                .filter(ace -> ace.getOid().equals(oid))
                .findAny();
    }

    @Override
    public Optional<AccessControlEntry> find(AccessControlEntry.Identifier id) {
        return aces.stream()
                .filter(ace -> ace.getId().equals(id))
                .findAny();
    }

    @Override
    public List<AccessControlEntry> findAll() {
        return aces.stream().collect(toList());
    }

    @Override
    public Stream<AccessControlEntry> streamAll() {
        return aces.stream();
    }

    @Override
    public List<AccessControlEntry> findBySid(SecurityIdentity sid) {
        return aces.stream()
                .filter(ace -> ace.getSid().equals(sid))
                .collect(toList());
    }

    @Override
    public List<AccessControlEntry> findByOid(ObjectIdentity oid) {
        return aces.stream()
                .filter(ace -> ace.getOid().equals(oid))
                .collect(toList());
    }

    @Override
    public boolean contains(AccessControlEntry ace) {
        return aces.contains(ace);
    }

    @Override
    public AccessControlEntry put(AccessControlEntry ace) {
        find(ace.getSid(), ace.getOid())
                .ifPresent(old -> aces.remove(old));
        aces.add(ace);
        return ace;
    }

    @Override
    public void remove(AccessControlEntry ace) {
        find(ace.getSid(), ace.getOid())
                .ifPresent(old -> aces.remove(old));
    }

    @Override
    public void remove(AccessControlEntry.Identifier id) {
        aces.stream()
                .filter(ace -> ace.getId().equals(id))
                .findAny()
                .ifPresent(old -> aces.remove(old));
    }
}
