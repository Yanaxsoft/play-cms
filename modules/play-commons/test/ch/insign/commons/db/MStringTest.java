package ch.insign.commons.db;

import ch.insign.commons.db.util.TestDatabase;
import ch.insign.commons.i18n.Language;
import org.junit.Rule;
import org.junit.Test;
import play.Application;
import play.Mode;
import play.inject.guice.GuiceApplicationBuilder;
import play.test.Helpers;
import play.test.WithApplication;

import java.util.Arrays;
import java.util.Map;

import static ch.insign.commons.db.util.TestDatabase.DATASOURCE_IDENTIFIER;
import static org.junit.Assert.*;
import static play.test.Helpers.inMemoryDatabase;


public class MStringTest extends WithApplication {

    @Rule
    public TestDatabase db = new TestDatabase();

    @Override
    @SuppressWarnings("unchecked")
    public Application provideApplication() {
        return new GuiceApplicationBuilder()// Create a guice application with modules
                .in(Helpers.class.getClassLoader())// Take a class loader provided by Play! helper
                .in(Mode.TEST)// Set an environment mode for configuration
                .configure((Map) inMemoryDatabase())// Explicitly convert to Map, not Configuration
                .build();
    }

    @Test
    public void testCanHoldLargeTextAndFallbackWorks() {
        char[] array = new char[2 * 65536 + 1];
        Arrays.fill(array, '*');
        String reallyLongString = new String(array);

        // initializedEntityManager - an entity manager used for manage inMemory database with specified context to prevent the thread/em problem
        // Allow us to create only needed tables and work specially for fakeApplication thread pool
        MString storedEntity = db.jpa.withTransaction(() -> {// Transaction for persist some MString
            MString ms = new MString();
            ms.set("en", reallyLongString);

            db.jpa.em().persist(ms);
            return ms;
        });

        String storedId = storedEntity.getId();// Retrieve and tests just stored primary
        assertNotNull(storedId);
        assertTrue(!storedId.isEmpty());

        db.jpa.withTransaction(() -> {// Transaction for read some MString
            MString ms = db.jpa.em().find(MString.class, Long.valueOf(storedId));
            assertEquals("Size doesn't match", reallyLongString.length(), ms.get("en").length());
            assertTrue("String ain't equals", reallyLongString.compareTo(ms.get("en")) == 0);

            return null;
        });
    }

    @Test
    public void testFallbackAndUnicodeWorks() {
        String unicode = "Iԉｔｅｒԉåｔï߀лａɭìȥａｔì߀ｎ ïѕ ｔհë ƙéϒ ｔ߀ ǥｌｏƅàｌ ãｃｃèｐｔɑлｃê.";

        // initializedEntityManager - an entity manager used for manage inMemory database with specified context
        // Allow us to create only needed tables and work specially for fakeApplication thread pool
        MString storedEntity = db.jpa.withTransaction(() -> {// Transaction for persist some MString
            MString ms = new MString();
            ms.set("es", unicode);
            ms.set("fr", "fr!");
            ms.set(Language.getDefaultLanguage(), "default!");

            db.jpa.em().persist(ms);
            return ms;
        });

        String storedId = storedEntity.getId();// Retrieve and tests just stored primary
        assertNotNull(storedId);
        assertTrue(!storedId.isEmpty());

        db.jpa.withTransaction(() -> {// Transaction for read some MString
            MString ms = db.jpa.em().find(MString.class, Long.valueOf(storedId));
            assertEquals("Didn't get correct language and unicode string back", unicode, ms.get("es"));
            assertEquals("Didn't get correct language back", "fr!", ms.get("fr"));
            assertEquals("Didn't get fallback string if lang not available", "default!", ms.get("ru", true));

            return null;
        });
    }
}
