package ch.insign.commons.db;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import play.db.Database;
import play.db.Databases;

import java.sql.Connection;

import static org.junit.Assert.assertTrue;

/**
 * Runs tests to prove the database connection stability
 */
public class DatabaseConnectionTest {

    private Database database;

    @Before
    public void setupDatabase() {
        database = Databases.inMemoryWith("jndiName", "DefaultDS");
    }

    @After
    public void shutdownDatabase() {
        database.shutdown();
    }

    @Test
    public void testIsDatabaseConnectionReached() throws Exception {
        try(Connection connection = database.getConnection()) {
            connection.getCatalog();

            assertTrue(connection.getMetaData().getDatabaseProductName().contains("H2"));
            assertTrue(connection.getMetaData().getSchemas().next());
        }
    }

}
