package ch.insign.commons.db;

import ch.insign.commons.db.util.TestDatabase;
import org.junit.Rule;
import org.junit.Test;
import play.db.jpa.JPAApi;

import javax.persistence.EntityManager;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class EntityManagerTest {

    @Rule
    public TestDatabase db = new TestDatabase();

    @Test
    public void testShouldReuseEntityManagerWhenExecutingTransaction() {
        JPAApi api = db.jpa;
        boolean reused = api.withTransaction(entityManager -> {
            EntityManager fromContext = api.em();
            assertNotNull(fromContext);

            return fromContext == entityManager;
        });

        assertTrue(reused);
    }

}
