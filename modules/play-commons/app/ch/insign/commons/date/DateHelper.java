/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.commons.date;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by uhon on 07.07.14.
 */
public class DateHelper {
	/**
	 * Convert a java.util.date into ISO 8601 string format.
	 *
	 * @param date
	 * @return ISO 8601 formatted date: yyyy-MM-dd'T'HH:mm
	 */
	public static String DateToISO(Date date) {
		return dateTimeFormat(date, "yyyy-MM-dd'T'HH:mm");
	}

	/**
	 * Convert a java.util.date to swiss date Format
	 *
	 * @param date
	 * @return formatted date: dd.MM.yyyy
	 */
	public static String dateFormatSwiss(Date date) {
		return dateTimeFormat(date, "dd.MM.yyyy");
	}

	/**
	 * Convert a java.util.date to swiss date Format including Time
	 *
	 * @param date
	 * @return formatted date: dd.MM.yyyy - HH:mm:ss
	 */
	public static String dateTimeFormatSwiss(Date date) {
		return dateTimeFormat(date, "dd.MM.yyyy - HH:mm:ss");
	}

	/**
	 * Convert a java.util.date to swiss date Format including Time
	 *
	 * @param date
	 * @return formatted date: dd.MM.yyyy - HH:mm:ss
	 */
	public static String dateTimeFormatSwissMinutes(Date date) {
		return dateTimeFormat(date, "dd.MM.yyyy - HH:mm");
	}

	/**
	 * Convert a java.util.date to swiss date Format including Time
	 *
	 * @param date
	 * @return formatted date: dd.MM.yyyy - HH:mm:ss
	 */
	public static String dateTimeFormat(Date date, String pattern) {
		if (date == null) {
			return "";
		}
		SimpleDateFormat simpleDateTimeFormat = new SimpleDateFormat(pattern);

		return simpleDateTimeFormat.format(date);
	}

}
