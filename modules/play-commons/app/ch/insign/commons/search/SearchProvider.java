/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.commons.search;

import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

import java.util.List;

/**
 * Interface for search providers.
 */
public interface SearchProvider {

    /**
     * Execute a search and return the results.
     * The provider will get a list of already added results and can then add his own or
     * modify the list as desired (e.g. replacing generic by more specific results)
     *
     * @param query
     * @param results the list of already found results (used e.g. to calculate paging / limits)
     * @return the updated list of results
     */
    public List<SearchResult> search(SearchQuery query, List<SearchResult> results);

    /**
     * The search provider was attached to the search manager
     */
    public void attach();

    /**
     * The search provider was removed from the search manager
     */
    public void detach();

    /**
     * Add or update index
     * @param id the page's id
     */
    public void updateIndex (String id);

    /**
     * Remove from Index
     * @param id the page's id
     */
    public void removeFromIndex (String id);

    /**
     * Empty and re-fill the search index
     */
    public void rebuildIndex();

}
