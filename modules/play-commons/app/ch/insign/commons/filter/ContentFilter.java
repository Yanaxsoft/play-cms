/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.commons.filter;

import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

import java.util.List;

/**
 * Base class for content filters
 * @author bachi
 */
public abstract class ContentFilter  {
	private final static Logger logger = LoggerFactory.getLogger(ContentFilter.class);

    protected FilterManager filterManager;

    public FilterManager getFilterManager() {
        return filterManager;
    }

    public void setFilterManager(FilterManager filterManager) {
        this.filterManager = filterManager;
    }


    public abstract String[] filterTags();

    /**
     * Process a tag of the form [[myKey:param1:param2:paramN]] before output to the client
     *
     * @param tag the full tag
     * @param params the parameters as specified in the tag
     * @return text replacement for the tag or null to indicate no modification
     */
    public String processTagOutput(String tag, List<String> params, Filterable source) {
        return null;
    }

    /**
     * * Process a tag of the form [[myKey:param1:param2:paramN]] on input, before persisting it.
     *
     * @param tag
     * @param params
     * @return
     */
    public String processTagInput(String tag, List<String> params, Filterable source) {
        return null;
    }

    /**
     * Process the html output before it is passed to the client.
     * Note: To optimize performance, you should prefer using managed tags and processTag() if you can.
     *
     * @param output
     * @return modified output or null to indicate no modification
     */
    public String processOutput(String output, Filterable source) {
        return null;
    }

    /**
     * Process the input before it is persisted.
     * @param input
     * @param source The input's source / context
     * @return modified input or null to indicate no modification
     */
    public String processInput(String input, Filterable source) {
        return null;
    }

    /**
     * Normal filters are applied before caching to optimize performance. This means
     * these filters are not re-executed when serving a cached page. Set to true if
     * you want the filter to be applied after caching and for every cached request.
     * TODO: Caching :) If external caching is used, this might not be applicable.
     *
     * @return
     */
    public boolean filterAfterCache() {
        return false;
    }

}
