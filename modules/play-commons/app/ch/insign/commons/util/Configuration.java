/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.commons.util;

import com.google.inject.Inject;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import play.mvc.Controller;

import java.util.Arrays;
import java.util.List;


/**
 * Manages play config values (from application.conf) in a more safe and easy-to-use way.
 *
 * Settings are configured in the application.conf file using the given config namespace, eg. "mymodule." prefix.
 * Upon instantiation, the test() method is called which should check (call) all defined values to ensure completeness
 * and to prevent later runtime errors.
 */
public abstract class Configuration  {
	private final static Logger logger = LoggerFactory.getLogger(Configuration.class);

    @Inject
    private static play.Configuration globalConfig;

    /** internal playConfig holder - always access through getConfig() **/
    private Config playConfig = null;

    /**
     * Constructor - checks for missing playConfig file values on instantiation, as they're
     * inherently a source of runtime errors.
     */
    public  Configuration() {

        try {
            playConfig = ConfigFactory.load().getConfig(configNamespace());
        } catch (Exception e) {
            logger.warn("No config values found for " + configNamespace());
        }

        try {
            test();

        } catch (Exception e) {
            throw new RuntimeException("Missing '" + configNamespace() + "' configuration values in application.conf: " + e.getMessage(), e);
        }
    }

    protected abstract String configNamespace();

    /**
     * Called on instantiation. Add a call to each of your config getter methods in here
     * to ensure they're available.
     */
    protected abstract void test();

    /**
     * Get the cms playConfig object (that is, config values that start with configNamespace(), e.g. "myModule.").
     * E.g .a config value of "myModule.enjoyLife=true" would be accessed like this:
     * getConfig().getBoolean("enjoyLife");
     *
     * Do not access application.conf settings from outside directly, provide a getter for each setting.
     *
     * @return a Typesafe Config object for the "cms." settings.
     */
    protected Config getConfig() {
        return playConfig;
    }

    /**
     * Retrieve base URL or fallbackUrl if current host cannot be determined
     * @deprecated The application.baseUrl might only make sense for tests since we have multi-site capabilities now
     */
    public static String resolveBaseUrl(String fallbackUrl) {
        String baseUrl;
        try {
            baseUrl = "http://" + Controller.request().host();
        } catch (RuntimeException e) {
            baseUrl = globalConfig.getString("application.baseUrl");
        }
        return baseUrl != null ? baseUrl : fallbackUrl;
    }

    // Some static helpers for directly getting config values with defaults if not set

    public static String getOrElse(String key, String defaultValue) {
        if (ConfigFactory.load().hasPath(key)) {
            return ConfigFactory.load().getString(key);
        } else {
            return defaultValue;
        }
    }

    public static boolean getOrElse(String key, boolean defaultValue) {
        if (ConfigFactory.load().hasPath(key)) {
            return ConfigFactory.load().getBoolean(key);
        } else {
            return defaultValue;
        }
    }

    public static int getOrElse(String key, int defaultValue) {
        if (ConfigFactory.load().hasPath(key)) {
            return ConfigFactory.load().getInt(key);
        } else {
            return defaultValue;
        }
    }

    public static List<String> getOrElse(String key, String... defaultValue) {
        if (ConfigFactory.load().hasPath(key)) {
            return ConfigFactory.load().getStringList(key);
        } else {
            return Arrays.asList(defaultValue);
        }
    }
}
