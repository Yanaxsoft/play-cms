/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.commons.util;

import org.apache.commons.codec.digest.DigestUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import play.mvc.Result;
import play.twirl.api.Content;

import static play.mvc.Controller.request;
import static play.mvc.Controller.response;
import static play.mvc.Http.HeaderNames.ETAG;
import static play.mvc.Results.notFound;
import static play.mvc.Results.ok;
import static play.mvc.Results.status;


/**
 * Class to create thumbnails
 */
public class Etag {
	private final static Logger logger = LoggerFactory.getLogger(Etag.class);

    public static Result resultWithETag(Content content) {
        if (content == null)  {
            return notFound("Given Content was not found");
        }

        String eTag = DigestUtils.md5Hex(content.body());

        String ifNoneMatch = request().getHeader("If-None-Match");
        if (ifNoneMatch != null && ifNoneMatch.equals(eTag)) {
            return status(304);
        }

        response().setHeader(ETAG, eTag);
        return ok(content);
    }


}
