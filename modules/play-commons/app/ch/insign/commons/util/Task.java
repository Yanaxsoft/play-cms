/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.commons.util;

import ch.insign.commons.db.Model;
import com.uaihebert.model.EasyCriteria;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import play.twirl.api.Html;

import javax.persistence.*;
import javax.persistence.criteria.*;
import javax.validation.ValidationException;
import java.io.Serializable;
import java.time.Instant;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

/**
 * Abstract Task class to create queueable persistent tasks.
 * Use it with the TaskQueue
 *
 * @author bachi
 */
@Entity
@Table(name = "cmn_tasks", indexes = {
		@Index(name = "task_type", columnList = "task_type"),
		@Index(name = "tag", columnList = "tag"),
		@Index(name = "created", columnList = "created"),
		@Index(name = "finished", columnList = "finished"),
		@Index(name = "queue_name", columnList = "queueName")})
@Inheritance(strategy= InheritanceType.JOINED)
@DiscriminatorColumn(name="task_type")
@NamedQueries(value = {
		@NamedQuery(
				name = "Task.findExpiredByType",
				query = "SELECT t FROM Task t WHERE t.tag = :taskType AND t.created <= :deadline"),
        @NamedQuery(
                name = "Task.findExpiredByType.count",
                query = "SELECT COUNT(t) FROM Task t WHERE t.tag = :taskType AND t.created <= :deadline")

})
public abstract class Task extends Model implements Serializable, Comparable{
	private final static Logger logger = LoggerFactory.getLogger(Task.class);

	public static enum ReviewStatus {
		NEW, IN_PROGRESS, CLOSED
	}

	public static enum Level {
		TRACE, DEBUG, INFO, WARN, ERROR
	}

	public static TaskFinder<Task> find = new TaskFinder<>(Task.class);

	protected String queueName;

	protected Instant created;
	protected Instant firstTry;
	protected Instant lastTry;
	private boolean finished;
	private boolean success;
	protected Instant scheduleAfter;
	protected int tries;
	protected int maxTries = 3;
	protected int priority = 5; // (1 = highest)
	protected int retryWaitPeriod = 0; //sec

	@Enumerated(EnumType.STRING)
	protected ReviewStatus reviewStatus = ReviewStatus.NEW;

	private int warnings = 0;
	private int errors = 0;

	@Lob
	protected String log;

	@Lob
	protected String note;

	protected String tag = getClass().getSimpleName();

	@Transient
	long queueId;

	private boolean isVisible = true;


	/**
	 * Place your task's code here. This method will get called from the TaskQueue
	 * when your task is to be executed.
	 * Note: The call is wrapped within JPA.withTransaction, so you can safely rollback if you need.
	 *
	 * @return true if the task was successfully executed (will be marked as finished), false otherwise.
	 */
	public abstract boolean execute();

	/**
	 * Called when the task permanently failed and is not being re-queued.
	 */
	public abstract void failed();

	/**
	 * Overwrite this to add a custom html section to the task detail view backend page.
	 * @return html partial if available
	 */
    public Html reportTemplatePartial() {
	    return Html.apply("");
    }

    /**
     * Delete expired tasks from the db. Calls task.delete() on each instance which you can overwrite to
     * react upon deletion.
     *
     * @param taskClass The task class to delete expired tasks from
     * @param dayCount  Delete taks older than dayCount days
     */
    public static void deleteExpired(Class taskClass, int dayCount) {

        int maxResult = 100; // Limit count for receipt of objects from the database

        Instant expiredDate = Instant.now().minus(dayCount, ChronoUnit.DAYS);
        Long counter = Task.find.findExpiredByTypeCount(taskClass, expiredDate);

        // Need to batch-process tasks here in order not to overload the memory with task instance,
        // and to avoid having a giant cache eating memory.
        if (counter != null) {
            while (counter > 0) {
                List<Task> tasks = Task.find.findExpiredByType(taskClass, expiredDate).setMaxResults(maxResult).getResultList();

                for (Task task : tasks) {
                    logger.info("Deleting" + taskClass.getSimpleName() + " [id = " + task.getId() + "]");
                    task.delete();
                }

                //Flush a batch of DML operations and release memory:
                jpaApi.em().flush();
                //Clearing the entity manager empties its associated cache, forcing new database queries to be executed later in the transaction
	            jpaApi.em().clear();

                counter = Task.find.findExpiredByTypeCount(taskClass, expiredDate);
            }
        }
    }

	public boolean isVisible() {
		return isVisible;
	}

	public void setVisible(boolean visible) {
		isVisible = visible;
	}

    public Instant getCreated() {
		return created;
	}

	public void setCreated(Instant created) {
		this.created = created;
	}

	public Instant getFirstTry() {
		return firstTry;
	}

	public void setFirstTry(Instant firstTry) {
		this.firstTry = firstTry;
	}

	public Instant getLastTry() {
		return lastTry;
	}

	public void setLastTry(Instant lastTry) {
		this.lastTry = lastTry;
	}

	public int getTries() {
		return tries;
	}

	public void setTries(int tries) {
		this.tries = tries;
	}

	public boolean retry() {
		return tries < getMaxTries();
	}

	public int getPriority() {
		return priority;
	}

	public void setPriority(int priority) {
		this.priority = priority;
	}

	public String getTag() {
		return tag;
	}

	public void setTag(String tag) {
		this.tag = tag;
	}

	public int getMaxTries() {
		return maxTries;
	}

	public void setMaxTries(int maxTries) {
		this.maxTries = maxTries;
	}

	String getQueueName() {
		return queueName;
	}

	void setQueueName(String queueName) {
		this.queueName = queueName;
	}

	public boolean isFinished() {
		return finished;
	}

	public void setFinished(boolean finished) {
		this.finished = finished;
		onUpdateStatus();
	}

	/**
	 * Was this task finished successfully?
	 */
	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
		onUpdateStatus();
	}

	public Instant getScheduleAfter() {
		return scheduleAfter;
	}

	public void setScheduleAfter(Instant scheduleAfter) {
		this.scheduleAfter = scheduleAfter;
	}

	public int getRetryWaitPeriod() {
		return retryWaitPeriod;
	}

	public void setRetryWaitPeriod(int retryWaitPeriod) {
		this.retryWaitPeriod = retryWaitPeriod;
	}

	public String getLog() {
		if (log==null) setLog("");
		return log;
	}

	public void setLog(String log) {
		this.log = log;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	/**
	 * Were there any warnings added to the log?
	 */
	public boolean hasWarnings() {
		return warnings > 0;
	}

	/**
	 * Were there any errors added to the log?
	 */
	public boolean hasErrors() {
		return errors > 0;
	}

	public ReviewStatus getReviewStatus() {
		return reviewStatus;
	}

	public void setReviewStatus(ReviewStatus reviewStatus) {
		this.reviewStatus = reviewStatus;
	}

	/**
	 * Add a timestamped log entry to the text-log
	 * @param msg the message to add to the log
	 */
	public void addLogEntry(String msg) {

		setLog(new StringBuilder()
			.append(" --- ")
			.append(Instant.now().atZone(ZoneId.systemDefault()).toString())
			.append(" ---\n")
			.append(msg)
			.append("\n\n")
			.append(getLog())
			.toString());
	}

	/**
	 * Add a timestamped log entry to the text-log and output to the current logger
	 * @param msg the message to add to the log
	 */
	public void addLogEntry(String msg, Level logLevel) {
		addLogEntry(msg);
		switch (logLevel) {
			case TRACE: logger.trace(msg);
				break;
			case DEBUG: logger.debug(msg);
				break;
			case INFO: logger.info(msg);
				break;
			case WARN: logger.warn(msg);
				warnings++;
				break;
			case ERROR: logger.error(msg);
				errors++;
				break;
		}
	}

	/**
	 * Add a timestamped log entry to the text-log and output to the current logger
	 * @param msg the message to add to the log
	 */
	public void addLogEntry(String msg, Level logLevel, Throwable e) {

		msg = Util.appendConstraintViolations(msg, e);

		switch (logLevel) {
			case TRACE: logger.trace(msg, e);
				break;
			case DEBUG: logger.debug(msg, e);
				break;
			case INFO: logger.info(msg, e);
				break;
			case WARN: logger.warn(msg, e);
				msg = "WARNING: " + msg;
				warnings++;
				break;
			case ERROR: logger.error(msg, e);
				msg = "ERROR: " + msg;
				errors++;
				break;
		}
		addLogEntry(msg + "\n\n" + e.getMessage());
	}


	@Override
	public String toString() {
		return String.format("%s #%s [%d/%d, first: %s, age: %s m]", getTag(), getId(), getTries(), getMaxTries(), (firstTry == null) ? "--" : firstTry.toString(), (firstTry == null || lastTry == null) ? "--" : String.valueOf(ChronoUnit.MINUTES.between(firstTry, lastTry)));
	}

	/**
	 * Tasks are ordered 1st by their scheduled execution time and 2nd by their priority and 3rd by ther queue position
	 */
	@Override
	public int compareTo(Object other) {

		Instant myTime = getScheduleAfter() == null ? Instant.ofEpochSecond(0) : getScheduleAfter();
		Instant otherTime = ((Task) other).getScheduleAfter() == null ? Instant.ofEpochSecond(0) : ((Task) other).getScheduleAfter();

		if (myTime.isAfter(otherTime))  return 1;
		if (myTime.isBefore(otherTime))  return -1;

		if (priority > ((Task) other).priority) return 1;
		if (priority < ((Task) other).priority) return -1;

		if (queueId > ((Task) other).queueId) return 1;
		if (queueId < ((Task) other).queueId) return -1;

		return 0;
	}

	/**
	 * Updates task on each status change
	 */
	public void onUpdateStatus() {

		if (isFinished()) {
			setScheduleAfter(null);
		}

		if (isFinished() && isSuccess() && !hasErrors()) {
			setReviewStatus(ReviewStatus.CLOSED);

		// if status is IN_PROGRESS (manually set), don't change it back to new
		} else if (getReviewStatus() != ReviewStatus.IN_PROGRESS) {
			setReviewStatus(ReviewStatus.NEW);
		}
	}

	/**
	 * Called before a failed task is re-added to the queue.
	 * By default sets scheduleAfter = now + retryWaitPeriod
	 */
	public void onRequeue() {
		if (getRetryWaitPeriod() > 0) {
			setScheduleAfter(Instant.now().plusSeconds(getRetryWaitPeriod()));
		}
	}

	/**
	 * Called when the task initially was added to the queue.
	 */
	public void onEnqueued() {}

	/**
	 * Override to implement task validation.
	 * Tasks that do not validate cannot be added to the queue.
	 * Throw a ValidationException if the validation should fail.
	 */
	public void validate() throws ValidationException {}


	/**
	 * Provides utility methods with Java-8 support.
	 * This is a workaround for EclipseLink's broken support of Java-8.
	 */
	private static class Util {

		/**
		 * Returns the original message with optionally appended embedded ConstraintViolations
		 * if the given throwable is ConstraintViolationException.
		 *
		 * The single space is used to separate the given message and appended ConstraintViolations.
		 */
		public static String appendConstraintViolations(String message, Throwable throwable) {
			return ConstraintViolationFormat.formatIfConstraintViolationException(throwable)
					.map(cve -> message + "\n\n" + cve)
					.orElse(message);
		}
	}

	public static class TaskFinder<T extends Task> extends Model.Finder<T> {
		private final static Logger logger = LoggerFactory.getLogger(TaskFinder.class);
		public final static int ALL_TASK = -1;
		public final static int OPEN_TASK = 0;
		public final static int FINISHED_TASK = 1;
		public final static int FAILED_TASK = 2;


		public TaskFinder(Class<T> tClass) {
			super(tClass);
		}

		/**
		 * Allows us retrieve only visible tasks
		 * {@see https://jira.insign.ch/browse/RASCHSLA-403}
         */
		public EasyCriteria<T> query() {
			return super.query().andEquals("isVisible", true);
		}

		/**
		 * Find all tasks of this queue
		 */
		public List<T> byQueue(String queueName) {

			EasyCriteria<T> query = query();
			try {
				query = query.andEquals("queueName", queueName);
				return query.getResultList();

			} catch (Exception e) {

				e.printStackTrace();
				logger.error(e.getMessage(), e);
				return new ArrayList<T>();
			}
		}

		/**
		 * Find all unfinished tasks in the given queue.
		 */
		public List<T> queueRestart(String queueName) {

			EasyCriteria<T> query = query();
			try {
				query = query
						.andEquals("queueName", queueName)
						.andEquals("finished", false);

				return query.getResultList();

			} catch (Exception e) {

				e.printStackTrace();
				logger.error(e.getMessage(), e);
				return new ArrayList<T>();
			}
		}

		public  TypedQuery<Task> findExpiredByType(Class taskType, Instant deadline) {
			return jpaApi.em().createNamedQuery("Task.findExpiredByType", Task.class)
					.setParameter("taskType", taskType.getSimpleName())
					.setParameter("deadline", deadline);
		}

		public  Long findExpiredByTypeCount(Class taskType, Instant deadline) {
			return jpaApi.em().createNamedQuery("Task.findExpiredByType.count", Long.class)
					.setParameter("taskType", taskType.getSimpleName())
					.setParameter("deadline", deadline).getSingleResult();
		}

		public List<String> findAllTags() {
			return jpaApi.em().createQuery("SELECT DISTINCT t.tag FROM Task AS t", String.class).getResultList();
		}

		public EasyCriteria<T> easyGetAllBySortingAndFiltersParametrs(String sortBy, String order, String filterByTag, int filterFinished, String reviewStatusString) {
			Task.ReviewStatus reviewStatus = null;
			try {
				reviewStatus = Task.ReviewStatus.valueOf(reviewStatusString);
			} catch (Exception e) {}

			EasyCriteria<T> query = query();
			if (StringUtils.isNotBlank(filterByTag)) {
				query.andEquals("tag", filterByTag);
			}
			if (filterFinished == 1) {
				query.andEquals("finished", true);
			}
			if (filterFinished == 0) {
				query.andEquals("finished", false);
			}

			if (filterFinished == FAILED_TASK) {
				query.andEquals("finished", true);
				query.andEquals("success", false);
			}

			if (reviewStatus != null) {
				query.andEquals("reviewStatus", reviewStatus);
			}

			if (StringUtils.isBlank(sortBy)) {
				query.orderByDesc("created").orderByDesc("finished");
			} else {
				if (order.equals("asc")) {
					query.orderByDesc(sortBy);
				} else {
					query.orderByAsc(sortBy);
				}
			}
			return query;

		}

		public List<T> easyGetListAllBySortingAndFiltersParametrs(String sortBy, String order,String filterByTag, int filterFinished, String reviewStatus) {
			return easyGetAllBySortingAndFiltersParametrs(sortBy, order, filterByTag, filterFinished, reviewStatus).getResultList();
		}


		/**
		 * Find all by sorting and filtering parameters
		 */
		public TypedQuery<T> getAllBySortingAndFiltersParametrs(String sortBy, String order,String filterByTag, int filterFinished) {

			CriteriaBuilder builder = jpaApi.em().getCriteriaBuilder();
			CriteriaQuery<T> query = builder.createQuery(getEntityClass());
			Root<T> tasks = query.from(getEntityClass());

			List<Predicate> conditions = new ArrayList<>();
			Predicate tagPredicate, finishedPredicate;

			if(filterByTag != null && !filterByTag.isEmpty()) {
				tagPredicate = builder.like(
						builder.lower(tasks.get("tag")),
						"%" + filterByTag.toLowerCase() + "%");
				conditions.add(tagPredicate);
			}

			if(filterFinished == 0 || filterFinished == 1) {
				finishedPredicate = builder.equal(tasks.get("finished"), filterFinished);
				conditions.add(finishedPredicate);
			}
			@SuppressWarnings("rawtypes")
			Expression orderByFieldExpression;

			if (sortBy.isEmpty() && order.isEmpty()) {
				return jpaApi.em().createQuery(query
						.select(tasks)
						.where(
								builder.and(
										conditions.toArray(new Predicate[]{})
								)
						)
						.orderBy(builder.asc(tasks.get("finished")), builder.desc(tasks.get("created")))
				);

			}

			orderByFieldExpression = tasks.get(sortBy);
			return jpaApi.em().createQuery(query
					.select(tasks)
					.where(
							builder.and(
									conditions.toArray(new Predicate[]{})
							)
					)
					.orderBy(order.toLowerCase().equals("desc")
							? builder.desc(orderByFieldExpression)
							: builder.asc(orderByFieldExpression))
			);
		}

	}
}
