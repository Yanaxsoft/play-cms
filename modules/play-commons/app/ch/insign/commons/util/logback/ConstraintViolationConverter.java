/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.commons.util.logback;

import ch.insign.commons.util.ConstraintViolationFormat;
import ch.qos.logback.classic.pattern.ThrowableHandlingConverter;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.classic.spi.ThrowableProxy;
import ch.qos.logback.core.CoreConstants;

import java.util.Optional;

/**
 * See http://logback.qos.ch/manual/layouts.html#customConversionSpecifier
 *
 * Example usage:
 *
 *  <conversionRule conversionWord="cve" converterClass="util.logback.CveConverter" />
 *
 */
public class ConstraintViolationConverter extends ThrowableHandlingConverter {
	@Override
	public String convert(ILoggingEvent event) {
		ThrowableProxy proxy = (ThrowableProxy) event.getThrowableProxy();
		return Optional.ofNullable(proxy)
				.flatMap(p -> ConstraintViolationFormat.formatIfConstraintViolationException(p.getThrowable()))
				.map(str -> CoreConstants.LINE_SEPARATOR + str + CoreConstants.LINE_SEPARATOR)
				.orElse(CoreConstants.EMPTY_STRING);
	}
}
