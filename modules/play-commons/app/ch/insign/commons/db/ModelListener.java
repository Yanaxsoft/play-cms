/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.commons.db;

import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

/**
 * Listener for model JPA events.
 * @author bachi
 */
public class ModelListener<T extends Model> {
    public void onPrePersist(T model) {}
    public void onPreUpdate(T model) {}
    public void onPreRemove(T model) {}
    public void onPostPersist(T model) {}
    public void onPostUpdate(T model) {}
    public void onPostRemove(T model) {}
}
