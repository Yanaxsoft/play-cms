/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.commons.db;

import ch.insign.commons.filter.FilterManager;
import ch.insign.commons.filter.Filterable;
import ch.insign.commons.i18n.Language;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import play.db.jpa.JPA;

import javax.persistence.*;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;
import java.util.HashMap;
import java.util.Map;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Entity
@Table(name = "cmn_mstring")
public class MString extends Model implements Filterable  {

    @Target({FIELD})
    @Retention(RUNTIME)
    public static @interface MStringRequiredForVisibleTab {
        String message() default REQUIRED_VISIBLE_MESSAGE;
        // validation implemented in MStringDataBinder
    }

    @Target({FIELD})
    @Retention(RUNTIME)
    public static @interface MStringRequired {
        String message() default REQUIRED_MESSAGE;
        // validation implemented in MStringDataBinder
    }

    public final static String REQUIRED_MESSAGE = "mstring.errors.required";
    public final static String REQUIRED_VISIBLE_MESSAGE = "mstring.errors.required.visible";


	private final static Logger logger = LoggerFactory.getLogger(MString.class);

	public static Finder<MString> find = new Finder<>(MString.class);
    protected static FilterManager filterManager = null;

    @Lob
    @ElementCollection
    @MapKeyColumn(name="LANGUAGE_KEY")
    @Column(name="TEXT")
    @CollectionTable(name="cmn_mstring_translations", joinColumns=@JoinColumn(name="MSTRING_ID"))
	private Map<String,String> translations = new HashMap();

	public void set(String languageCode, String text) {
        if (text != null) {
            translations.put(languageCode, text);
        } else {
            if (translations.containsKey(languageCode)) translations.remove(languageCode);
        }

	}

    /**
     * * Get the translated string. Will return a non-null empty string ("") if not set.
     * @param languageCode
     * @return
     */
	public String get(String languageCode) {
        return get(languageCode, false);
	}

    /**
     * Get the translated string. Will return a non-null empty string ("") if not set.
     * @param languageCode
     * @param fallback fall back to the default language if there is no translation to the current language?
     * @return
     */
    public String get(String languageCode, boolean fallback) {
        if(translations.containsKey(languageCode)) {
            return translations.get(languageCode);
        }  else {
            if(fallback && translations.containsKey(Language.getDefaultLanguage())) {
                return translations.get(Language.getDefaultLanguage());
            } else {
                return "";
            }
        }
    }

    /**
     * Get the currently used filter manager
     * @return
     */
    public static FilterManager getFilterManager() {
        return filterManager;
    }

    /**
     * Set the filter manager that MString should use.
     * @param filterManager
     */
    public static void setFilterManager(FilterManager filterManager) {
        MString.filterManager = filterManager;
    }

    @PrePersist
    @PreUpdate
    @Override
    public void filterInputData() {

        // This can happen within or outside of a bound EM
        // e.g. when flushing inside the controller we're inside
        // but when at the end of the request we seem to be outside of that transaction(?)
        boolean haveBoundEM;
        try {
            JPA.em();
            haveBoundEM=true;

        } catch (Exception e) {
            haveBoundEM=false;
        }

        if (haveBoundEM) {
            filterInputDataDo();
        } else {
            this.filterInputDataDo();
        }
    }

    private void filterInputDataDo() {
        if (getFilterManager() != null) {
            for (String lang : translations.keySet()) {
                set(lang, getFilterManager().processInput(get(lang), MString.this));
            }
        }
    }

    @Override
    public void filterOutputData() {
        // MString does not do output filtering itself
        // as this is meant to be done on Controller level.
    }


    /**
     * Get the translated string for the current language.
     * Will return a non-null empty string ("") if not set.
     *
     * @return
     */
	public String get() {
        return get(Language.getCurrentLanguage(), true);
	}

    /**
     * Returns all String values in this MString.
     * @return All Strings.
     */
    public String[] all() {
        return translations.values().toArray(new String[translations.values().size()]);
    }

	@Override
	public String toString() {
		return get();
	}

    /**
     * Check if the required translation is non-empty
     */
    public boolean nonEmpty() {
        return !get().equals("");
    }

    /**
     * Check if the required translation is non-empty
     */
    public boolean nonEmpty(String language) {
        return !get(language).equals("");
    }

    /**
     * Check if the required translation is non-empty
     */
    public boolean nonEmpty(String language, boolean fallback) {
        return !get(language, fallback).equals("");
    }

    public Map<String,String> map() {
        HashMap<String,String> copy = new HashMap<>();
        copy.putAll(translations);
        return copy;
    }

	@PostUpdate
	private void clearJpaCache() {
        try {
            JPA.em().getEntityManagerFactory().getCache().evict(MString.class, getRawId());
        } catch (Exception e) {
            logger.info("EM not bound, skipping evicting cache for MString");
        }
	}

}
