/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.commons.db;

import com.google.common.collect.AbstractIterator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.Query;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

/**
 * Encapsulate the default(limit/offset) implementation of QueryStream
 * @param <T>
 */
public class DefaultQueryStream<T> implements QueryStream<T> {
    private final static Logger logger = LoggerFactory.getLogger(DefaultQueryStream.class);

    private final Query query;
    private final Class<T> type;
    private Map<String, Object> parameters = new HashMap<>();

    public DefaultQueryStream(Query query, Class<T> type) {
        Objects.requireNonNull(query, "Internal state of query can't be undefined");
        Objects.requireNonNull(query, "Internal state of type can't be undefined");

        this.query = query;
        this.type = type;
    }

    @Override
    public Stream<T> getResultStream() {
        Iterator<T> queryIterator = new AbstractIterator<T>() {
            final int BUFFER_SIZE = 2000;
            Iterator buffer;
            int offset = 0;
            Map<String, Object> params = new HashMap<>(parameters);

            @Override
            protected T computeNext() {
                if (buffer == null || !buffer.hasNext()) {
                    params.forEach(query::setParameter);
                    query.setFirstResult(offset);
                    query.setMaxResults(BUFFER_SIZE);

                    buffer = query.getResultList().iterator();

                    offset += BUFFER_SIZE;
                }

                return buffer.hasNext()
                        ? type.cast(buffer.next())
                        : endOfData();
            }
        };

        return StreamSupport.stream(toSpliterator(queryIterator), false);
    }

    @Override
    public <R> Optional<R> withResultStream(Function<Stream<T>, R> function) {
        try (Stream<T> s = getResultStream()) {
            return Optional.ofNullable(function.apply(s));
        }
    }

    @Override
    public QueryStream<T> setParameter(String name, Object value) {
        parameters.put(name, value);
        return this;
    }


    /**
     * Mutates an iterable instance to spliterator for build a new stream
     */
    private Spliterator<T> toSpliterator(Iterator<T> resultsIterator) {
        return Spliterators.spliteratorUnknownSize(
                resultsIterator,
                Spliterator.DISTINCT | Spliterator.NONNULL | Spliterator.CONCURRENT | Spliterator.IMMUTABLE
        );
    }

}
