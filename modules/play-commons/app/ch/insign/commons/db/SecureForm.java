/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.commons.db;

import com.typesafe.config.ConfigFactory;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.FormElement;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.yaml.snakeyaml.external.biz.base64Coder.Base64Coder;
import play.data.Form;
import play.data.format.Formatters;
import play.data.validation.ValidationError;
import play.i18n.MessagesApi;
import play.twirl.api.Html;

import java.io.*;
import java.net.URI;
import java.net.URISyntaxException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Use this class to secure forms against parameter tampering by
 * signing the parameter list of the form sent to the client.
 *
 * @author bachi
 */
public class SecureForm<T> extends Form<T> {

	private final static Logger logger = LoggerFactory.getLogger(SecureForm.class);

    public static final String SIG_FIELD_NAME = "_formsignature";
    public static final String SIG_FIELD_ADD_FIELDS = "_formsignature_additional";
    private static final String DELIM = "@";



    /*** Form superclass - related code ***/

    private final Class<T> backedType;

    public SecureForm(Class<T> clazz, MessagesApi messagesApi, Formatters formatters, javax.validation.Validator validator) {
        super(clazz, messagesApi, formatters, validator);
        backedType = clazz;
    }

    public SecureForm(String name, Class<T> clazz, MessagesApi messagesApi, Formatters formatters, javax.validation.Validator validator) {
        super(name, clazz, messagesApi, formatters, validator);
        backedType = clazz;
    }

    public SecureForm(String name, Class<T> clazz, Class<?> groups, MessagesApi messagesApi, Formatters formatters, javax.validation.Validator validator) {
        super(name, clazz, groups, messagesApi, formatters, validator);
        backedType = clazz;
    }

    public SecureForm(String rootName, Class<T> clazz, Map<String,String> data, Map<String,List<ValidationError>> errors, Optional<T> value, MessagesApi messagesApi, Formatters formatters, javax.validation.Validator validator) {
        super(rootName, clazz, data, errors, value, messagesApi, formatters, validator);
        backedType = clazz;
    }

    /*** End of superclass-related code. ***/


    public static String salt() {
        return ConfigFactory.load().getString("play.crypto.secret");
    }

    /**
     * Adds the formKey value to the contained forms.
     * (forms need to contain the @formKey() hidden field)
     */
    public static Html signForms(Html html) {
        return Html.apply(signForms(html.toString()));
    }

    /**
     * Adds the formKey value to the contained forms.
     * (forms need to contain the @formKey() hidden field)
     */
    public static String signForms (String html) {

        Document doc = Jsoup.parse(html);
        Elements elements = doc.getElementsByTag("form");
        List<FormElement> forms = elements.forms();

        for (FormElement form : forms) {

            Element signature = null;
            ArrayList<String> inputNames = new ArrayList<>();

            // Extract all GET param names from the form's action url
            try {
                List<NameValuePair> params = URLEncodedUtils.parse(new URI(form.attr("action")), "UTF-8");
                for (NameValuePair param : params) {
                    inputNames.add(param.getName());
                }
            } catch (URISyntaxException e) {
                logger.warn("signForms: invalid uri: " + e.getMessage());
            }


            // All form elements that submit data and contain a name attr
            Elements inputs = form.select("input[name], textarea[name], select[name], button[name]");
            for (Element input : inputs) {

                // Find the signature field
                if (SIG_FIELD_NAME.equals(input.attr("name"))) {
                    signature = input;
                    inputNames.addAll(extractAdditionalAllowedFields(input.attr("value")));
                }
                // Extract additional allowed fields if the separate hidden field is present, e.g.
                // E.g. <input type="hidden" name="_formSignature_additional" value="myAllowedField, anotherone, wild*">
                else if (SIG_FIELD_ADD_FIELDS.equals(input.attr("name"))) {
                    inputNames.addAll(extractAdditionalAllowedFields(input.attr("value")));
                }
                // Add any other field to the list
                else {
                    inputNames.add(input.attr("name"));
                }
            }

            if (signature != null) {
                String signatureValue = null;
                try {
                    signatureValue = generateFormKey(inputNames);
                    signature.attr("value", signatureValue);

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        String result = doc.toString();
        return result;
    }

    /**
     * Extract additional fields from a string
     */
    private static List<String> extractAdditionalAllowedFields(String allowedFields) {
        List<String> additional = new ArrayList<>();

        if (allowedFields != null && allowedFields.length() > 0) {
            String[] allowedFieldAr = allowedFields.split(",");
            for (String allowedField : allowedFieldAr) {
                additional.add(allowedField.trim());
            }
        }
        return additional;
    }

    public static List<String> decodeFormKey(String formSignature) throws InvalidKeyException {
        // Example:
        // rO0ABXNyABNqYXZhLnV0aWwuQXJyYXlMaXN0eIHSHZnHYZ0DAAFJAARzaXpleHAAAAAFdwQAAAAFdAALZGlzcGxheUZyb210AAlkaXNwbGF5VG90AAh0aXRsZS5kZXQACHRpdGxlLmZydAAIdGl0bGUuaWR4@rO0ABXQALkzvv715We+/vSkiT0Dvv73VhXFiWHrvv71v77+9TgEKJy4SIu+/vQ0F77+9NGg=

        int pos = formSignature.indexOf(DELIM);
        if (pos == -1) {
            throw new InvalidKeyException("SecureForm formKey validation error ("+DELIM+" missing)");
        }
        String payload = formSignature.substring(0, pos);
        String hash = formSignature.substring(pos + DELIM.length());

        if (payload == null || payload.equals("") || hash == null || hash.equals("")) {
            throw new InvalidKeyException("SecureForm formKey validation error (could not extract payload and hash)");
        }

        String hash2 = getHashFor(payload);

        if (!hash.equals(hash2)) {
            throw new InvalidKeyException("SecureForm formKey validation error.");
        }

        try {
            ArrayList<String> fieldList = (ArrayList<String>) fromString(payload);
            return fieldList;

        } catch (IOException | ClassNotFoundException e) {
            throw new InvalidKeyException("Could not deserialize form signature: " + e.getMessage(), e);
        }
    }

    /**
     * Bind data like Form<T>, but ensure that the form contains a valid formKey, which
     * prevents parameter tampering. Throws a runtime exception if the formKey is not valid.
     */
    @Override
    public Form<T> bind(Map<String,String> data, String ... allowedFields) {

        T object = blankInstance();

        // Validate the submitted form fields: Either by signed formKey or by passed allowedFields.
        try {
            SecureForm.validateSubmittedForm(data, allowedFields, object);

        } catch (InvalidKeyException e) {

            // We're strict about failed submissions.
            throw new RuntimeException(e);
        }

        return super.bind(data, allowedFields);
    }

    /**
     * Validate a form submission
     *
     * @param data key/value pairs of the submitted form data.
     * @param allowedFields additinal allowed fields defined in the code
     * @param targetObject the object to which the data should be mapped.
     *
     * @throws InvalidKeyException
     */
    public static void validateSubmittedForm(Map<String, String> data, String[] allowedFields, Object targetObject) throws InvalidKeyException {

        // Try to regenerate allowedFields from a _formKey field using SecureForm
        List<String> definedFormFields;
        if (data.containsKey(SIG_FIELD_NAME)) {
            try {
                definedFormFields = SecureForm.decodeFormKey(data.get(SIG_FIELD_NAME));

            } catch (InvalidKeyException e) {
                throw new InvalidKeyException("Form signature is invalid.", e);
            }

            // Merge allowed fields and fields from formKey
            if (allowedFields != null) {
                definedFormFields.addAll(Arrays.asList(allowedFields));
            }

            // Tbd: Should we merge allowedFields? Case against: Play does silently skip fields, which can
            // and does lead to various issues eg with MStrings.
            //allowedFields = (String[]) definedFormFields.toArray(new String[0]);

            // Create a regex from the passed field list
            // Convert * to .* and [] to [\d*] automatically
            List regexParts = new ArrayList();
            for (String definedFormField : definedFormFields) {
                regexParts.add(definedFormField
                        .trim()
                        .replace("*", ".*")
                        .replace("[]", "\\[\\d*\\]"));
            }
            String regex = StringUtils.join(regexParts, "|");
            Pattern pattern = Pattern.compile(regex);
            Matcher matcher = pattern.matcher("");

            // Throw an exception if an unexpected field was found
            for (String fieldName : data.keySet()) {
                matcher.reset(fieldName);
                if (!matcher.matches()
                        && !fieldName.equals(SIG_FIELD_NAME)
                        && !fieldName.equals(SIG_FIELD_ADD_FIELDS)) {

                    //logger.warn("SecureForm: Submitted field is not valid: " + fieldName + ". Add it to allowedFields in .bindFromRequest() to get rid of this message.");
                    throw new InvalidKeyException("SecureForm: Submitted field is not valid: " + fieldName);
                }
            }

        } else {

            // No formKey found. For security reasons, we do not allow to bind forms to Model entities directly
            // without defining the allowed fields (to prevent parameter tampering problems)
            if (targetObject instanceof Model && (allowedFields == null || allowedFields.length == 0)) {
                StringBuilder codeline = new StringBuilder();
                codeline.append("String allowedFields[] = {");
                for (String key : data.keySet()) {
                    codeline.append("\"").append(key).append("\", ");
                }
                codeline.append("};");
                throw new InvalidKeyException("Trying to map a form to an entity without defining allowedFields or using a formKey is not allowed. To allow the submitted form, add: " + codeline.toString().replace(", }", "}"));
            }
        }
    }


    /**
     * Generate an automatic form field signature
     *
     * @param inputNames the list of allowed field names
     * @return the form key
     */
    public static String generateFormKey(List<String> inputNames) throws IOException {
        Collections.sort(inputNames);
        String fieldString = toString(new ArrayList<String>(inputNames));
        return fieldString + DELIM + getHashFor(fieldString);
    }

    private static String getHashFor(String input) {

        try {
            MessageDigest messageDigest = MessageDigest.getInstance("SHA-256");
            messageDigest.update((input+salt()).getBytes());
            String sig = toString(new String(messageDigest.digest()));
            return sig;

        } catch (Exception e) {
            throw new RuntimeException("SecureForm: Could not generate hash value: " + e.getMessage(), e);
        }
    }

    /** Read the object from Base64 string. */
    private static Object fromString( String s ) throws IOException,
            ClassNotFoundException {
        byte [] data = Base64Coder.decode(s);
        ObjectInputStream ois = new ObjectInputStream(
                new ByteArrayInputStream(  data ) );
        Object o  = ois.readObject();
        ois.close();
        return o;
    }

    /** Write the object to a Base64 string. */
    private static String toString( Serializable o ) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ObjectOutputStream oos = new ObjectOutputStream( baos );
        oos.writeObject( o );
        oos.close();
        return new String( Base64Coder.encode( baos.toByteArray() ) );
    }
}
