/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.commons.db;

import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * A unified event handler for all JPA model events.
 * See http://www.objectdb.com/java/jpa/persistence/event for JPA event details.
 * @author bachi
 */
public class ModelEvents  {
	private final static Logger logger = LoggerFactory.getLogger(ModelEvents.class);

    private static class Entry  {
        Class<? extends Model> clazz;
        ModelListener<Model> listener;

        Entry(Class<? extends Model> clazz, ModelListener<Model> listener) {
            this.clazz = clazz;
            this.listener = listener;
        }
    }

    //private static HashMap<Class<? extends Model>, List<ModelListener<Model>>> listeners = new HashMap<>();
    private static List<Entry> listeners = Collections.synchronizedList(new ArrayList<Entry>());

    /**
     * Add an event listener for a specific model class
     * @param clazz
     * @param listener
     */
    public static void addListener(Class clazz, ModelListener<Model> listener) {
        listeners.add(new Entry(clazz, listener));
    }

    public static void removeListener(Class clazz, ModelListener<Model> listener) {
        List<Entry> removals = new ArrayList<>();
        for (Entry entry : listeners) {
            if (entry.clazz.equals(clazz) && entry.listener.equals(listener)) {
                removals.add(entry);
            }
        }
        listeners.removeAll(removals);
    }

    /** Events, called from Model (package-visibility intended) **/

    static void onPrePersist(Model m) {
        for (Entry entry : listeners) {
            if (entry.clazz.isInstance(m)) {
                entry.listener.onPrePersist(m);
            }
        }
    }

    static void onPreUpdate(Model m) {
        for (Entry entry : listeners) {
            if (entry.clazz.isInstance(m)) {
                entry.listener.onPreUpdate(m);
            }
        }
    }

    static void onPreRemove(Model m) {
        for (Entry entry : listeners) {
            if (entry.clazz.isInstance(m)) {
                entry.listener.onPreRemove(m);
            }
        }
    }

    static void onPostPersist(Model m) {
        for (Entry entry : listeners) {
            if (entry.clazz.isInstance(m)) {
                entry.listener.onPostPersist(m);
            }
        }
    }

    static void onPostUpdate(Model m) {
        for (Entry entry : listeners) {
            if (entry.clazz.isInstance(m)) {
                entry.listener.onPostUpdate(m);
            }
        }
    }

    static void onPostRemove(Model m) {
        for (Entry entry : listeners) {
            if (entry.clazz.isInstance(m)) {
                entry.listener.onPostRemove(m);
            }
        }
    }

}
