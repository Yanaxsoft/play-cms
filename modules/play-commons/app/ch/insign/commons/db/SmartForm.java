/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.commons.db;

import org.apache.commons.lang3.ArrayUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanWrapper;
import org.springframework.beans.BeanWrapperImpl;
import org.springframework.beans.InvalidPropertyException;
import play.data.Form;
import play.data.format.Formatters;
import play.data.validation.Constraints;
import play.data.validation.ValidationError;
import play.db.jpa.JPA;
import play.i18n.MessagesApi;
import play.libs.F;

import javax.validation.Validator;
import java.beans.PropertyDescriptor;
import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.security.InvalidKeyException;
import java.util.*;
import java.util.stream.Collectors;


/**
 * We decided to override the default Form implementation. This has several reasons:
 * <ul>
 *   <li>
 *       We can bind MStrings (and other complex types as well) directly with this solution
 *   </li>
 *   <li>
 *       We can return the same JPA object that we find from the db later on to save it.
 *       <br />
 *       Eg:
 *       <code>
 *           Model model = Model.find.byId(4);
 *           Form form = SmartForm.form(Model.class).fill(model).bindFromRequest();
 *           if(!form.hasErrors()) {
 *               model.updateFromForm(form.get());
 *               model.save();
 *           }
 *       </code>
 *   </li>
 * </ul>
 * This has some downsides:
 * <ul>
 *     <li>
 *         If we update Play, we need to update the class ch.insign.commons.db.Form as well (most likely).
 *     </li>
 * </ul>
 * @author Timo Schmid <timo.schmid@gmail.com>
 */
public class SmartForm<T> extends SecureForm<T> {

	private final static Logger logger = LoggerFactory.getLogger(SmartForm.class);

    private final Class<T> backedType;
    private final Formatters formatters;
    private final Validator validator;

    private SmartForm(Class<T> clazz) {
        this(null, clazz, new HashMap<>(), new HashMap<>(), Optional.empty());
    }

    private SmartForm(String rootName, Class<T> clazz, Map<String, String> data, Map<String, List<ValidationError>> errors, Optional<T> value) {
        this(rootName, clazz, data, errors, value,
                play.api.Play.current().injector().instanceOf(MessagesApi.class),
                play.api.Play.current().injector().instanceOf(Formatters.class),
                play.api.Play.current().injector().instanceOf(javax.validation.Validator.class));
    }

    private SmartForm(String rootName, Class<T> clazz, Map<String, String> data, Map<String, List<ValidationError>> errors, Optional<T> value,
                      MessagesApi messagesApi, Formatters formatters, javax.validation.Validator validator) {
        super(rootName, clazz, data, errors, value, messagesApi, formatters, validator);
        backedType = clazz;
        this.formatters = formatters;
        this.validator = validator;
    }

    @Override
    public Form.Field field(String key) {
        String fieldValue = null;
        Field superField = super.field(key);
        if(data().containsKey(key) || !superField.indexes().isEmpty()) {
            return superField;
        } else {
            if(key != null) {
                String[] splittedKey = key.split("\\.");
                if(splittedKey.length == 2) {
                    String fieldName = splittedKey[0];
                    String languageKey = splittedKey[1];
                    if(value().isPresent()) {
                        try {
                            BeanWrapper beanWrapper = new BeanWrapperImpl(value().get());
                            beanWrapper.setAutoGrowNestedPaths(true);
                            if(beanWrapper.isReadableProperty(fieldName)) {
                                PropertyDescriptor propertyDescriptor = beanWrapper.getPropertyDescriptor(fieldName);
                                if(DataBinding.isSupported(propertyDescriptor.getPropertyType())) {
                                    Object oValue = beanWrapper.getPropertyValue(fieldName);
                                    Map<String, String> data = DataBinding.getMap(propertyDescriptor.getPropertyType(), fieldName, oValue);
                                    if(data.containsKey(key)) {
                                        fieldValue = formatters.print(beanWrapper.getPropertyTypeDescriptor(fieldName), data.get(key));
                                    }
                                }
                            }
                        } catch (InvalidPropertyException ignored) {
                        }
                    }
                }
            }
        }
        if(fieldValue != null) {
            // This part is just copied from play.data.Form
            // In case we update to a more recent play version, we need to update

            // Error
            List<ValidationError> fieldErrors = errors().get(key);
            if(fieldErrors == null) {
                fieldErrors = new ArrayList<>();
            }

            // Format
            F.Tuple<String,List<Object>> format = null;
            BeanWrapper beanWrapper = new BeanWrapperImpl(getFormInstance());
            beanWrapper.setAutoGrowNestedPaths(true);
            try {
                for(Annotation a: beanWrapper.getPropertyTypeDescriptor(key).getAnnotations()) {
                    Class<?> annotationType = a.annotationType();
                    if(annotationType.isAnnotationPresent(play.data.Form.Display.class)) {
                        play.data.Form.Display d = annotationType.getAnnotation(play.data.Form.Display.class);
                        if(d.name().startsWith("format.")) {
                            List<Object> attributes = new ArrayList<>();
                            for(String attr: d.attributes()) {
                                Object attrValue = null;
                                try {
                                    attrValue = a.getClass().getDeclaredMethod(attr).invoke(a);
                                } catch(NoSuchMethodException|SecurityException|IllegalAccessException|InvocationTargetException ignored) {
                                }
                                attributes.add(attrValue);
                            }
                            format = F.Tuple(d.name(), attributes);
                        }
                    }
                }
            } catch(NullPointerException e) {}

            // Constraints
            javax.validation.metadata.PropertyDescriptor property = validator.getConstraintsForClass(backedType).getConstraintsForProperty(key);
            List<F.Tuple<String,List<Object>>> constraints = new ArrayList<>();
            if(property != null) {
                constraints = Constraints.displayableConstraint(property.getConstraintDescriptors());
            }

            return new Field(this, key, constraints, format, fieldErrors, fieldValue);
        }
        return super.field(key);
    }


    public Form<T> bind(Map<String, String> data, String... allowedFields) {

    	cleanupData(data);

        T object = getFormInstance();
        // Validate the submitted form fields: Either by signed formKey or by passed allowedFields.
        try {
            SecureForm.validateSubmittedForm(data, allowedFields, object);

        } catch (InvalidKeyException e) {

            // We're strict about failed submissions.
            throw new RuntimeException(e);
        }

        // Calling get on this form object, as this will be the jpa-managed entity.
        Form<T> superForm = super.bind(data, allowedFields);
        T targetObject;

        try {
            targetObject = superForm.get();
        } catch (IllegalStateException e) {
            for(Map.Entry<String, List<ValidationError>> entry : superForm.errors().entrySet()) {
                logger.debug("entry: "+entry.getKey());
            }
            targetObject = null;
        }

        Map<String, List<ValidationError>> errors = superForm.errors();
        logger.debug("superForm errors: "+superForm.errors());
        BeanWrapper beanWrapper = new BeanWrapperImpl(getFormInstance());
        beanWrapper.setAutoGrowNestedPaths(true);
        for(java.beans.PropertyDescriptor propertyDescriptor : beanWrapper.getPropertyDescriptors()) {
            if(allowedFields == null || allowedFields.length == 0 || ArrayUtils.contains(allowedFields, propertyDescriptor.getName()) && propertyDescriptor.getReadMethod() != null && propertyDescriptor.getWriteMethod() != null) {
                try {
                    if(DataBinding.isSupported(propertyDescriptor.getPropertyType())) {
                        Method readMethod = propertyDescriptor.getReadMethod();
                        Method writeMethod = propertyDescriptor.getWriteMethod();
                        if(readMethod != null && writeMethod != null) {
                            // Bound and validate data including only supported languages
                            Object unboundObject = propertyDescriptor.getReadMethod().invoke(object);
                            Object boundObject = DataBinding.bind(propertyDescriptor.getPropertyType(), propertyDescriptor.getName(), unboundObject, superForm.data());
                            errors.remove(propertyDescriptor.getName());
                            errors.putAll(DataBinding.validate(propertyDescriptor.getName(), boundObject, object, superForm.data()));
                            if (errors.size() > 0) {
                                // Refresh object is there were errors. Otherwise it will be saved to db with error values
                                if (boundObject instanceof Model && JPA.em().contains(boundObject)) {
                                    ((Model)boundObject).refresh();
                                }
                            }

                            if(targetObject != null) {
                                writeMethod.invoke(targetObject, boundObject);
                            }
                            writeMethod.invoke(object, boundObject);
                        }
                    }

                    // When handling multi-select fields (like checkbox lists or selects), play generates an object
                    // for each element in the list, even if it isn't actually marked as selected. That's why we have
                    // to iterate over the list and remove any blank/empty instances.
                    if (targetObject != null && propertyDescriptor.getPropertyType() == List.class && propertyDescriptor.getReadMethod()
                            != null && propertyDescriptor.getWriteMethod() != null) {
                        List<?> list = (List<?>)propertyDescriptor.getReadMethod().invoke(targetObject);
                        List<Integer> elementsToRemove = new ArrayList<>();
                        if (list != null && list.size() > 0){

                            // For the not selected options in the select or the not selected checkboxes, play
                            // creates a new instance of the underlying class via the default constructor. That's why
                            // we have to compare against a new instance and not against null.
                            for(int i = list.size() - 1;i >= 0;i--) {
                                Object obj = list.get(i);
                                if(obj == null) {
                                    elementsToRemove.add(i);
                                } else if (obj instanceof Model && ((Model)obj).getId() == null) {
                                    String name = propertyDescriptor.getName() + "[" + i + "]";
                                    if(!hasAnyData(name, data)) {
                                        elementsToRemove.add(i);
                                    }
                                }
                            }

                            for(int index : elementsToRemove) {
                                list.remove(index);
                            }

                            propertyDescriptor.getWriteMethod().invoke(object, list);
                        }
                    }
                } catch (IllegalAccessException | InvocationTargetException e) {
                    e.printStackTrace();
                }
            }
        }

        return new SmartForm<>(superForm.name(), backedType, superForm.data(), errors, updateFromForm(object, targetObject, superForm.data()));
    }

    /**
     * Spring tries to instantiate all composite objects passed in the request,
     * even those that pass id=0, i.e a select field. In some cases you can't remove them from the request,
     * unless you remove the field from the DOM. Instead, this function checks for "empty" composite-id keys
     * (elements like some_field.id=0) and removes them.
     * @param data
     */
    private void cleanupData(Map<String, String> data) {
    	List<String> elementsToRemove = data
                .entrySet()
                .stream()
                .filter(entry -> entry.getKey().endsWith(".id") && (entry.getValue().equals("") || entry.getValue().equals("0")))
                .map(Map.Entry::getKey)
                .collect(Collectors.toList());

        elementsToRemove.forEach(data::remove);
	}

    private static boolean hasAnyData(String fieldName, Map<String,String> data) {
        for(String key : data.keySet()) {
            if(key.startsWith(fieldName)) {
                return true;
            }
        }
        return false;
    }

    public static <T> Optional<T> updateFromForm(T target, T detachedEntity, Map<String,String> formData) throws UpdateFromFormException {
        if (detachedEntity == null) {
            return Optional.empty();
        }

        // Loop through all form fields and copy the property value from the detached to
        // the persisted instance for each submitted form field.
        // Assumption: The form field name matches exactly the property name (as assumes play's form class)

        List<String> normalizedFormFields = new ArrayList<>();
        for (String formFieldName : formData.keySet()) {
            String[] f = formFieldName.split("\\[|\\.");
            normalizedFormFields.add(f[0]);
        }


        BeanWrapper beanWrapper = new BeanWrapperImpl(target);
        beanWrapper.setAutoGrowNestedPaths(true);
        for(PropertyDescriptor descriptor : beanWrapper.getPropertyDescriptors()) {
            try {

                if (!normalizedFormFields.contains(descriptor.getName())) {
                    logger.debug("updatFromForm: Prop ignored, not in form: " + descriptor.getName());
                    continue;
                }

                // logger.error("FORM: descriptor: " + descriptor.getName());
                Method readMethod = descriptor.getReadMethod();
                if(readMethod == null) {
                    // logger.error("FORM: no read method");
                    continue;
                }
                Method writeMethod = descriptor.getWriteMethod();
                if(writeMethod == null) {
                    // logger.error("FORM: no write method");
                    continue;
                }
                Object oldValue = readMethod.invoke(target);
                Object fieldValue = readMethod.invoke(detachedEntity);

                // Are these checks required now that the is-in-form check is done above?
                /*if(oldValue == null
                        || formData.containsKey(descriptor.getName())
                        || (
                            (fieldValue != null)
                            && fieldValue != false
                        )
                ) {*/
                logger.debug("updateFromForm: " + target.toString()+ " | setting " + descriptor.getName() + " to " + fieldValue);
                writeMethod.invoke(target, fieldValue);
                /*} else {
                    // logger.error("FORM: oldValue = " + oldValue);
                    // logger.error("FORM: fieldValue = " + fieldValue);
                    // logger.error("FORM: formData = " + formData);
                }*/
            } catch(IllegalAccessException|InvocationTargetException e) {
                logger.error(e.getClass().getSimpleName() + ": ", e);
                String readAccessible = (descriptor.getReadMethod().isAccessible()) ? "acessible" : "not accessible";
                String writeAccessible = (descriptor.getWriteMethod().isAccessible()) ? "acessible" : "not accessible";
                logger.error(target.getClass().getSimpleName() + "." + descriptor.getReadMethod().getName() + " - " + readAccessible);
                logger.error(target.getClass().getSimpleName() + "." + descriptor.getWriteMethod().getName() + " - " + writeAccessible);
            }
        }
        // Logic for field trimming (@Trim annotation)
        Class<?> clazz = target.getClass();
        while(clazz != null) {
            for(java.lang.reflect.Field field : clazz.getDeclaredFields()) {
                if(!field.getName().equals("subtitle") /* WTF? Fixme.. */) {
                    continue;
                }
                logger.debug(clazz.getSimpleName() + "." + field.getName() + " => " + field.getDeclaredAnnotations().length + " Annotations.");
                // TODO For some reason, the @Trim annotation does not appear in the list of annotations. find out why.
                for(Annotation annotation : field.getDeclaredAnnotations()) {
                    logger.debug("annotation type: " + annotation.annotationType());
                    if(annotation instanceof Trim) {
                        logger.debug("Trimming field: " + field.getName());
                        if(field.getType() == String.class) {
                            boolean accessible = field.isAccessible();
                            if(!accessible) {
                                field.setAccessible(true);
                            }
                            try {
                                String str = (String)field.get(target);
                                if(str != null) {
                                    field.set(target, str.trim());
                                }
                            } catch(IllegalAccessException e) {
                            }
                            if(!accessible) {
                                field.setAccessible(false);
                            }
                        } else if(field.getType() == MString.class) {
                            boolean accessible = field.isAccessible();
                            if(!accessible) {
                                field.setAccessible(true);
                            }
                            try {
                                MString str = (MString)field.get(target);
                                if(str != null) {
                                    for(Map.Entry<String,String> entry : str.map().entrySet()) {
                                        if(entry.getValue() != null) {
                                            str.set(entry.getKey(), entry.getValue().trim());
                                        }
                                    }
                                }
                            } catch(IllegalAccessException e) {
                            }
                            if(!accessible) {
                                field.setAccessible(false);
                            }
                        } else {
                            logger.error("The annotation @Trim only works for String and MString fields.");
                        }
                    }
                }
            }
            clazz = clazz.getSuperclass();
        }
        /*
        for (String property : formData.keySet()) {

        	int strpos = property.indexOf("[");
        	if (strpos != -1)
        		property = property.substring(0, strpos);

            logger.debug("UpdateFromForm: Trying to merge form field: '" + property + "'");

            // Get the field
            try {
                java.lang.reflect.Field field = getFieldByName(target, property);

                boolean accessible = field.isAccessible();

                field.setAccessible(true);

                // Read the field
                Object value = null;
                try {
                    value = field.get(detachedEntity);
                } catch (IllegalArgumentException | IllegalAccessException e) {
                    field.setAccessible(accessible);
                    throw new UpdateFromFormException("Could not read field with name '" + property + "'", e);
                }

                // Old method: Set the field if its value is not null
                // if (value != null) {

                // New method: Even if it's null, if the key was sent by the form but the value is null, we
                // should set the value back to null (and hope this is fine)
                // TODO: Remove the if(true) if everything works
                if (true) {
                    try {

                        Method setter = new java.beans.PropertyDescriptor(property, target.getClass()).getWriteMethod();
                        setter.invoke(target, value);

                        logger.debug("UpdateFromForm: Updated " + property + " (using setter)");

                    } catch (Exception e) {

                        // 2nd if the setter is not available, try to set the property value directly
                        try {
                            field.set(target, value);
                            logger.debug("UpdateFromForm: Updated " + property + " (directly, no setter)");
                        } catch (IllegalArgumentException | IllegalAccessException e1) {
                            field.setAccessible(accessible);
                            throw new UpdateFromFormException("Could not set field with name '" + property + "'", e1);
                        }
                    }
                }
            }
            catch (NoSuchFieldException e1) {
                logger.warn("UpdateFromForm: Form field '" + property + "' not found in entity " + target);
            } catch (SecurityException e2) {
                throw new UpdateFromFormException("Could not access field with name '" + property + "'", e2);
            }
        }
        */

        return Optional.of(target);
    }

    // @Transient
    // private List<java.lang.reflect.Field> fieldList;

    /**
     * Get the Field by its name, regardless where in the class hierarchy it was defined.
     * @param name
     * @return Field
     * @throws NoSuchFieldException
     */
    protected static java.lang.reflect.Field getFieldByName(Object object, String name) throws NoSuchFieldException {
        for (java.lang.reflect.Field f : getAllFields(object)) {
            if (f !=  null && f.getName().equals(name)) return f;
        }
        throw new NoSuchFieldException();
    }

    /**
     * Get a (cached) list of all fields of this object and all superclasses.
     * TODO Re-Enable caching
     * @return
     */
    protected static List<java.lang.reflect.Field> getAllFields(Object object) {
        // if (fieldList == null) {
        List<java.lang.reflect.Field> fieldList = new ArrayList<java.lang.reflect.Field>();
        Class<?> i = object.getClass();
        while (i != null && i != Object.class) {
            fieldList.addAll(Arrays.asList(i.getDeclaredFields()));
            i = i.getSuperclass();
        }
        // }
        return fieldList;
    }

    /**
     * This exception is thrown if a JPA persisted entity could not be merged with a form's data.
     * This most likely indicates wrong use of the form/entity API.
     */
    public static class UpdateFromFormException extends RuntimeException  {
	private final static Logger logger = LoggerFactory.getLogger(UpdateFromFormException.class);
        public UpdateFromFormException() {super();}
        public UpdateFromFormException(String message) {super(message);}
        public UpdateFromFormException(String message, Throwable cause) {super(message, cause);}
        public UpdateFromFormException(Throwable cause) {super(cause);}
        private static final long serialVersionUID = 1L;
    }

    /**
     * Populates this form with an existing value, used for edit forms.
     *
     * @param value existing value of type <code>T</code> used to fill this form
     * @return a copy of this form filled with the new data
     */
    public Form<T> fill(T value) {
        Form<T> superForm = super.fill(value);
        return new SmartForm<T>(superForm.name(), backedType, superForm.data(), superForm.errors(), Optional.of(value));
    }

    /**
     * Modified version to return the bound object (The first three lines).
     * @return
     */
    protected T getFormInstance() {
        try {
            return value().orElse(blankInstance());
        } catch(Exception e) {
            throw new RuntimeException("Cannot instantiate " + backedType + ". It must have a default constructor", e);
        }
    }

    // FIXME there should be more DI, includes FormFactory(but Attributes class still be a complex model without injections)
    public static <T> Form<T> form(Class<T> tClass) {
        return new SmartForm<>(tClass);
    }

    public Form.Field superField(String key){
    	return super.field(key);
    }

    /**
     * Safe method to check if a form key is empty. Especially for composite keys, it is highly recommended
     * that you use this method first, before you access the form field, because otherwise you might get
     * persistence exceptions.
     *
     * @param key
     * @return
     */
    public boolean isFieldEmpty(String key){
    	if (value().isPresent()){

		    if (field(key).value() != null) {
			    return false;
		    }

    		List<String> parts = Arrays.asList(key.split("\\."));
    		String newKey = null;

    		for(String part: parts){
    			if (newKey == null)
    				newKey = part;
    			else
    				newKey += "."+part;
    			if (field(newKey).value() == null)
    				return true;
    		}

    		return false;
    	}

    	return field(key) == null;
    }

}
