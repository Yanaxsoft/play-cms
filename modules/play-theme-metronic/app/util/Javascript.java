/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package util;

import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import play.twirl.api.Html;
import play.mvc.Http.Context;
import play.twirl.api.HtmlFormat;
import scala.collection.JavaConversions;
import scala.collection.mutable.Seq;

/**
 * Since all javascript plugins are included at the end of the page, you will only be able to
 * use them if you append javascripts afterwards. This class takes care of that.
 * @author zahariev
 *
 */
public class Javascript  {
	private final static Logger logger = LoggerFactory.getLogger(Javascript.class);
	public static final String KEY = "javascripts";
	
	public static void add(Html script){
		List<Html> list;
		if (Context.current().args.containsKey(KEY))
			list = (List<Html>)Context.current().args.get(KEY);
		else
			list = new ArrayList<>();
		list.add(script);
		
		Context.current().args.put(KEY, list);
	}
	
	public static Html getAll(){
		return Optional.ofNullable(Context.current().args.get(KEY))
				.filter(x -> x instanceof List)
				.map(x -> (List<Html>) x)
				.map(x -> JavaConversions.asScalaBuffer(x).toList())
				.map(x -> HtmlFormat.fill(x))
				.orElse(HtmlFormat.empty());
	}
	
	public static void clear(){
		Context.current().args.put(KEY, new ArrayList<Html>());
	}
}
