/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.metronic.controllers

import javax.inject.{Inject, Singleton}

import controllers.Assets.Asset
import controllers.AssetsBuilder
import play.api.http.{HttpErrorHandler, LazyHttpErrorHandler}

/**
  * @author Urs Honegger &lt;u.honegger@insign.ch&gt;
  */
@Singleton
class Assets @Inject() (errorHandler: HttpErrorHandler) extends controllers.Assets(errorHandler) {
  def lib(path: String, file: Asset) = versioned(path, file)
}

object Assets extends AssetsBuilder(LazyHttpErrorHandler) {
}
