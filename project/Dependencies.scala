import play.sbt.PlayImport._
import sbt._

object Dependencies {

  object Libraries {
    val cdiApi = "javax.enterprise" % "cdi-api" % "1.1"
    val commonsEmail = "org.apache.commons" % "commons-email" % "1.3.2"
    val commonsIo = "commons-io" % "commons-io" % "2.4"
    val commonsLang3 = "org.apache.commons" % "commons-lang3" % "3.2.1"
    val commonsLogging = "commons-logging" % "commons-logging" % "1.1.3"
    val easyCriteria = "uaihebert.com" % "EasyCriteria" % "3.0.0"
    val eclipselink = "org.eclipse.persistence" % "eclipselink" % "2.6.4"
    val elasticsearch = "org.elasticsearch" % "elasticsearch" % "2.3.3"
    val gson = "com.google.code.gson" % "gson" % "2.5"
    val httpRequest = "com.github.kevinsawicki" % "http-request" % "5.5"
    val httpclient = "org.apache.httpcomponents" % "httpclient" % "4.5.1"
    val httpcore = "org.apache.httpcomponents" % "httpcore" % "4.4.4"
    val im4java = "org.im4java" % "im4java" % "1.4.0"
    val json = "org.json" % "json" % "20151123"
    val jsonPath = "com.jayway.jsonpath" % "json-path" % "2.1.0"
    val jsoup = "org.jsoup" % "jsoup" % "1.7.3"
    val metricsElasticsearchReporter = "org.elasticsearch" % "metrics-elasticsearch-reporter" % "2.0"
    val metricsPlay = "com.kenshoo" %% "metrics-play" % "2.5.0_0.5.0-play-2.5-fix"
    val mockitoAll = "org.mockito" % "mockito-all" % "1.9.5"
    val playHtmlCompressor = "com.mohiva" %% "play-html-compressor" % "0.6.0"
    val poi = "org.apache.poi" % "poi" % "3.11"
    val poiOoxml = "org.apache.poi" % "poi-ooxml" % "3.11"
    val reflections = "org.reflections" % "reflections" % "0.9.9"
    val shiroCore = "org.apache.shiro" % "shiro-core" % "1.2.5"
    val xbeanFinder = "org.apache.xbean" % "xbean-finder" % "3.7"
  }

  object Webjars {
    var bootstrap = "org.webjars" % "bootstrap" % "3.3.5"
    var bootstrapModal = "org.webjars" % "bootstrap-modal" % "2.2.5"
    var bootstrapMultiselect = "org.webjars" % "bootstrap-multiselect" % "0.9.13-1"
    var bootstrapSwitch = "org.webjars" % "bootstrap-switch" % "3.3.2"
    var bootstrapTagsinput = "org.webjars.bower" % "bootstrap-tagsinput" % "0.8.0"
    var chosen = "org.webjars.bower" % "chosen" % "1.4.2"
    val datatables = "org.webjars" % "datatables" % "1.9.4-2"
    val datatablesBootstrap = "org.webjars" % "datatables-bootstrap" % "2-20120202-2"
    val excanvas = "org.webjars" % "excanvas" % "3"
    val fontAwesome = "org.webjars" % "font-awesome" % "4.6.1"
    val flexSlider = "org.webjars" % "FlexSlider" % "2.2.2"
    var highlightjs = "org.webjars.bower" % "highlightjs" % "8.5.0"
    val jquery = "org.webjars" % "jquery" % "1.10.2"
    val jqueryBlockui = "org.webjars" % "jquery-blockui" % "2.65"
    val jqueryCookie = "org.webjars" % "jquery-cookie" % "1.3.1"
    val jqueryForm = "org.webjars" % "jquery-form" % "3.51"
    val jqueryMigrate = "org.webjars" % "jquery-migrate" % "1.2.1"
    var jqueryNestable = "org.webjars.bower" % "jquery-nestable" % "0.0.1"
    val jquerySelect2 = "org.webjars" % "select2" % "3.4.3"
    val jquerySlimScroll = "org.webjars" % "jQuery-slimScroll" % "1.3.1"
    var jqueryTypeahead = "org.webjars.bower" % "jquery-typeahead" % "2.2.1"
    val jqueryUi = "org.webjars" % "jquery-ui" % "1.10.3"
    var respond = "org.webjars.bower" % "respond" % "1.4.2"
    var tether = "org.webjars.bower" % "tether" % "1.3.2"
    var tinymce = "org.webjars.bower" % "tinymce" % "4.3.3"
    var uniform = "org.webjars" % "uniform" % "2.1.2"
    var fancyBox = "org.webjars" % "fancybox" % "2.1.5"
  }


  /** Projects */

  val cmsDeps =  Seq(
    filters,
    javaCore,
    Libraries.json,
    Libraries.commonsEmail,
    Libraries.commonsIo,
    Libraries.commonsLogging,
    Libraries.eclipselink,
    Libraries.elasticsearch,
    Libraries.gson,
    Libraries.httpRequest,
    Libraries.httpclient,
    Libraries.httpcore,
    Libraries.im4java,
    Libraries.jsonPath,
    Libraries.jsoup,
    Libraries.metricsElasticsearchReporter,
    Libraries.metricsPlay,
    Libraries.mockitoAll,
    Libraries.playHtmlCompressor,
    Libraries.poi,
    Libraries.poiOoxml,
    Webjars.flexSlider
  )

  val authDeps = Seq(
    cache,
    javaJpa,
    Libraries.cdiApi,
    Libraries.commonsLang3,
    Libraries.easyCriteria,
    Libraries.eclipselink,
    Libraries.mockitoAll,
    Libraries.reflections,
    Libraries.shiroCore,
    Libraries.xbeanFinder
  )

  val commonsDeps = Seq(
    cache,
    javaCore,
    javaJdbc,
    javaJpa,
    javaWs,
    Libraries.easyCriteria,
    Libraries.eclipselink,
    Libraries.httpcore,
    Libraries.httpclient,
    Libraries.jsoup,
    Libraries.mockitoAll
  )

  val metronicDeps = Seq(
    javaCore,
    javaJdbc,
    javaJpa,
    Webjars.bootstrap,
    Webjars.bootstrapModal,
    Webjars.bootstrapMultiselect,
    Webjars.bootstrapSwitch,
    Webjars.bootstrapTagsinput,
    Webjars.chosen,
    Webjars.datatables,
    Webjars.datatablesBootstrap,
    Webjars.excanvas,
    Webjars.fontAwesome,
    Webjars.flexSlider,
    Webjars.highlightjs,
    Webjars.jquery,
    Webjars.jqueryBlockui,
    Webjars.jqueryCookie,
    Webjars.jqueryForm,
    Webjars.jqueryMigrate,
    Webjars.jqueryNestable,
    Webjars.jquerySelect2,
    Webjars.jquerySlimScroll,
    Webjars.jqueryTypeahead,
    Webjars.jqueryUi,
    Webjars.respond,
    Webjars.tether,
    Webjars.tinymce,
    Webjars.uniform,
    Webjars.fancyBox
  )

}