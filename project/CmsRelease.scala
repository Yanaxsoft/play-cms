import com.typesafe.sbt.GitPlugin.autoImport._
import sbt._
import sbt.Keys._
import sbtrelease._
import sbtrelease.ReleasePlugin.autoImport._
import sbtrelease.ReleaseStateTransformations.{setReleaseVersion => _, _}
import de.heikoseeberger.sbtheader.HeaderPlugin

object CmsRelease extends AutoPlugin {

  override def requires = HeaderPlugin

  lazy val showNextVersion: SettingKey[String] =
    settingKey[String]("the future version once releaseNextVersion has been applied to it")

  lazy val showReleaseVersion: SettingKey[String] =
    settingKey[String]("the future version once releaseNextVersion has been applied to it")

  private val setReleaseVersion: ReleaseStep = setVersion(_._1)
  private val setNextVersion: ReleaseStep = setVersion(_._2)

  private def setVersion(selectVersion: Versions => String): ReleaseStep = { st: State =>
    val vs = st.get(ReleaseKeys.versions).getOrElse(sys.error("No versions are set! Was this release part executed before inquireVersions?"))
    val selected = selectVersion(vs)

    st.log.info("Setting version to '%s'." format selected)
    val useGlobal =Project.extract(st).get(releaseUseGlobalVersion)

    reapply(Seq(
      if (useGlobal) version in ThisBuild := selected
      else version := selected
    ), st)
  }

  val checkHeaders: ReleaseStep =
    releaseStepCommand("checkHeaders")

  val settings : Seq[Setting[_]] = Seq(

    // sbt-git
    // Derive the version of the project the most recent git tag

    git.useGitDescribe := true,
    git.baseVersion := "0.0.0",
    git.uncommittedSignifier := None,
    git.gitTagToVersionNumber := { v => {
      val ver = Version(v drop 1) // drop prefix "v" from version string
      val isSnapshot = git.gitCurrentTags.value.isEmpty || git.gitUncommittedChanges.value
      val hasSnapshotQualifier = ver.flatMap(_.qualifier).exists(_ == "-SNAPSHOT")

      if (isSnapshot && !hasSnapshotQualifier) {
        ver.map(_.bump(releaseVersionBump.value).asSnapshot.string)
      } else {
        ver.map(_.string)
      }
    }},

    // sbt-release
    // Release the project by running tests and publishing artifacts with bumped up version

    releasePublishArtifactsAction := publish.value,

    releaseProcess := Seq(
      checkSnapshotDependencies,
      inquireVersions,
      checkHeaders,
      runClean,
      runTest,
      setReleaseVersion,
      tagRelease,
      publishArtifacts,
      pushChanges
    )
  )
}