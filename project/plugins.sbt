// Comment to get more information during initialization
logLevel := Level.Warn

// The Typesafe repository
resolvers += "Typesafe repository" at "https://dl.bintray.com/typesafe/maven-releases/"

// Use the Play sbt plugin for Play projects
addSbtPlugin("com.typesafe.play" % "sbt-plugin" % "2.5.12")

// Generates getters and setters for Java properties
addSbtPlugin("com.typesafe.sbt" % "sbt-play-enhancer" % "1.1.0")

// Plugin for publishing to bintray
addSbtPlugin("me.lessis" % "bintray-sbt" % "0.3.0")

// SbtWeb plugins
addSbtPlugin("com.typesafe.sbt" % "sbt-less" % "1.1.0")

// Provides the build info to the project
addSbtPlugin("com.eed3si9n" % "sbt-buildinfo" % "0.6.1")

// Automatically create headers in source code files
addSbtPlugin("de.heikoseeberger" % "sbt-header" % "1.8.0")

// Release versioning
addSbtPlugin("com.github.gseitz" % "sbt-release" % "1.0.3")

addSbtPlugin("com.typesafe.sbt" % "sbt-git" % "0.8.5")
