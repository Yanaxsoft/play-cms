/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.cms.actors;

import akka.actor.ActorSystem;
import akka.actor.Props;
import akka.actor.UntypedActor;
import ch.insign.commons.util.Configuration;
import ch.insign.playauth.authz.AccessControlEntry;
import ch.insign.playauth.authz.AccessControlList;
import ch.insign.playauth.authz.DomainObjectRetrievalStrategy;
import com.google.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import play.api.Play;
import play.api.inject.Injector;
import play.db.jpa.JPAApi;
import scala.concurrent.duration.Duration;

import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.TimeUnit;

/**
 * AclGarbageCollectorActoris used to clear permissions that refers to a not existing security or object identities.
 */
public class AclGarbageCollectorActor extends UntypedActor {
    private static final Logger logger = LoggerFactory.getLogger(AclGarbageCollectorActor.class);
    @Inject
    private static ActorSystem actorSystem;
    @Inject
    private static JPAApi jpaApi;

    public static void start() {
        int nextExecutionHour = Configuration.getOrElse("akka.actor.AclGarbageCollectorActor.executeTime", 3);
        long nextExecutionInSeconds = getNextExecutionInSeconds(nextExecutionHour);

        logger.info("AclGarbageCollectorActor will be executed at "
                + nextExecutionHour + ":00 (in " + nextExecutionInSeconds + " seconds)");

        actorSystem.scheduler().schedule(
                Duration.create(nextExecutionInSeconds, TimeUnit.SECONDS),
                Duration.create(Configuration.getOrElse("akka.actor.AclGarbageCollectorActor.interval", 7), TimeUnit.DAYS),
                actorSystem.actorOf(Props.create(AclGarbageCollectorActor.class)),
                "AclGarbageCollectorActor execute",
                actorSystem.dispatcher(),
                null
        );
    }

    @Override
    public void onReceive(Object message) throws Exception {
        logger.info("AclGarbageCollectorActor onReceive() started");
        jpaApi.withTransaction(AclGarbageCollectorActor::processCleanup);
    }


    private static void processCleanup() {
        Injector injector = Play.current().injector();
        AccessControlList acl = injector.instanceOf(AccessControlList.class);
        DomainObjectRetrievalStrategy objrs = injector.instanceOf(DomainObjectRetrievalStrategy.class);

        Set<AccessControlEntry> aclDeleteList = new HashSet<>();

        logger.info("acl clean_up started");
        acl.streamAll().forEach((aclEntry) -> {
            try {
                if (!aclEntry.getSid().getType().equals("*")
                        && !aclEntry.getSid().getIdentifier().equals("*")) {

                    Object sidObject = objrs.getSidObject(aclEntry);
                    if (sidObject == null) {
                        aclDeleteList.add(aclEntry);
                    }
                }

                if (!aclEntry.getOid().getType().equals("*")
                        && !aclEntry.getOid().getIdentifier().equals("*")) {

                    Object oidObject = objrs.getOidObject(aclEntry);
                    if (oidObject == null) {
                        aclDeleteList.add(aclEntry);
                    }
                }
            } catch (DomainObjectRetrievalStrategy.UnknownDomainClassException e) {
                logger.warn(e.getMessage());
            }
        });

        aclDeleteList.forEach(acl::remove);

        logger.info("acl clean_up finished. {} entries were removed. ", aclDeleteList.size());
    }

    /**
     * Get time in seconds to next given hour.
     * @param hour
     * @return
     */
    private static long getNextExecutionInSeconds(int hour) {
        Date now = new Date();

        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, hour);
        calendar.set(Calendar.MINUTE, 0);

        // if hour was passed on this day, add one day
        if (calendar.getTime().before(now)) {
            calendar.add(Calendar.DATE, 1);
        }

        return getSecondsToNextDate(calendar.getTime());
    }

    private static long getSecondsToNextDate(Date nextExecution) {
        Date now = new Date();
        long diffInMillies = nextExecution.getTime() - now.getTime();
        return TimeUnit.SECONDS.convert(diffInMillies,TimeUnit.MILLISECONDS);
    }

}
