/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.cms.attributeset

import play.twirl.api.Html
import play.api.data.Field

package object helper {
  object repeat {
    def apply(field: Field, min: Int = 1)(fieldRenderer: (Integer, Field) => Html): Seq[Html] = {
      val indexes = field.indexes match {
        case Nil => 0 until min
        case complete if complete.size >= min => field.indexes
        case partial =>
          // We don't have enough elements, append indexes starting from the largest
          val start = field.indexes.max + 1
          val needed = min - field.indexes.size
          field.indexes ++ (start until (start + needed))
      }

      indexes.map(i => fieldRenderer(i, field("[" + i + "]")))
    }
  }
}
