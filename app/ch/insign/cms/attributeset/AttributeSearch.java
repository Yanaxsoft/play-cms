/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.cms.attributeset;

import ch.insign.cms.attributeset.model.Option;
import ch.insign.cms.attributeset.model.OptionValue;
import ch.insign.commons.db.Model;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

/**
 * DON'T instantiate this class directly. Use the {@link AttributeService#search(Model.Finder)} method instead.
 *
 * Example Usage:
 * List<Event> list = AttributeService.search(Event.find)
 *                      .andAll("set", "profession", Arrays.asList(cook, receptionist))
 *                      .andAny("set", "language", Arrays.asList(deutsch, english))
 *                      .andAll("set", "region", Arrays.asList(zurich))
 *                      .get();
 */
public class AttributeSearch<T extends Model> {
    private List<T> res = new ArrayList<>();
    private Set<String> set = new HashSet<>();
    private boolean initialized;

    private final static Logger logger = LoggerFactory.getLogger(AttributeSearch.class);

    private Model.Finder<T> finder;

    /**
     * @param finder An instance of Model.Finder that will be used to initiate queries. Used to specify the class
     *               of the objects, that you are searching for and which will be returned.
     */
    AttributeSearch(Model.Finder<T> finder){
        this.finder = finder;
    }

    /**
     * Searches for objects of the given class (specified by the finder), that have ALL of the given options
     * assigned to them as value for the given attribute set and for the given attribute.
     *
     * @param setKey The key of the attribute set
     * @param attrKey The key of the attribute that you are filtering by.
     * @param options An array of options that you are filtering by.
     */
    public AttributeSearch<T> andAll(String setKey, String attrKey, List<Option> options){
        // you must provide at least one Option for the search
        if (options.size() == 0) {
            setIntersection(finder.all());
            return this;
        }
        List<T> res = new ArrayList<>();

        /*
         * A note on the implementation: since we are searching the Objects that have ALL of the given options, we can
         * minimize the time complexity by selecting the option from the list that has the least amount of Objects.
         * Then we can loop through those objects and for each check if it indeed contains all other Options.
         */
        Option minOption = options.stream()
                .min((a, b) -> Integer.compare(a.getMappedValues().size(), b.getMappedValues().size())).get();
        for (OptionValue val: minOption.getMappedValues()){
            T owner = finder.byId(val.getInstance().getOwnerObject());
            if (owner != null) {
                OptionValue optVal = (OptionValue) AttributeService.getAttributeValue(setKey, attrKey, owner.getId());
                if (optVal != null && optVal.getValue().containsAll(options)) {
                    res.add(owner);
                }
            }
        }
        setIntersection(res);
        return this;
    }

    /**
     * Searches for objects of the given class (specified by the finder), that have AT LEAST ONE of the given options
     * assigned to them as value for the given attribute set and for the given attribute.
     *
     * @param setKey The key of the attribute set
     * @param attrKey The key of the attribute that you are filtering by.
     * @param options An array of options that you are filtering by.
     */
    public AttributeSearch<T> andAny(String setKey, String attrKey, List<Option> options){
        // you must provide at least one Option for the search
        if (options.size() == 0) {
            setIntersection(finder.all());
            return this;
        }

        Set<String> stringSet = new HashSet<>();

        for (Option opt: options){
            for (OptionValue val: opt.getMappedValues()){
                stringSet.add(val.getInstance().getOwnerObject());
            }
        }

        if (stringSet.isEmpty()) {
            return this;
        }

        setIntersection(finder.query().andStringIn("id", new ArrayList<>(stringSet)).getResultList());

        return this;
    }

    private void setIntersection(List<T> next){
        if (!initialized){
            initialized = true;
            res = next;
            for (T elem: next){
                set.add(elem.getId());
            }
            return;
        }

        /* Note on performance: Implemented with HashMap for O(m+n) complexity. Possible optimization with
           binary search but ONLY IF one of the sets is MUCH smaller than the other.
        */
        HashSet<String> newSet = new HashSet<>();
        res = new ArrayList<>();
        for (T elem: next){
            if (set.contains(elem.getId())){
                newSet.add(elem.getId());
                res.add(elem);
            }
        }
        set = newSet;
    }

    /**
     * @return A list of objects satisfying all previous search criteria.
     */
    public List<T> get(){
        return res;
    }
}
