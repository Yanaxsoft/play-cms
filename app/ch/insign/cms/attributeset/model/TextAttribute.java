/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.cms.attributeset.model;

import play.data.DynamicForm;
import play.twirl.api.Html;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="cms_eav_attribute_text")
@DiscriminatorValue("text")
public class TextAttribute extends Attribute {

    @Override
    public Html editForm(play.data.Form form, Value value) {
        return ch.insign.cms.attributeset.views.html.backend.textAttributeForm.render(form, this, value);
    }

    @Override
    public Value getDefaultValue() {
        TextValue res = new TextValue();
        res.setValue("Initial value for tests! Replace me!");
        return res;
    }

    @Override
    public Value restoreValueFromRequest(Value oldValue) {
        TextValue res = (TextValue)oldValue;
        res.setValue(DynamicForm.form().bindFromRequest().get("setInstance."+getKey()+".value"));
        return res;
    }
}
