/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.cms.attributeset.model;

import ch.insign.commons.db.MString;
import ch.insign.commons.db.Model;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * A grouping of attributes. Each attribute can belong to multiple sets. Assignment of a set to a particular
 * entity is done statically by the entity - the entity "knows" which sets belong to it and the entity
 * can access them by using {@link AttributeSetFinder#byKey(String)}.
 */
@Entity
@Table(name="cms_eav_attribute_set")
public class AttributeSet extends Model{

    private final static Logger logger = LoggerFactory.getLogger(AttributeSet.class);

    public static final AttributeSetFinder find = new AttributeSetFinder();

    @Column(name = "set_key")
    private String key;

    @OneToOne(cascade = CascadeType.ALL)
    public MString name;

    @ManyToMany
    @OrderColumn
    public List<Attribute> attributes = new ArrayList<>();

    @OneToMany(mappedBy = "set", cascade = CascadeType.ALL, orphanRemoval = true)
    @MapKey(name="ownerObject")
    private Map<String, AttributeSetInstance> selections = new HashMap<>();

    /**
     * Get attributes with selected (or default) values for certain entity
     */
    public Map<Attribute, Value> getAttributesWithValues(String entityId) {
        Map<Attribute, Value> attributeValueMap = new HashMap<>();

        AttributeSetInstance currentSetInstance = null;

        if (entityId != null) {
            currentSetInstance = selections.get(entityId);
        }

        for (Attribute attr: attributes) {
            if (currentSetInstance == null || currentSetInstance.get(attr) == null) {
                attributeValueMap.put(attr, attr.getDefaultValue());
            } else {
                attributeValueMap.put(attr, currentSetInstance.get(attr));
            }
        }

        return attributeValueMap;
    }

    public AttributeSetInstance getAttributeSetInstance(String entityId) {
        return selections.get(entityId);
    }

    // ====== Getter and Setter ======

    public MString getName() {
        return name;
    }

    public void setName(MString name) {
        this.name = name;
    }

    public List<Attribute> getAttributes() {
        return attributes;
    }

    public void setAttributes(List<Attribute> attributes) {
        this.attributes = attributes;
    }

    public void removeAttribute(Attribute attribute) {
        attributes.remove(attribute);
    }

    public Map<String, AttributeSetInstance> getSelections() {
        return selections;
    }

    public void setSelections(Map<String, AttributeSetInstance> selections) {
        this.selections = selections;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public static class AttributeSetFinder extends Finder<AttributeSet>{
        private AttributeSetFinder(){
            super(AttributeSet.class);
        }

        public AttributeSet byKey(String key){
            try {
                return query().andEquals("key", key).getSingleResult();
            } catch(NoResultException e){
                return null;
            }
        }

        public List<AttributeSet> byAttribute(Attribute attribute){
            return query()
                    .andEquals("attributes", attribute)
                    .getResultList();
        }
    }
}
