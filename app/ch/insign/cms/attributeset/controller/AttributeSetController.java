/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.cms.attributeset.controller;

import ch.insign.cms.attributeset.model.AttributeSet;
import ch.insign.cms.attributeset.views.html.backend.attributeSetAddEdit;
import ch.insign.cms.attributeset.views.html.backend.attributeSetList;
import ch.insign.cms.permissions.aop.RequiresBackendAccess;
import ch.insign.commons.db.SmartForm;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import play.data.Form;
import play.db.jpa.Transactional;
import play.mvc.Controller;
import play.mvc.Result;

@RequiresBackendAccess
@Transactional
public class AttributeSetController extends Controller {
    private final static Logger logger = LoggerFactory.getLogger(AttributeSetController.class);

    public Result index() {
        return ok(attributeSetList.render(AttributeSet.find.all()));
    }

    public Result showAdd() {
        Form<AttributeSet> form = SmartForm.form(AttributeSet.class);
        return ok(attributeSetAddEdit.render(form));
    }

    public Result doAdd(){
        Form<AttributeSet> form = SmartForm.form(AttributeSet.class).bindFromRequest("name", "attributes*", "key");
        if (form.hasErrors()) {
            return badRequest(attributeSetAddEdit.render(form));
        }
        form.get().save();
        return redirect(ch.insign.cms.attributeset.controller.routes.AttributeSetController.index());
    }

    public Result delete(String id){
        AttributeSet set = AttributeSet.find.byId(id);
        if (set != null){
            set.delete();
        }
        return redirect(ch.insign.cms.attributeset.controller.routes.AttributeSetController.index());
    }

    public Result showEdit(String id){
        AttributeSet set = AttributeSet.find.byId(id);
        if (set == null){
            // TODO: handle missing ids
            throw new IllegalArgumentException("Attribute Set with id "+id+" not found!");
        }
        Form<AttributeSet> form = SmartForm.form(AttributeSet.class).fill(set);
        return ok(attributeSetAddEdit.render(form));
    }

    public Result doEdit(String id){
        AttributeSet set = AttributeSet.find.byId(id);
        if (set == null){
            // TODO: Handle exception
            throw new IllegalArgumentException("Attribute Set with id "+id+" not found!");
        }
        Form<AttributeSet> form = SmartForm.form(AttributeSet.class).fill(set).bindFromRequest("name", "attributes*", "key");
        if (form.hasErrors()) {
            return badRequest(attributeSetAddEdit.render(form));
        }
        form.get().save();
        return redirect(ch.insign.cms.attributeset.controller.routes.AttributeSetController.index());
    }
}
