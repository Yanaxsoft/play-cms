/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.cms.views.admin.utils;

import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

import play.mvc.Http.Context;
import ch.insign.cms.controllers.routes;

public class BackUrl  {
	private final static Logger logger = LoggerFactory.getLogger(BackUrl.class);
	/**
     * Helper method to determine the backUrl for the next request. If the current URI is from
     * a POST request, it takes the previous URL instead.
     * @return
     */
    public static String get(){
    	String backUrl = Context.current().request().uri();
    	if (!Context.current().request().method().toUpperCase().equals("GET")){
    		logger.debug("******** in here "+Context.current().request().getQueryString("backURL"));
    		backUrl = getPrevious();
    	}
    	
    	return backUrl;
    }
    
    /**
     * Returns the URL of the previous page if available, otherwise a default fallback URL.
     * @return
     */
    public static String getPrevious(){
    	String backUrl = Context.current().request().getQueryString("backURL");
		if (backUrl == null || backUrl.equals("")){
			backUrl = routes.NavigationController.index(null, null).url();
		}
		
		return backUrl;
    }
}