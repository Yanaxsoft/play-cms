@(keys: Set[String])

$(function() {
    @for(key <- keys) {
        namespacedKeyValue("@key", "@Messages(key)", Messages);
    }
});