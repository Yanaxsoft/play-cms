/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.cms;

import ch.insign.cms.blocks.jotformpageblock.service.JotFormService;
import ch.insign.cms.models.*;
import ch.insign.cms.service.EmailService;
import ch.insign.commons.filter.FilterManager;
import ch.insign.commons.search.SearchManager;
import ch.insign.playauth.controllers.RouteResolver;

/**
 * CMSApi provides convenient interface to the CMS services.
 */
public interface CMSApi {
    CmsConfiguration getConfig();

    Sites getSites();

    JotFormService getJotFormService();

    void setConfig(CmsConfiguration configInstance);

    void setRouteResolver(RouteResolver routeResolver);

    RouteResolver getRouteResolver();

    PartyFormManager getPartyFormManager();

    PartyEvents getPartyEvents();

    void setPartyEvents(PartyEvents partyEvents);

    void setPartyFormManager(PartyFormManager partyFormManager);

    BlockManager getBlockManager();

    FilterManager getFilterManager();

    SearchManager getSearchManager();

    UncachedManager getUncachedManager();

    EmailService getEmailService();

    void setBlockManager(BlockManager blockManagerInstance);

    void setJetFormSetup(JotFormService jotFormServiceInstance);

    void registerEmailService(EmailService emailServiceInstance);
}
