/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.cms.utils;

import ch.insign.cms.blocks.errorblock.ErrorPage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import play.mvc.Result;

import javax.persistence.Transient;
import java.util.Calendar;
import java.util.Optional;

import static play.mvc.Results.internalServerError;

public class Error  {
	private final static Logger logger = LoggerFactory.getLogger(Error.class);

    public static Result notFound(String msg) {
        try {
            return play.mvc.Results.notFound(
                    getErrorPage(ErrorPage.KEY_NOT_FOUND, msg).render());
        } catch (Exception e) {
            return internalServerError(Optional.ofNullable(e.getMessage()).orElseGet(e::toString));
        }
    }

    public static Result internal(String msg) {
        try {
            return play.mvc.Results.internalServerError(
                    getErrorPage(ErrorPage.KEY_INTERNAL_ERROR, msg).render());
        } catch (Exception e) {
            return internalServerError(Optional.ofNullable(e.getMessage()).orElseGet(e::toString));
        }
    }

    public static Result forbidden(String msg) {
        try {
            return play.mvc.Results.forbidden(
                    getErrorPage(ErrorPage.KEY_FORBIDDEN, msg).render());
        } catch (Exception e) {
            return internalServerError(Optional.ofNullable(e.getMessage()).orElseGet(e::toString));
        }
    }

    public static Result unavailable(String msg) {
        try {
            return play.mvc.Results.notFound(
                    getErrorPage(ErrorPage.KEY_UNAVAILABLE, msg).render());
        } catch (Exception e) {
            return internalServerError(Optional.ofNullable(e.getMessage()).orElseGet(e::toString));
        }
    }

    private static ErrorPage getErrorPage(String pageKey, String msg) throws Exception {
        String id = String.valueOf(Calendar.getInstance().getTimeInMillis());

	    try {
		    // Note: Handling adequate logging (stack trace, severity, remote) is the caller's responsibility, here we only log what
		    // error screen we show to the user.
		    logger.warn(String.format("%s #%s: %s", pageKey, id, msg));
	    } catch (Exception e) {
		    e.printStackTrace();
	    }

	    ErrorPage page = ErrorPage.find.byKey(pageKey);
        if (page == null) {
            Exception e =  new Exception("Cannot find error page with key: " + pageKey);
	        logger.error(e.getMessage(), e);
	        throw e;
        }

        page.setErrorId(id);
        page.setErrorMsg(msg);

        return page;
    }
}
