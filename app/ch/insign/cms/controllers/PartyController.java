/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.cms.controllers;

import ch.insign.cms.forms.CustomerEmailTemplateForm;
import ch.insign.cms.forms.PasswordChange;
import ch.insign.cms.models.CMS;
import ch.insign.cms.models.CustomerEmailTemplate;
import ch.insign.cms.views.admin.utils.AdminContext;
import ch.insign.cms.views.html.admin.party.list;
import ch.insign.cms.views.html.admin.party.passwordForm;
import ch.insign.cms.views.html.admin.party.rolesForm;
import ch.insign.commons.db.SecureForm;
import ch.insign.commons.db.SmartForm;
import ch.insign.commons.i18n.Language;
import ch.insign.playauth.PlayAuthApi;
import ch.insign.playauth.party.Party;
import ch.insign.playauth.party.PartyRole;
import ch.insign.playauth.party.PartyType;
import ch.insign.playauth.party.address.EmailAddress;
import ch.insign.playauth.party.support.DefaultParty;
import ch.insign.playauth.permissions.PartyPermission;
import ch.insign.playauth.permissions.PartyRolePermission;
import com.google.inject.Inject;
import org.apache.shiro.authz.AuthorizationException;
import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.slf4j.LoggerFactory;
import play.Logger;
import play.data.Form;
import play.data.validation.ValidationError;
import play.db.jpa.JPAApi;
import play.db.jpa.Transactional;
import play.i18n.MessagesApi;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.With;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import javax.persistence.metamodel.EntityType;
import java.util.*;
import java.util.stream.Stream;

import static ch.insign.playauth.PlayAuth.getPartyRoleManager;
import static play.libs.Json.toJson;


@With({GlobalActionWrapper.class, CspHeader.class})
@Transactional
public class PartyController extends Controller {
	private final static org.slf4j.Logger logger = LoggerFactory.getLogger(PartyController.class);

	private static Form<CustomerEmailTemplateForm> customerEmailTemplateFormForm;

	private MessagesApi messagesApi;
	private JPAApi jpaApi;
	private PlayAuthApi playAuthApi;

	@Inject
	public PartyController(MessagesApi messagesApi, JPAApi jpaApi, PlayAuthApi playAuthApi) {
		this.messagesApi = messagesApi;
		this.jpaApi = jpaApi;
		this.playAuthApi = playAuthApi;
	}

	@RequiresAuthentication
	public Result list() {
		playAuthApi.requirePermission(PartyPermission.BROWSE);

		List<Party> parties = new ArrayList<>(playAuthApi.getPartyManager().findAll());
		return ok(list.render(parties, new AdminContext(), ch.insign.cms.controllers.routes.PartyController.datatable().url()));
	}

	@RequiresAuthentication
	@SuppressWarnings("unchecked")
	public Result createParty() {
		playAuthApi.requirePermission(PartyPermission.ADD);
		DefaultParty party;

		try {
			party = (DefaultParty) playAuthApi.getPartyManager().getPartyClass().newInstance();
		} catch (InstantiationException | IllegalAccessException e) {
			return internalServerError("Cannot find party class.");
		}

		Form<DefaultParty> partyForm = (Form<DefaultParty>) SmartForm
				.form(party.getClass());
		partyForm = partyForm.fill(party);

		return ok(SecureForm.signForms(CMS.getPartyFormManager().createForm(partyForm)));
	}

	@RequiresAuthentication
	@SuppressWarnings("unchecked")
	public Result saveParty() {
		playAuthApi.requirePermission(PartyPermission.ADD);

		Form<DefaultParty> form = (Form<DefaultParty>) SmartForm.form(playAuthApi.getPartyManager().getPartyClass()).bindFromRequest();
		customerEmailTemplateFormForm = SmartForm.form(CustomerEmailTemplateForm.class).bindFromRequest();

		if (form.hasErrors() || validateCreateParty(form).size() > 0) {
			return badRequest(SecureForm.signForms(
					CMS.getPartyFormManager().createForm(form)
			));
		}

		DefaultParty partyForm = form.get();

		Party party = playAuthApi.getPartyManager().create(
				partyForm.getName(), partyForm.getPassword(), new EmailAddress(partyForm.getEmail()), PartyType.PERSON);

		CMS.getPartyEvents().onCreate(form, (DefaultParty) party);

		Stream.of(PartyPermission.READ,
				PartyPermission.EDIT,
				PartyPermission.REQUEST_PASSWORD_RESET,
				PartyPermission.EDIT_PASSWORD)
				.forEach(p -> playAuthApi.getAccessControlManager().allowPermission(party, p, party));

		// Check if we need to send customer email templates
		if (!customerEmailTemplateFormForm.hasErrors()) {
			CustomerEmailTemplateForm customerEmailTemplateForm = customerEmailTemplateFormForm.get();
			if (customerEmailTemplateForm.isSendEmail()) {
				sendCustomerEmailTemplate(party, customerEmailTemplateForm.getCustomerEmailTemplates());
			}
		}

		customerEmailTemplateFormForm = null;
		return redirect(CMS.getRouteResolver().listParties());
	}

	private Map<String, List<ValidationError>> validateCreateParty(Form<DefaultParty> form) {
		DefaultParty partyForm = form.get();
		Map<String, List<ValidationError>> errorList = new HashMap<>();

		if (partyForm.getPassword() == null || partyForm.getPassword().equals("")) {
			errorList.put("password", new ArrayList<>(Collections.singletonList(new ValidationError("password",
					messagesApi.get(lang(), "error.required")))));
		}

		if (partyForm.getPassword() != null && !partyForm.getPassword().equals(partyForm.getRepeatPassword())) {
			errorList.put("password", new ArrayList<>(Collections.singletonList(new ValidationError("password",
					messagesApi.get(lang(), "auth.password.signup.error.passwords_not_same")))));
		}

		// Check if email already exists
		if (playAuthApi.getPartyManager().findOneByPrincipal(partyForm.getEmail()) != null) {
			errorList.put("email", new ArrayList<>(Collections.singletonList(new ValidationError("email",
					messagesApi.get(lang(), "auth.signup.message.email.exist")))));
		}

		form.errors().putAll(errorList);

		return errorList;
	}

	private Map<String, List<ValidationError>> validateUpdateParty(Form<DefaultParty> form) {
		DefaultParty currParty = form.get();
		Map<String, List<ValidationError>> errorList = new HashMap<>();

		// Check if email already exists
		Party otherParty = playAuthApi.getPartyManager().findOneByPrincipal(currParty.getEmail());
		if (otherParty != null && !otherParty.getId().equals(currParty.getId())) {
			errorList.put("email", new ArrayList<>(Collections.singletonList(
					new ValidationError("email", messagesApi.get(lang(), "auth.signup.message.email.exist")))));
		}

		form.errors().putAll(errorList);

		return errorList;
	}

	@RequiresAuthentication
	public Result editParty(String id) {
		DefaultParty party = (DefaultParty) playAuthApi.getPartyManager().find(id);

		playAuthApi.requirePermission(PartyPermission.EDIT, party);

		if (party == null) {
			return notFound("Party not found: " + id);
		}

		@SuppressWarnings("unchecked")
		Form<DefaultParty> partyForm = (Form<DefaultParty>) SmartForm.form(party.getClass());
		partyForm = partyForm.fill(party);

		return ok(SecureForm.signForms(
				CMS.getPartyFormManager().editForm(partyForm, party)
		));
	}

	@RequiresAuthentication
	@SuppressWarnings("unchecked")
	public Result updateParty(String id) {
		DefaultParty party = (DefaultParty) playAuthApi.getPartyManager().find(id);

		jpaApi.em().detach(party); // prevent accidental auto-persist

		playAuthApi.requirePermission(PartyPermission.EDIT);

		if (party == null) {
			return notFound("User not found: " + id);
		}

		Form<DefaultParty> boundForm = ((Form<DefaultParty>) SmartForm.form(party.getClass()))
				.fill(party)
				.bindFromRequest();


		customerEmailTemplateFormForm = SmartForm.form(CustomerEmailTemplateForm.class).bindFromRequest();

		if (boundForm.hasErrors() || validateUpdateParty(boundForm).size() > 0) {
			return badRequest(SecureForm.signForms(
					CMS.getPartyFormManager().editForm(boundForm, party)
			));
		}

		String message = messagesApi.get(lang(), "backend.user.edit.message.success");

		// Check if we need to send customer email templates
		if (!customerEmailTemplateFormForm.hasErrors()) {
			CustomerEmailTemplateForm customerEmailTemplateForm = customerEmailTemplateFormForm.get();
			if (customerEmailTemplateForm.isSendEmail()) {
				message += " " + messagesApi.get(lang(), "backend.user.edit.message.email.sent");
				sendCustomerEmailTemplate(party, customerEmailTemplateForm.getCustomerEmailTemplates());
			}
		}

		party = jpaApi.em().merge(party);

		CMS.getPartyEvents().onUpdate(boundForm, party);

		customerEmailTemplateFormForm = null;
		flash("success", message);
		return redirect(CMS.getRouteResolver().editParty(id));
	}

	/**
	 * Send customer email templates
	 *
	 * @param party                  The party which was created or updated and to which we have to send emails
	 * @param customerEmailTemplates A list of customer email temlates to be sent
	 */
	protected void sendCustomerEmailTemplate(Party party, List<CustomerEmailTemplate> customerEmailTemplates) {
		if (customerEmailTemplates != null) {
			for (CustomerEmailTemplate customerEmailTemplate : customerEmailTemplates) {
				CustomerEmailTemplate sendingCustomerEmailTemplate = CustomerEmailTemplate.find.byId(customerEmailTemplate.getId());
				if (sendingCustomerEmailTemplate != null) {
					sendCustomerEmail(party, sendingCustomerEmailTemplate.getSubject().get(), sendingCustomerEmailTemplate.getContent().get());
				} else {
					logger.error("CustomerEmailTemplate not found: " + customerEmailTemplate.getId());
				}
			}
		} else {
			logger.error("CustomerEmailTemplate list empty");
		}
	}

	/**
	 * Send the actual customer email
	 *
	 * @param party   The part to send the email to
	 * @param subject The email subject (coming from the template)
	 * @param content The email content (coming from the template)
	 * @return True if sending the email was successful, false if there was an exception
	 */
	protected boolean sendCustomerEmail(Party party, String subject, String content) {
		HashMap<String, String> emailData = getCustomerEmailData(party);
		return CMS.getEmailService().sendWithCustomText(subject, content, party.getEmail(), emailData, Language.getCurrentLanguage());
	}

	/**
	 * Prepare the email data for the customer email template. Override this method to add additional fields
	 *
	 * @param party The created or edited party
	 * @return A hashmap of data sent to the email template
	 */
	protected HashMap<String, String> getCustomerEmailData(Party party) {
		HashMap<String, String> emailData = new HashMap<>();
		emailData.put("name", party.getName());
		return emailData;
	}

	@RequiresAuthentication
	public Result changePassword(String id) {
		final DefaultParty party = (DefaultParty) playAuthApi.getPartyManager().find(id);

		playAuthApi.requirePermission(PartyPermission.EDIT_PASSWORD, party);

		return ok(SecureForm.signForms(
				passwordForm.render(new AdminContext(), party, SmartForm.form(PasswordChange.class), null)
		));
	}

	@RequiresAuthentication
	public Result doChangePassword(String id) {
		final DefaultParty party = (DefaultParty) playAuthApi.getPartyManager().find(id);

		playAuthApi.requirePermission(PartyPermission.EDIT_PASSWORD, party);

		final Form<PasswordChange> form = SmartForm.form(PasswordChange.class).bindFromRequest();

		if (form.hasErrors()) {
			return badRequest(SecureForm.signForms(
					passwordForm.render(new AdminContext(), party, form, null)
			));
		}

		party.setCredentials(playAuthApi.getPasswordService().encryptPassword(form.get().getPassword()));
		CMS.getPartyEvents().onPasswordUpdate(form, party);

		flash("success", messagesApi.get(lang(), "backend.user.password.changed.msg"));
		return redirect(CMS.getRouteResolver().changePassword(party.getId()));
	}

	@RequiresAuthentication
	public Result listRole(String userId) {
		DefaultParty party = (DefaultParty) playAuthApi.getPartyManager().find(userId);

		List<PartyRole> roles = new ArrayList<>(playAuthApi.getPartyRoleManager().findAll());

		Form<DefaultParty> partyForm = SmartForm.form(DefaultParty.class);
		partyForm = partyForm.fill(party);

		return ok(SecureForm.signForms(
				rolesForm.render(new AdminContext(), party, partyForm, roles, null)
		));
	}

	@RequiresAuthentication
	public Result updatePartyRoles(String id) {
		DefaultParty party = (DefaultParty) playAuthApi.getPartyManager().find(id);

		if (party == null) {
			return notFound("Party not found: " + id);
		}

		Form<DefaultParty> userForm = SmartForm
				.form(DefaultParty.class)
				.fill(party)
				.bindFromRequest();


		try {
			// add all new roles first. otherwise user may not have permission to add them.
			getPartyRoleManager().findAll().stream()
					.filter(role -> isRoleSelected(role, userForm))
					.filter(role -> !party.getRoles().contains(role))
					.forEach(role -> {
						playAuthApi.requirePermission(PartyRolePermission.GRANT, role);
						party.addRole(role);
					});

			getPartyRoleManager().findAll().stream()
					.filter(role -> !isRoleSelected(role, userForm))
					.filter(role -> party.getRoles().contains(role))
					.forEach(role -> {
						playAuthApi.requirePermission(PartyRolePermission.REVOKE, role);
						party.removeRole(role);
					});
			flash("success", messagesApi.get(lang(), "backend.user.success.role.updated"));
		} catch (AuthorizationException e) {
			flash("error", messagesApi.get(lang(), "backend.user.error.cannotGrantOrRevokeRole"));
			return redirect(CMS.getRouteResolver().listRole(party.getId()));
		}

		return redirect(CMS.getRouteResolver().listRole(party.getId()));
	}

	public static Form<CustomerEmailTemplateForm> getCustomerEmailTemplateForm() {
		if (customerEmailTemplateFormForm == null) {
			customerEmailTemplateFormForm = SmartForm.form(CustomerEmailTemplateForm.class);
		}
		return customerEmailTemplateFormForm;
	}

	private static boolean isRoleSelected(PartyRole role, Form<?> form) {
		return form.data().entrySet().stream()
				.anyMatch(e -> e.getKey().contains("_roles") && e.getValue().equals(role.getId()));
	}

	private TypedQuery<DefaultParty> findByKeyword(String q, String orderByField, String orderByDirection) {
		CriteriaBuilder builder = jpaApi.em().getCriteriaBuilder();
		CriteriaQuery<DefaultParty> query = builder.createQuery(DefaultParty.class);

		EntityType<DefaultParty> userEntityType = jpaApi.em().getMetamodel().entity(DefaultParty.class);

		// join tables
		Root<DefaultParty> users = query.from(DefaultParty.class);
		List<Predicate> conditionsOr = new ArrayList<>();

		final List<String> userFields = new ArrayList<>(
				Arrays.asList("email", "name"));
		for (String field : userFields) {
			conditionsOr.add(builder.like(
					builder.lower(users.get(
							userEntityType.getDeclaredSingularAttribute(field, String.class))),
					"%" + q.toLowerCase() + "%"));
		}

		if (q.isEmpty() && orderByField.isEmpty() && orderByDirection.isEmpty()) {
			return jpaApi.em().createQuery(query
					.select(users)
					.where(
							builder.and(
									builder.or(conditionsOr.toArray(new Predicate[]{}))
							)
					)
					.distinct(true));
		}

		Expression orderByFieldExpression = users.get(orderByField);

		return jpaApi.em().createQuery(query
				.select(users)
				.where(
						builder.and(
								builder.or(conditionsOr.toArray(new Predicate[]{}))
						)
				)
				.orderBy(orderByDirection.toLowerCase().equals("desc")
						? builder.desc(orderByFieldExpression)
						: builder.asc(orderByFieldExpression))
				.distinct(true));

	}

	@RequiresAuthentication
	public Result datatable() {
		playAuthApi.requirePermission(PartyPermission.BROWSE);

		final List<String> orderByFields = new ArrayList<>(Arrays.asList(
				"name", "email", "roleName"));

		final String sEcho = request().getQueryString("sEcho");
		final int iDisplayStart = Integer.parseInt(request().getQueryString("iDisplayStart"));
		final int iDisplayLength = Integer.parseInt(request().getQueryString("iDisplayLength"));
		final String sSearch = request().getQueryString("sSearch");
		final int iSortCol_0 = Integer.parseInt(request().getQueryString("iSortCol_0"));
		final String sSortDir_0 = request().getQueryString("sSortDir_0");

		Logger.debug("Search users by keyword: " + sSearch + ". Offset: " + iDisplayStart + ":" + iDisplayLength);

		TypedQuery<DefaultParty> typedQuery = findByKeyword(
				sSearch,
				orderByFields.get(iSortCol_0),
				sSortDir_0
		);

		final int totalCountFiltered = typedQuery.getResultList().size();

		if (iDisplayLength > 0) {
			typedQuery
					.setFirstResult(iDisplayStart)
					.setMaxResults(iDisplayLength);
		}

		final List<DefaultParty> parties = typedQuery.getResultList();

		final List<List<String>> aaData = new ArrayList<>();
		for (DefaultParty party : parties) {
			final List<String> userData = new ArrayList<>();

			userData.add(String.format(
					"<a href=\"%s\">%s</a>",
					CMS.getRouteResolver().editParty(party.getId()),
					party.getName()));

			userData.add(String.format(
					"<a href=\"%s\">%s</a>",
					CMS.getRouteResolver().editParty(party.getId()),
					party.getEmail()));

			String roleNames = "";
			for (PartyRole role : party.getRoles()) {
				roleNames = roleNames + role.getName() + "<br />";
			}
			userData.add(roleNames);

			aaData.add(userData);
		}

		final HashMap<String, Object> result = new HashMap<>();
		result.put("sEcho", sEcho);
		result.put("iTotalRecords", playAuthApi.getPartyManager().findAll().size());
		result.put("iTotalDisplayRecords", totalCountFiltered);
		result.put("aaData", aaData);

		return ok(toJson(result));
	}

}
