/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.cms.controllers;

import ch.insign.cms.models.*;
import ch.insign.cms.permissions.ApplicationPermission;
import ch.insign.cms.permissions.NavigationPermission;
import ch.insign.cms.views.html.admin._navigationVpathList;
import ch.insign.cms.views.html.admin.navigationItemEdit;
import ch.insign.cms.views.html.admin.navigationOverview;
import ch.insign.cms.views.html.admin.navigationSearch;
import ch.insign.playauth.authz.DomainPermission;
import com.google.inject.Inject;
import play.data.FormFactory;
import ch.insign.commons.db.Paginate;
import ch.insign.commons.db.SmartForm;
import ch.insign.playauth.PlayAuth;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.common.primitives.Ints;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import play.data.DynamicForm;
import play.data.Form;
import play.db.jpa.JPA;
import play.db.jpa.JPAApi;
import play.db.jpa.Transactional;
import play.i18n.MessagesApi;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Results;
import play.mvc.With;

import javax.persistence.TypedQuery;
import java.io.IOException;

/**
 * Controller for all navigation-related admin actions
 *
 * @author bachi
 *
 */
@With(GlobalActionWrapper.class)
@Transactional
public class NavigationController extends Controller  {
	private static final Logger logger = LoggerFactory.getLogger(NavigationController.class);

    private MessagesApi messagesApi;
    private FormFactory formFactory;
    private JPAApi jpaApi;

    @Inject
    public NavigationController(MessagesApi messagesApi, FormFactory formFactory, JPAApi jpaApi) {
        this.messagesApi = messagesApi;
        this.formFactory = formFactory;
        this.jpaApi = jpaApi;
    }

    public Result index(String backURL, String mode) {
		if (mode == null) {
			if (session("navigation.mode") != null) {
				mode = session("navigation.mode");
			}
		} else {
			session("navigation.mode", mode);
		}
		boolean showAll = "all".equals(mode);

        // Allows access only for superusers
	    PlayAuth.requirePermission(ApplicationPermission.BROWSE_BACKEND);
        return ok(navigationOverview.render(backURL, showAll));
    }


	/**
	 * Save the add/edit form submission
	 * and redirect back to the originating page.
	 *
	 * @param backURL
	 */
    public Result save(String id, String parentId, String backURL) {

        Form<NavigationItem> boundForm = SmartForm.form(NavigationItem.class);
        String oldVpath = null;

	    NavigationItem oldItem = null;
        if (id != null && !id.equals("")) {
            oldItem = NavigationItem.find.byId(id);
            oldVpath = oldItem.getVirtualPath();
            boundForm = boundForm.fill(oldItem);
        }

	    if (oldItem != null) {
		    PlayAuth.requirePermission(NavigationPermission.EDIT, oldItem);
	    } else {
		    PlayAuth.requirePermission(NavigationPermission.ADD);
	    }

        boundForm = boundForm.bindFromRequest();
        logger.debug("item: " + boundForm.get());
        NavigationItem item = boundForm.get();

		// Check for other errors
        logger.debug("hasErrors: " + boundForm.hasErrors());
		if(boundForm.hasErrors()) {
			logger.warn("Bound form errors: " + boundForm.errors());
			flash("error", "Please correct the form below.");
            if (JPA.em().contains(item))
             {
                item.refresh(); // TODO: Better rollback the jpa transaction?
            }
			return badRequest(navigationItemEdit.render(item, boundForm, backURL, parentId));
		}

		// Check if a parent node needs to be set
		if (parentId != null && !parentId.equals("")) {
			try {
				//FIXME: use now with pageBlock: item.setParentNode(parentId);

			} catch (Exception e) {
				e.printStackTrace();
                if (JPA.em().contains(item))
                 {
                    item.refresh(); // TODO: Better rollback the jpa transaction?
                }
				return internalServerError(e.getMessage());
			}
		}

        // Check if the vpath was changed and the previous one should be added to the history
        if (boundForm.data().containsKey("addToVpathHistory") &&
                oldVpath != null && !item.getVirtualPath().equals(oldVpath)) {
            item.addVPathHistoryEntry(oldVpath);
        }

		item.save();

		// TODO: Flash

		// If a backURL was set, go there. Otherwise go to the nav item.
		if(backURL==null) {
            return Results.redirect(item.getURL());
        } else {
			return redirect(backURL);
		}
	}


    public Result createNavigationHistoryEntry(String id) {

	    PlayAuth.requirePermission(NavigationPermission.ADD);

        NavigationItem item = NavigationItem.find.byId(id);
        ObjectNode result = Json.newObject();

        DynamicForm requestData = formFactory.form().bindFromRequest();
        String vpath = requestData.get("vpath");

        if (item == null) {
            return notFound();
        }

        PlayAuth.requirePermission(NavigationPermission.EDIT, item);

        if (vpath.trim().equals("")) {
            result.put("status", "error");
            result.put("message", messagesApi.get(lang(), "navigation.collapse.error.required"));
            return ok(result);
        }

        if (item.getVpathHistory().contains(vpath)) {
            result.put("status", "error");
            result.put("message", messagesApi.get(lang(), "navigation.collapse.error.exists"));
            return ok(result);
        }

        if (item.getURL().equals(vpath)) {
            result.put("status", "error");
            result.put("message", messagesApi.get(lang(), "navigation.collapse.error.equalsMainUrl"));
            return ok(result);
        }

        for(NavigationItem navigationItem: NavigationItem.find.all()) {
            if(navigationItem.getURL().equals(vpath) || navigationItem.getVpathHistory().contains(vpath)) {
                result.put("status", "error");
                result.put("message", messagesApi.get(lang(), "navigation.collapse.error.exists_virtual_path"));
                return ok(result);
            }

        }

        item.getVpathHistory().add(vpath);
        item.save();

        result.put("status", "ok");
        result.put("content",_navigationVpathList.render(item).toString());

        return ok(result);
    }

    public Result reorder(String backURL) {

	    PlayAuth.requirePermission(NavigationPermission.EDIT);

        if (!request().body().asFormUrlEncoded().containsKey("navorder")) {
            internalServerError("Serialized ordering data missing");
        }

        String json = request().body().asFormUrlEncoded().get("navorder")[0];

        try {
            BlockFinder.flush();

            PageBlock.find.rootParent().sortSubBlocks(AbstractBlock.getSortedBlocksFromJsonNode(new ObjectMapper().readTree(json)));
            JPA.em().flush();
            BlockCache.flush(); // could be refined at some point if we could determine which blocks use page structure
            BlockFinder.flush();

        } catch (IOException e) {
            logger.error("Can't parse json: ", json);
            e.printStackTrace();
            return internalServerError("Can't parse data.");
        }

        return redirect(backURL);
    }


    /**
     * @param q Search query
     * @param p Search results page
     */
    public Result search(String q, int p, int itemsPerPage, boolean filterTrashedBlocks) {

        PlayAuth.requirePermission(NavigationPermission.BROWSE);

        if (itemsPerPage == 0) {
            itemsPerPage = NavigationItem.PAGINATE_DEFAULT;
        }


        final String searchQuery = toWildcardPattern(q);
        final TypedQuery<NavigationItem> query;
        final int total;

        // Try to detect current site, otherwise fallback to `includeAllSites`
        String currentSite = CMS.getSites().current().key;

        query = NavigationItem.find.queryByVpathOrVpathHistory(searchQuery, currentSite);
        total = Ints.checkedCast(NavigationItem.find.countByVpathOrVpathHistory(searchQuery, currentSite));

        Paginate<NavigationItem> pages = new Paginate<>(query, itemsPerPage, total);

        return ok(navigationSearch.render(pages, q, p, filterTrashedBlocks));
    }

    public Result deleteNavigationHistoryEntry(String navId, String entry) {
        NavigationItem item = NavigationItem.find.byId(navId);

        if (item == null) {
            return notFound();
        }

	    PlayAuth.requirePermission(NavigationPermission.DELETE, item);

        if (!item.getVpathHistory().contains(entry)) {
            if (entry.isEmpty() && item.getVpathHistory().contains(null)) {
                entry = null;
            } else {
                return notFound();
            }
        }

        item.getVpathHistory().remove(entry);
        item.save();

        return ok();
    }

    private String toWildcardPattern(String q) {
        StringBuilder pattern = new StringBuilder();

        if (!q.isEmpty()) {
            // search from the beginning?
            if (!q.startsWith("^")) {
                pattern.append("%");
            }

            pattern.append(q.replace("%", "").replace("^", "").replace("$", ""));

            // search from the end?
            if (!q.endsWith("$")) {
                pattern.append("%");
            }
        } else {
            pattern.append("%");
        }

        return pattern.toString();
    }

}
