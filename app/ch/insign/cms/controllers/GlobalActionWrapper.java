/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.cms.controllers;

import ch.insign.cms.CMSApi;
import ch.insign.cms.models.Metrics;
import ch.insign.cms.models.Template;
import ch.insign.cms.permissions.ApplicationPermission;
import ch.insign.playauth.PlayAuthApi;
import ch.insign.playauth.authz.AuthorizationHandler;
import ch.insign.playauth.controllers.actions.WithSubjectAction;
import com.codahale.metrics.Timer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import play.db.jpa.JPAApi;
import play.i18n.Lang;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Http.Context;
import play.mvc.Result;

import javax.inject.Inject;
import java.util.Optional;
import java.util.concurrent.CompletionStage;

/**
 * Global action interceptor to be used on all Action classes. Provides error handling,
 * global parameters like language-switching using ?lang=xy etc.
 *
 */
public class GlobalActionWrapper extends WithSubjectAction<Void> {
	private final static Logger logger = LoggerFactory.getLogger(GlobalActionWrapper.class);

    private static final String KEY_FLAGS = "flags";
    private static final String KEY_LANG = "lang";
    private static final String KEY_DEBUG = "debug";

    private JPAApi jpaApi;
    private CMSApi cmsApi;
    private PlayAuthApi authApi;
    private AuthorizationHandler authorizationHandler;
    private Metrics metrics;

    @Inject
    public GlobalActionWrapper(JPAApi jpaApi, CMSApi cmsApi, PlayAuthApi authApi, AuthorizationHandler authorizationHandler, Metrics metrics) {
        this.jpaApi = jpaApi;
        this.cmsApi = cmsApi;
        this.authApi = authApi;
        this.metrics = metrics;
        this.authorizationHandler = authorizationHandler;
    }

    @Override
    public CompletionStage<Result> doCall(final Context ctx) {

        jpaApi.withTransaction(() -> {
            // Do a backend permission check
		    if (ctx.request().path().startsWith(cmsApi.getConfig().backendPath())
				    && !ctx.request().path().equals(cmsApi.getConfig().backendPath() + "/login")
				    && !ctx.request().path().equals(cmsApi.getConfig().backendPath() + "/logout")) {

			    // WithSubjectAction.call handles the login redirect if required.
			    authApi.requirePermission(ApplicationPermission.BROWSE_BACKEND);
		    }

		    // Set the request's language
		    selectLanguage(ctx);

		    // ** Check for global params

		    // ?flags=yes - show me those flags in edit forms! :)
		    if (Controller.request().queryString().containsKey(KEY_FLAGS)) {
			    String hasFlags = Controller.request().getQueryString(KEY_FLAGS);
			    if ("yes".equals(hasFlags)) {
				    Controller.session().put(KEY_FLAGS, "yes");
			    } else if ("no".equalsIgnoreCase(hasFlags)) {
				    Controller.session().remove(KEY_FLAGS);
			    }
		    }

		    // ?debug=true/false: Enable/disable debugging info
		    String debugMode = Controller.request().getQueryString(KEY_DEBUG);
		    if (debugMode != null) {
                authApi.requirePermission(ApplicationPermission.DEBUG);
			    logger.info("Turning debug mode " + debugMode);
			    Template.setDebugMode(debugMode.equals("true"));
		    }
	    });

	    Timer.Context globalTimer = metrics.getPageTime().time();
	    Timer.Context specificTimer = metrics.getRegistry().timer(ctx.request().path()).time();

	    try {
            return delegate.call(ctx);
        } finally {
            globalTimer.stop();
            specificTimer.stop();
        }
    }

    @Override
    protected PlayAuthApi getPlayAuthApi() {
        return authApi;
    }

    @Override
    protected AuthorizationHandler getAuthorizationHandler() {
        return authorizationHandler;
    }

    // FIXME: Put the language features into the to-be-created user class
    public final static String KEY_PREF_LANG_FRONTEND ="cms.language.frontend";
	public final static String KEY_PREF_BACKEND_LANG ="cms.language.backend";

    /**
     * Generic language control
     * If visiting the frontend, Play's language cookie is used. If in the backend as admin, we use a separate language setting
     */
    private void selectLanguage(Context ctx) {
        try {
            boolean backend = Controller.request().uri().contains(cmsApi.getConfig().backendPath());

            // Prio 1: ?lang=xx: Language switching
            String changeLangReq = Controller.request().getQueryString("lang");
            if (changeLangReq != null && isLanguageAllowed(changeLangReq, backend)) {
                boolean res = Controller.changeLang(changeLangReq);
                if (res) {
                    logger.debug("Successfully changed language to: " + changeLangReq  + "(" + (backend ? KEY_PREF_BACKEND_LANG:KEY_PREF_LANG_FRONTEND) + ")");
                    // Update party's language preference
                    if ( ! authApi.isAnonymous()) {
                        authApi.getCurrentParty()
                                .ifPresent(p -> p.setPreferredOption(backend ? KEY_PREF_BACKEND_LANG : KEY_PREF_LANG_FRONTEND, changeLangReq));
                    }
                    return;
                } else {
                    logger.warn("Failed to change language to: " + changeLangReq);
                }
            }


            // Prio 2 for anon user: language (session) cookie
            if (authApi.isAnonymous()) {

                Http.Cookie langCookie = Controller.request().cookie(play.Play.langCookieName());
                if (langCookie != null && !backend) {
                    logger.debug("Language taken from cookie: " + langCookie.value()  + "(" + (backend ? KEY_PREF_BACKEND_LANG:KEY_PREF_LANG_FRONTEND) + ")");
                    return;
                }
            }
            // Prio 2 for authenticated user: user pref language (front- or backend)
            else {

                Optional<String> maybePrefLang = authApi.getCurrentParty()
                        .flatMap(p -> p.getPreferredOption(backend ? KEY_PREF_BACKEND_LANG : KEY_PREF_LANG_FRONTEND));

                if (maybePrefLang.isPresent()) {
                    String prefLang = maybePrefLang.get();
                    if (isLanguageAllowed(prefLang, backend)) {
                        Controller.changeLang(prefLang);
                        logger.debug("Set language to stored user's language: " + prefLang + "(" + (backend ? KEY_PREF_BACKEND_LANG : KEY_PREF_LANG_FRONTEND) + ")");
                        return;
                    }
                }
            }

            // Prio 3: browser language
            for (Lang acceptLang : Controller.request().acceptLanguages()) {
                String acceptLangStr = acceptLang.code().substring(0,2);
                if (cmsApi.getConfig().frontendLanguages().contains(acceptLangStr) && isLanguageAllowed(acceptLangStr, backend)) {
                    Controller.changeLang(acceptLangStr);
                    logger.debug("Set language to browser's language: " + acceptLangStr + "(" + (backend ? KEY_PREF_BACKEND_LANG:KEY_PREF_LANG_FRONTEND) + ")");
                    if ( ! authApi.isAnonymous()) {
                        authApi.getCurrentParty()
                                .ifPresent(p -> p.setPreferredOption(backend ? KEY_PREF_BACKEND_LANG : KEY_PREF_LANG_FRONTEND, acceptLangStr));
                    }
                    return;
                }
            }

        } catch (Exception e) {
            logger.error("Error while determining user language: " + e.getMessage(), e);
        }
    }

    /**
     * Check if language is allowed in config file.
     * @param lang
     * @param isBackend
     * @return
     */
    private boolean isLanguageAllowed(String lang, boolean isBackend) {
        if (isBackend) {
            return cmsApi.getConfig().backendLanguages().contains(lang);
        } else {
            return cmsApi.getConfig().frontendLanguages().contains(lang);
        }
    }
}
