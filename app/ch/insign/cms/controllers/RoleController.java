/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.cms.controllers;

import ch.insign.cms.views.admin.utils.AdminContext;
import com.google.inject.Inject;
import ch.insign.commons.db.SecureForm;
import ch.insign.commons.db.SmartForm;
import ch.insign.playauth.PlayAuth;
import ch.insign.playauth.authz.AccessControlManager;
import ch.insign.playauth.authz.DomainPermission;
import ch.insign.playauth.party.PartyRole;
import ch.insign.playauth.party.support.DefaultPartyRole;
import ch.insign.playauth.permissions.PartyRolePermission;
import ch.insign.playauth.utils.AuthzFormHandler;
import org.apache.shiro.authz.annotation.RequiresAuthentication;
import play.data.Form;
import play.data.FormFactory;
import play.db.jpa.JPA;
import play.db.jpa.JPAApi;
import play.db.jpa.Transactional;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.With;
import ch.insign.cms.views.html.admin.role.add;
import ch.insign.cms.views.html.admin.role.edit;
import ch.insign.cms.views.html.admin.role.list;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import static ch.insign.playauth.PlayAuth.getSecurityIdentity;

@With({GlobalActionWrapper.class, CspHeader.class})
public class RoleController extends Controller {
    private FormFactory formFactory;
    private JPAApi jpaApi;

    @Inject
    public RoleController(FormFactory formFactory, JPAApi jpaApi) {
        this.formFactory = formFactory;
        this.jpaApi = jpaApi;
    }

    @Transactional
    @RequiresAuthentication
    public Result list() {
	    PlayAuth.requirePermission(PartyRolePermission.BROWSE);

        List<PartyRole> securityRoles = new ArrayList<>(PlayAuth.getPartyRoleManager().findAll());
        return ok(list.render(new AdminContext(), securityRoles));
    }

    @Transactional
    @RequiresAuthentication
    public Result edit(String id) {
        DefaultPartyRole role = (DefaultPartyRole) PlayAuth.getPartyRoleManager().find(id);

        if (role == null) {
            return internalServerError("Role not found: " + id);
        }

	    PlayAuth.requirePermission(PartyRolePermission.READ, role);

        Form<DefaultPartyRole> roleForm = SmartForm
                .form(DefaultPartyRole.class)
                .fill(role);

        return ok(SecureForm.signForms(edit.render(new AdminContext(), id, roleForm, getDefinedPermissions())));
    }

    @Transactional
    @RequiresAuthentication
    public Result doEdit(String id) {
        DefaultPartyRole role = (DefaultPartyRole) PlayAuth.getPartyRoleManager().find(id);

        if (role == null) {
            return internalServerError("Role not found: " + id);
        }

	    PlayAuth.requirePermission(PartyRolePermission.EDIT, role);

        Form<DefaultPartyRole> form = SmartForm
                .form(DefaultPartyRole.class)
                .fill(role)
                .bindFromRequest();

        if (form.hasErrors()) {
            if (JPA.em().contains(role)) {
                JPA.em().refresh(role);
            }
            return badRequest(SecureForm.signForms(edit.render(new AdminContext(), id, form, getDefinedPermissions())));
        }

        AuthzFormHandler.save(getSecurityIdentity(role), null, form.data());

        save(form.get());

        flash(AdminContext.MESSAGE_SUCCESS, "Role " + form.get().getName() + " has been updated");

        return redirect(routes.RoleController.list());
    }

    @Transactional
    @RequiresAuthentication
    public Result add() {
	    PlayAuth.requirePermission(PartyRolePermission.ADD);

        Form<DefaultPartyRole> form = formFactory.form(DefaultPartyRole.class);
        return ok(SecureForm.signForms(add.render(new AdminContext(), form, getDefinedPermissions())));
    }

    @Transactional
    @RequiresAuthentication
    public Result doAdd() {
	    PlayAuth.requirePermission(PartyRolePermission.ADD);

        Form<DefaultPartyRole> form = SmartForm.form(DefaultPartyRole.class).bindFromRequest();

        if (form.hasErrors()) {
            return badRequest(SecureForm.signForms(add.render(new AdminContext(), form, getDefinedPermissions())));
        }

        DefaultPartyRole role = form.get();
        save(role);

        AuthzFormHandler.save(getSecurityIdentity(role), null, form.data());

        flash(AdminContext.MESSAGE_SUCCESS, "Role " + form.get().getName() + " has been created");

        return redirect(routes.RoleController.list());
    }

    @Transactional
    @RequiresAuthentication
    public Result delete(String id) {
        DefaultPartyRole role = (DefaultPartyRole) PlayAuth.getPartyRoleManager().find(id);

        if (role == null) {
            flash(AdminContext.MESSAGE_ERROR, "Role not found");
        } else {

	        PlayAuth.requirePermission(PartyRolePermission.DELETE, role);

            PlayAuth.getPartyRoleManager().delete(role);
            flash(AdminContext.MESSAGE_SUCCESS, "Role was deleted");
        }

        return redirect(routes.RoleController.list());
    }

    public Set<String> retrieveSubmittedPermissions(Form<DefaultPartyRole> form) {
	    return form.data().entrySet()
			    .stream()
			    .filter(e -> e.getKey().contains("_permissions"))
			    .map(Map.Entry::getValue)
			    .collect(Collectors.toSet());
    }

    private Map<String, List<DomainPermission<?>>> getDefinedPermissions() {
	    AccessControlManager acm = PlayAuth.getAccessControlManager();
	    return PlayAuth.getPermissionManager()
			    .getDefinedPermissions()
			    .stream()
			    .filter(PlayAuth::isRestricted) // don't manage "unrestricted" permissions
			    .collect(Collectors.groupingBy(DomainPermission::domain));
    }

    private void save(PartyRole role) {
        if (JPA.em().contains(role)) {
            JPA.em().merge(role);
        } else {
            JPA.em().persist(role);
        }
    }
}
