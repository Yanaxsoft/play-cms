/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.cms.controllers

import java.io.File
import javax.inject.{Inject, Singleton}

import ch.insign.cms.models.CMS
import controllers.Assets.Asset
import controllers.AssetsBuilder
import play.api.Environment
import play.api.http.{HttpErrorHandler, LazyHttpErrorHandler}
import play.api.mvc.{Action, AnyContent}

/**
  * @author Urs Honegger &lt;u.honegger@insign.ch&gt;
  */
@Singleton
class Assets @Inject() (errorHandler: HttpErrorHandler, environment: Environment) extends controllers.Assets(errorHandler) {
  private val notAllowedChars = """[/|\\]+""".r

  def lib(path: String, file: Asset) = versioned(path, file)
  def at(path: String, file: Asset) = versioned(path, file)

  def uploads(rootPath: String, file: String): Action[AnyContent] = Action { request =>
    if (!CMS.getConfig.isImageUploadEnable || notAllowedChars.findFirstMatchIn(s"$file").isDefined) {
      NotFound
    } else {

      val fileToServe = new File(environment.getFile(rootPath), file)
      if (fileToServe.exists) {
        Ok.sendFile(fileToServe, inline = true).withHeaders(CACHE_CONTROL -> "max-age=3600")
      } else {
        NotFound
      }
    }

  }
}

object Assets extends AssetsBuilder(LazyHttpErrorHandler) {
}
