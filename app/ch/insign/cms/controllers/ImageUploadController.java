/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.cms.controllers;

import ch.insign.cms.models.CMS;
import ch.insign.cms.permissions.aop.RequiresBackendAccess;
import ch.insign.cms.views.html.imageupload.jbTinyMceImagesUploadResult;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.inject.Inject;
import com.google.inject.Provider;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import play.Application;
import play.db.jpa.Transactional;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;
import play.mvc.With;

import java.io.File;

/**
 * Contains all actions for tinyMCE and standalone image upload.
 */
@With({GlobalActionWrapper.class, CspHeader.class})
@Transactional
public class ImageUploadController extends Controller {
	private static final Logger logger = LoggerFactory.getLogger(ImageUploadController.class);

    private Provider<Application> application;

    @Inject
    public ImageUploadController(Provider<Application> application) {
        this.application = application;
    }

    /**
     * Upload action for jbimages TinyMce plugin
     * @return
     */
    @RequiresBackendAccess
    public Result jbTinyMceImageUpload() {
        String appPath = application.get().path().getPath();

        Http.MultipartFormData body = request().body().asMultipartFormData();
        Http.MultipartFormData.FilePart picture = body.getFile("userfile");
        if (picture != null) {
            String fileName = picture.getFilename();
            String contentType = picture.getContentType();
            File tmpFile = (File) picture.getFile();
            File thumbnail = new File(appPath + CMS.getConfig().imageUploadRootPath() +File.separator + fileName);
            try {
                FileUtils.copyFile(tmpFile, thumbnail);
            } catch (Exception e) {
                e.printStackTrace();
                logger.debug("error: "+e.getMessage());
                return ok(jbTinyMceImagesUploadResult.render("", "error", "failed"));
            }

            // TODO: We could use _asset-helper here to create a link for the asset via CDN. But if we do so, all links
            // would be stored in db containing CDN-prefix. Is this what we want?
            return ok(jbTinyMceImagesUploadResult.render(
                    CMS.getConfig().imageUploadWWWRoot() + File.separator + fileName,
                    "file_uploaded",
                    "ok"
            ));
        } else {
            return ok(jbTinyMceImagesUploadResult.render("", "error", "failed"));
        }
    }

    /**
     * Upload action for standalone image upload
     * @return
     */
    @RequiresBackendAccess
    public Result imageUpload() {
        ObjectNode result = Json.newObject();
        String appPath = application.get().path().getPath();

        Http.MultipartFormData body = request().body().asMultipartFormData();
        Http.MultipartFormData.FilePart picture = body.getFile("userfile");
        if (picture != null) {
            String fileName = picture.getFilename();
            String contentType = picture.getContentType();
            File tmpFile = (File) picture.getFile();
            File thumbnail = new File(appPath + CMS.getConfig().imageUploadRootPath() +File.separator + fileName);
            try {
                FileUtils.copyFile(tmpFile, thumbnail);
            } catch (Exception e) {
                e.printStackTrace();
                logger.debug("error: " + e.getMessage());
                result.put("status", "error");
                return ok(result);
            }
            result.put("status", "ok");
            result.put("filename", CMS.getConfig().imageUploadWWWRoot() + File.separator + fileName);
            return ok(result);
        } else {
            result.put("status", "error");
            return ok(result);
        }
    }



}
