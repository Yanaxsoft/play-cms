/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.cms.controllers;

import ch.insign.cms.models.CMS;
import ch.insign.cms.models.Sites;
import ch.insign.cms.views.admin.utils.AdminContext;
import com.google.inject.Inject;
import play.data.FormFactory;
import ch.insign.commons.db.SecureForm;
import ch.insign.commons.db.SmartForm;
import play.db.jpa.JPA;
import play.db.jpa.JPAApi;
import play.db.jpa.Transactional;
import ch.insign.playauth.PlayAuth;
import ch.insign.cms.models.EmailTemplate;
import ch.insign.cms.permissions.EmailTemplatePermission;
import play.data.Form;
import play.i18n.MessagesApi;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.With;
import play.twirl.api.Html;
import ch.insign.cms.views.html.admin.emailTemplate.*;

/**
 * Controller for email template management in backend
 */
@With(GlobalActionWrapper.class)
@Transactional
public class EmailTemplateController extends Controller {

    protected MessagesApi messagesApi;
    protected FormFactory formFactory;
    protected JPAApi jpaApi;

    @Inject
    public EmailTemplateController(MessagesApi messagesApi, FormFactory formFactory, JPAApi jpaApi) {
        this.messagesApi = messagesApi;
        this.formFactory = formFactory;
        this.jpaApi = jpaApi;
    }

    public Result listForAllSites() {
        PlayAuth.requirePermission(EmailTemplatePermission.BROWSE);

        return ok(list.render(EmailTemplate.find.all()));
    }

    public Result listForCurrentSite() {
        PlayAuth.requirePermission(EmailTemplatePermission.BROWSE);

        Sites.Site currentSiteInstance = CMS.getSites().current();
        return ok(list.render(EmailTemplate.find.bySite(currentSiteInstance.key)));
    }

    public Result edit(String id) {
        EmailTemplate emailTemplate = EmailTemplate.find.byId(id);

        if (emailTemplate == null) {
            return ch.insign.cms.utils.Error.notFound(messagesApi.get(lang(), "backend.email.not.found", id));
        }

	    PlayAuth.requirePermission(EmailTemplatePermission.READ, emailTemplate);

        Form<EmailTemplate> form = SmartForm
                .form(EmailTemplate.class)
                .fill(emailTemplate);

        return ok(SecureForm.signForms(edit.render(form, emailTemplate, null)));
    }

    public Result doEdit(String id) {
        EmailTemplate emailTemplate = EmailTemplate.find.byId(id);

        if (emailTemplate == null) {
            return ch.insign.cms.utils.Error.notFound(messagesApi.get(lang(), "backend.email.not.found", id));
        }

	    PlayAuth.requirePermission(EmailTemplatePermission.EDIT, emailTemplate);

        Form<EmailTemplate> form = SmartForm
                .form(EmailTemplate.class)
                .fill(emailTemplate)
                .bindFromRequest();

        if (form.hasErrors()) {
            emailTemplate.refresh();

	        return badRequest(SecureForm.signForms(edit.render(form, emailTemplate, null)));

        }

        form.get().save();
        JPA.em().flush();

        flash(AdminContext.MESSAGE_SUCCESS, messagesApi.get(lang(), "backend.user.email.message.update", form.get().getDescription()));

        return redirect(ch.insign.cms.controllers.routes.EmailTemplateController.listForCurrentSite());
    }

    public Result add() {
	    PlayAuth.requirePermission(EmailTemplatePermission.ADD);

        Form<EmailTemplate> form = formFactory.form(EmailTemplate.class);

        String outputRaw = add.render(form, false, null).toString();
        outputRaw = SecureForm.signForms(outputRaw);
        Html output = Html.apply(outputRaw);

        return ok(output);
    }

    public Result doAdd() {
	    PlayAuth.requirePermission(EmailTemplatePermission.ADD);

        Form<EmailTemplate> form = SmartForm
                .form(EmailTemplate.class)
                .bindFromRequest();

        if (form.hasErrors()) {
            String outputRaw = add.render(form, false, null).toString();
            outputRaw = SecureForm.signForms(outputRaw);
            Html output = Html.apply(outputRaw);

            return badRequest(output);
        }

        EmailTemplate templateForSaving = form.get();

        // Before saving there should be attached site key/wildcart for template
        Sites.Site currentSiteInstance = CMS.getSites().current();
        String currentSiteKey = currentSiteInstance.key;
        templateForSaving.setSite(currentSiteKey);

        templateForSaving.save();

        flash(AdminContext.MESSAGE_SUCCESS, messagesApi.get(lang(), "backend.user.email.message.add", form.get().getDescription()));

        return redirect(ch.insign.cms.controllers.routes.EmailTemplateController.listForCurrentSite());
    }
}
