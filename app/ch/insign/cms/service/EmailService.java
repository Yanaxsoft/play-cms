/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.cms.service;

import ch.insign.cms.models.CMS;
import ch.insign.cms.models.EmailTemplate;
import ch.insign.cms.models.Sites;
import org.apache.commons.mail.HtmlEmail;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import play.Configuration;
import play.Play;
import play.i18n.Lang;
import play.i18n.Messages;
import ch.insign.cms.views.html.email_layout;
import play.twirl.api.Html;

import java.io.UnsupportedEncodingException;
import java.lang.String;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;

/**
 * Send email
 *
 * Layout - file views/email/layout.scala.html
 * Content - from db by template key
 *
 * Example:
 *
 * HashMap<String, String> emailData = new HashMap<>();
 * emailData.put("firstname", "Test");
 * emailData.put("lastname", "Insign");
 * EmailService.send("password.recovery", "test@insign.ch", emailData);
 */
public class EmailService {
	private final static Logger logger = LoggerFactory.getLogger(EmailService.class);

    /**
     * Every multisite can have unique templateKey in the borders of its site
     *
     * @param templateKey
     * @param recipient
     * @param data
     * @param language
     * @param site
     * @return
     */
    public boolean send(String templateKey, String recipient, HashMap<String, String> data, String language, String site) {

        EmailTemplate emailTemplate = EmailTemplate.find.byKey(templateKey, site);

        if (emailTemplate == null) {
            logger.error("Error. Email template with given key does not exist. Template key:  " + templateKey);
            return false;
        }

        return send(
                emailTemplate.getSubject().get(language, true),
                emailTemplate.getContent().get(language, true),
                recipient,
                data,
                emailTemplate.getSender(),
                emailTemplate.getBcc(),
                language,
                site);
    }

    public boolean send(String templateKey, String recipient, HashMap<String, String> data, String language) {
        return send(templateKey,
                recipient,
                data,
                language,
                CMS.getSites().current().key);
    }

    /**
     * Function was added to have possibity to change emailTemplate object properties (e.g. getSender is multilanguage in HTS project)
     *
     * @param emailTemplate
     * @param recipient
     * @param data
     * @param language
     * @param site
     * @return
     */
    public boolean send(EmailTemplate emailTemplate, String recipient, HashMap<String, String> data, String language, String site) {

        if (emailTemplate == null) {
            logger.error("Error. Email template with given key does not exist.");
            return false;
        }

        return send(
                emailTemplate.getSubject().get(language, true),
                emailTemplate.getContent().get(language, true),
                recipient,
                data,
                emailTemplate.getSender(),
                emailTemplate.getBcc(),
                language,
                site);
    }

    public boolean send(EmailTemplate emailTemplate, String recipient, HashMap<String, String> data, String language) {
        return send(emailTemplate,
                recipient,
                data,
                language,
                CMS.getSites().current().key);
    }

    /**
     * Send email with custom text
     *
     * @param subject
     * @param content
     * @param recipient
     * @param data
     * @param language
     * @return
     */
    public boolean sendWithCustomText(String subject, String content, String recipient, HashMap<String, String> data, String language) {
        return send(subject, content, recipient, data, null, null, language);
    }

    /**
     * Override this method to set own layout for mails
     *
     * @param content
     * @return
     */
    protected Html render(String content, String language) {
        return email_layout.render(content, language);

    }

    /**
     * Override this method to set own layout for mails
     *
     * @param content
     * @return
     */
    protected Html render(String content, String language, String site) {
        return render(content, language);
    }

    /**
     * Get email sender name
     * @return
     */
    protected String getSenderName(String language) {
        return Messages.get(new Lang(Lang.forCode(language)), "sender.name");
    }

    /**
     * Get email sender name
     * @return
     */
    protected String getSenderName(String language, String site) {
        return getSenderName(language);
    }

    private boolean send(String subject, String content, String recipient, HashMap<String, String> data, String sender, String bcc, String language) {
        content = render(parseString(content, data), language).toString();

        return sendEmail(
                subject,
                content,
                recipient,
                data,
                sender,
                getSenderName(language),
                bcc,
                language
        );
    }

    private boolean send(String subject, String content, String recipient, HashMap<String, String> data, String sender, String bcc, String language, String site) {
        content = render(parseString(content, data), language, site).toString();

        return sendEmail(
                subject,
                content,
                recipient,
                data,
                sender,
                getSenderName(language, site),
                bcc,
                language
        );
    }

    protected boolean sendEmail(String subject, String content, String recipient, HashMap<String, String> data, String sender, String senderName, String bcc, String language) {
        subject = parseString(subject, data);

        String host = CMS.getConfig().getEmailSmtpHost();
        int port = CMS.getConfig().getEmailSmtpPort();

        if (sender == null || sender.equals("")) {
            sender = CMS.getConfig().getEmailSender();
        }

        logger.info("Send email to " + recipient + ". Subject: " + subject);

        logEmail(sender, senderName, recipient, host + ":" + port, bcc, subject, content);

        if (!CMS.getConfig().isEmailMock()) {
            try {
                HtmlEmail email = new HtmlEmail();
                email.setHostName(host);
                email.setSmtpPort(port);
                email.setFrom(sender, senderName, "UTF-8");
                email.setSubject(subject);
                if (recipient != null && !recipient.equals("")) {
                    StringTokenizer st = new StringTokenizer(recipient, ",");
                    while (st.hasMoreTokens()) {
                        email.addTo(st.nextToken().trim());
                    }
                }
                email.setContent(content, "text/html; charset=utf-8");
                if (bcc != null && !bcc.equals("")) {
                    StringTokenizer st = new StringTokenizer(bcc, ",");
                    while (st.hasMoreTokens()) {
                        email.addBcc(st.nextToken().trim());
                    }
                }

                email.send();
            } catch (Exception e) {
                logger.error("Error sending email. Exception: ", e);
                return false;
            }
        }

        return true;
    }

    protected void logEmail(String sender, String senderName, String recipient, String host, String bcc, String subject, String content) {
        String modeIfno = "Mock mode disable.";

        if (CMS.getConfig().isEmailMock()) {
            modeIfno = "Mock mode enable (email send only in logs).";
        }

        try {
            logger.info(modeIfno);
            logger.info("FROM: " + " " + senderName + " " + sender, "UTF-8");
            logger.info("TO: " + recipient);
            logger.info("HOST: " + host);
            logger.info("BCC: " + bcc);
            logger.info("SUBJECT: " + subject);
            logger.info("CONTENT: " + new String(content.getBytes(), "UTF-8"));
        } catch (UnsupportedEncodingException e) {
            logger.error(modeIfno + " Error sending email. Exception: ", e);
        }
    }

    protected String parseString(final String text, final HashMap<String, String> data) {
        String result = text;

	    result = result.replace("{base_url}", Play.application().configuration().getString("application.baseUrl"));

        if (null != data) {
            for (Map.Entry<String, String> entry : data.entrySet()) {
                if (entry.getValue() != null) {
                    result = result.replace("{" + entry.getKey() + "}", entry.getValue());
                } else {
                    result = result.replace("{" + entry.getKey() + "}", "");
                }
            }
        }
        return result;
    }
}
