/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.cms.blocks.jotformpageblock.model;

import ch.insign.commons.db.Model;

import javax.persistence.*;
import java.util.List;
import java.util.Map;

/**
 * Form submissions synchronized with jot form api and associated with current users (parties) by email.
 */
@Entity
@Table(name = "cms_jotform_user_submissions")
@NamedQueries(value = {
		@NamedQuery(
				name = "JotFormSubmissions.findByUserEmail",
				query = "SELECT DISTINCT j FROM JotFormSubmissions j WHERE j.userEmail = :email "),
		@NamedQuery(
				name = "JotFormSubmissions.findByUserEmailAndSubmissionId",
				query = "SELECT DISTINCT j FROM JotFormSubmissions j WHERE j.userEmail = :email AND j.submissionId = :submissionId "),
        @NamedQuery(
                name = "JotFormSubmissions.findSubmissionId",
                query = "SELECT DISTINCT j FROM JotFormSubmissions j WHERE j.submissionId = :submissionId ")
})
public class JotFormSubmissions extends Model {

	public static JotFormSubmissionsFinder find = new JotFormSubmissionsFinder();

	private Long formId;

	private String formTitle;

	private Long submissionId;

	private String userEmail;

	@Lob
	@ElementCollection
	@MapKeyColumn(name="INPUT_NAME")
	@Column(name="INPUT_VALUE")
	@CollectionTable(name="cms_jotform_form_values", joinColumns=@JoinColumn(name="FORM_VALUE_ID"))
	private Map<String,String> formValues;

	public String getUserEmail() {
		return userEmail;
	}

	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}

	public Long getFormId() {
		return formId;
	}

	public void setFormId(Long formId) {
		this.formId = formId;
	}

	public String getFormTitle() {
		return formTitle;
	}

	public void setFormTitle(String formTitle) {
		this.formTitle = formTitle;
	}

	public Long getSubmissionId() {
		return submissionId;
	}

	public void setSubmissionId(Long submissionId) {
		this.submissionId = submissionId;
	}

	public Map<String, String> getFormValues() {
		return formValues;
	}

	public void setFormValues(Map<String, String> formValues) {
		this.formValues = formValues;
	}

	public static class JotFormSubmissionsFinder extends Model.Finder<JotFormSubmissions> {
		private JotFormSubmissionsFinder() {
			super(JotFormSubmissions.class);
		}

		public JotFormSubmissions findByUserEmailAndSubmissionId(String email, Long submissionId) {
			return jpaApi.em().createNamedQuery("JotFormSubmissions.findByUserEmailAndSubmissionId", JotFormSubmissions.class)
					.setParameter("email", email)
					.setParameter("submissionId", submissionId)
					.getResultList()
					.stream()
					.findFirst()
					.orElse(null);
		}

		public List<JotFormSubmissions> findByUserEmail(String email) {
			return jpaApi.em().createNamedQuery("JotFormSubmissions.findByUserEmail", JotFormSubmissions.class)
					.setParameter("email", email)
					.getResultList();
		}

        public List<JotFormSubmissions> findBySubmissionId(Long id) {
            return jpaApi.em().createNamedQuery("JotFormSubmissions.findSubmissionId", JotFormSubmissions.class)
                    .setParameter("submissionId", id)
                    .getResultList();
        }
	}
}
