/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.cms.blocks.jotformpageblock.controller;

import ch.insign.cms.CMSApi;
import ch.insign.cms.blocks.jotformpageblock.JotFormPageBlock;
import ch.insign.cms.blocks.jotformpageblock.actor.SyncFormSubmissions;
import ch.insign.cms.blocks.jotformpageblock.view.html.*;
import ch.insign.cms.controllers.CspHeader;
import ch.insign.cms.controllers.GlobalActionWrapper;
import ch.insign.cms.models.CMS;
import ch.insign.cms.blocks.jotformpageblock.model.JotForm;
import ch.insign.cms.blocks.jotformpageblock.permission.JotFormPermission;
import ch.insign.cms.permissions.aop.RequiresBackendAccess;
import ch.insign.cms.blocks.jotformpageblock.service.JotFormService;
import ch.insign.cms.views.admin.utils.AdminContext;
import com.google.inject.Inject;
import play.db.jpa.JPA;
import ch.insign.playauth.PlayAuth;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import play.db.jpa.Transactional;
import play.i18n.Messages;
import play.i18n.MessagesApi;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Results;
import play.mvc.With;

import java.io.File;
import java.io.FileOutputStream;
import java.util.HashMap;
import java.util.List;

import static play.libs.Json.toJson;

@With({GlobalActionWrapper.class})
@Transactional
@RequiresBackendAccess
public class JotFormController extends Controller {
	private static final Logger logger = LoggerFactory.getLogger(JotFormController.class);

	private MessagesApi messagesApi;
	private JotFormService jotForm;

	@Inject
	public JotFormController(MessagesApi messagesApi, CMSApi cmsApi) {
		this.messagesApi = messagesApi;
		this.jotForm = cmsApi.getJotFormService();
	}

	public Result list() {
		PlayAuth.requirePermission(JotFormPermission.VIEW);
		return ok(list.render());
	}

	public Result add(Long id) {
		PlayAuth.requirePermission(JotFormPermission.ADD);

		ObjectNode result = play.libs.Json.newObject();

		if (jotForm.exist(id)) {
			result.put("status", "error");
			result.put("message", messagesApi.get(lang(), "jotform.modal.exist.error", id));
			return ok(result);
		}

		JSONObject jotFormObject = JotFormService.getAPIform(id);

		if (!JotFormService.validResponseCode(jotFormObject)) {
			result.put("status", "error");
			result.put("message", messagesApi.get(lang(), "jotform.no.valid.error", id));
			return ok(result);
		}

		jotForm.create(jotFormObject);

    	result.put("status", "ok");
		return ok(result);
	}

	public Result delete(String id) {
		PlayAuth.requirePermission(JotFormPermission.DELETE);

		JotForm form = JotForm.find.byId(id);

		if (form == null) {
			return ch.insign.cms.utils.Error.notFound(messagesApi.get(lang(), "jotform.not.found", id));
		}

        List<JotFormPageBlock> jotFormPageBlocks = JotFormPageBlock.find.byApiJotFormId(form.getApiFormId()).getResultList();

        if (!jotFormPageBlocks.isEmpty()) {
            flash(AdminContext.MESSAGE_WARNING, messagesApi.get(lang(), "jotform.warning.message.delete", jotFormPageBlocks.size()));
            return Results.redirect(routes.JotFormController.list());
        }

		flash(AdminContext.MESSAGE_SUCCESS, messagesApi.get(lang(), "jotform.message.delete"));
		form.delete();
		return Results.redirect(routes.JotFormController.list());
	}

	/**
	 * @param id jotForm id
	 * @return jotForm template
	 */
	public Result preview(Long id) {
		PlayAuth.requirePermission(JotFormPermission.VIEW);
		return ok(preview.render(id));
	}

	/**
	 * Displays all forms that are in api
	 *
	 * @return api jotForms
	 */
	public Result listFormModal() {
		PlayAuth.requirePermission(JotFormPermission.VIEW);
		JSONObject form = jotForm.getUserForms();

		if (!JotFormService.validResponseCode(form)) {
            logger.debug(messagesApi.get(lang(), "jotform.no.valid.forms.error"));
			return ok(listFormsModal.render(null));
		}

		return ok(listFormsModal.render(jotForm.getUserForms()));
	}

	/**
	 * Displays all forms that are added for use in blocks
	 *
	 * @return jotForms
	 */
	public Result tableContent() {
		PlayAuth.requirePermission(JotFormPermission.VIEW);
		return ok(tableBodyContent.render());
	}

	/**
	 * @param id jotForm Id
	 * @param pageId JotFromPageBlock id
	 * @return api form submissions
	 */
	public Result formSubmissions(String pageId, String lang) {
		PlayAuth.requirePermission(JotFormPermission.VIEW_SUBMISSIONS);
		return ok(formSubmissions.render(pageId, lang));
	}

	public Result export(String pageId, String lang) {
		PlayAuth.requirePermission(JotFormPermission.EXPORT_SUBMISSIONS);

		int rowIndex = 0;

		try {
			SXSSFWorkbook wb = new SXSSFWorkbook(1000);
			wb.setCompressTempFiles(true);
			SXSSFSheet sh = (SXSSFSheet) wb.createSheet();
			Row rowHead = sh.createRow(rowIndex);

			for (int i = 0; i < jotForm.getFormSubmissionsHeaders(pageId, lang).size(); i++) {
				rowHead.createCell(i).setCellValue(jotForm.getFormSubmissionsHeaders(pageId, lang).get(i).get("sTitle").textValue());
			}

			for (JsonNode node : jotForm.getFormSubmissionsData(pageId, lang)) {
				Row rowBody = sh.createRow(++rowIndex);
				rowBody.createCell(0).setCellValue(rowIndex - 1);

				for (int i = 1; i < node.size(); i++) {
					rowBody.createCell(i).setCellValue(Jsoup.parse(node.get(i).textValue()).text());
				}
			}

			File tempFile = File.createTempFile("jotform-logs", ".xls");
			FileOutputStream out = new FileOutputStream(tempFile);
			wb.write(out);
			out.close();

			response().setHeader("Content-Disposition", "attachment; filename=jotform-logs.xls");

			return ok(tempFile);
		} catch (Exception e) {
			logger.error("In JotFormController export Exception: " + e.getMessage());
		}

		return redirect(routes.JotFormController.formSubmissions(pageId, lang));
	}

	public Result datatable(String pageId, String lang) {
		PlayAuth.requirePermission(JotFormPermission.VIEW_SUBMISSIONS);

		final HashMap<String, Object> result = new HashMap<>();

		result.put("aaData", jotForm.getFormSubmissionsData(pageId, lang));
		result.put("aoColumns", jotForm.getFormSubmissionsHeaders(pageId, lang));

		return ok(toJson(result));
	}

	/**
	 * Starts actor that updates form submissions from the jot form api
	 */
	public Result updateDataSubmissions() {
		SyncFormSubmissions.startOnce();
		return ok();
	}

	/**
	 * Immediately updates form submissions from the jot form api.
	 */
	public Result updateFormDataSubmission(Long id) {
		CMS.getJotFormService().syncJotFormSubmissions(id);
		return ok();
	}

	/**
	 * Display form submissions by the user's email
	 *
	 * @param user email
	 * @return view jotForm submissions
	 */
	public Result viewSubmissionJotForm(String email) {
		return ok(viewSubmissionJotForm.render(email));
	}
}
