/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.cms.blocks.jotformpageblock.actor;

import akka.actor.ActorSystem;
import akka.actor.Props;
import akka.actor.UntypedActor;
import ch.insign.cms.models.CMS;
import com.google.inject.Inject;
import play.db.jpa.JPA;
import ch.insign.commons.util.Configuration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import play.api.Play;
import play.db.jpa.JPAApi;
import scala.concurrent.duration.Duration;
import java.util.concurrent.TimeUnit;

/**
 * Actor is used for synchronization jot form submissions from jot form api
 * */
public class SyncFormSubmissions extends UntypedActor {
	private final static Logger logger = LoggerFactory.getLogger(SyncFormSubmissions.class);
	private final static String ACTOR_RECEIVE_MESSAGE = "START UPDATE";

	@Inject
	private static ActorSystem actorSystem;

	@Inject
	private static JPAApi jpaApi;

	public static void start() {
		actorSystem.scheduler().schedule(
				Duration.create(Configuration.getOrElse("jotform.api.akka.actor.SyncFormSubmissions.executeTime", 5), TimeUnit.SECONDS),
				Duration.create(Configuration.getOrElse("jotform.api.akka.actor.SyncFormSubmissions.interval", 12), TimeUnit.HOURS),
				actorSystem.actorOf(Props.create(SyncFormSubmissions.class)),
				ACTOR_RECEIVE_MESSAGE,
				actorSystem.dispatcher(),
				null
		);
	}

	public static void startOnce() {
		actorSystem.scheduler().scheduleOnce(
				Duration.create(1, TimeUnit.SECONDS),
				actorSystem.actorOf(Props.create(SyncFormSubmissions.class)),
				ACTOR_RECEIVE_MESSAGE,
				actorSystem.dispatcher(),
				null
		);
	}

	@Override
	public void onReceive(Object message) throws Exception {
		if (message.equals(ACTOR_RECEIVE_MESSAGE)) {
			jpaApi.withTransaction(() -> {
				logger.info("SyncFormSubmissions START");
                CMS.getJotFormService().syncAllJotFormsSubmissions();
				logger.info("SyncFormSubmissions END");
			});
		}
	}
}
