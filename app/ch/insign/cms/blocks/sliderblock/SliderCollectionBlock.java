/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.cms.blocks.sliderblock;

import ch.insign.cms.models.AbstractBlock;
import ch.insign.cms.models.CollectionBlock;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Predicate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import play.twirl.api.Html;
import play.data.Form;
import play.data.validation.Constraints;
import play.mvc.Controller;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static com.google.common.collect.Collections2.filter;

@Entity
@Table(name = "cms_block_collection_slider")
@DiscriminatorValue("SliderCollectionBlock")
//@SecuredEntity( name = "SliderCollectionBlock", actions = { "read", "modify" })
public class SliderCollectionBlock extends CollectionBlock  {
	private final static Logger logger = LoggerFactory.getLogger(SliderCollectionBlock.class);

    @Transient
    private String sliderEntryOrder;

    @Constraints.Required
    private Integer backgroundWidth = 1140;

    @Constraints.Required
    private Integer backgroundHeight = 365;

    @Override
    public Html render() {
        return ch.insign.cms.blocks.sliderblock.html.sliderCollectionBlock.render(this);
    }

    @Override
    public List<Class<? extends AbstractBlock>> getAllowedSubBlocks() {
        return new ArrayList<Class<? extends AbstractBlock>>(){{
            add(SliderEntryBlock.class);
        }};
    }

    @Override
    public Html editForm(Form editForm) {
        return ch.insign.cms.blocks.sliderblock.html.sliderCollectionBlockEdit.render(
                this,
                (Form<SliderCollectionBlock>) editForm,
                Controller.request().getQueryString("backURL"));
    }

    @Override
    public void save() {
        List<AbstractBlock> subBlocks = getSubBlocks();
        if(subBlocks.size() > 0) {
            try {
                System.out.println(getSortedBlocksFromJsonNode(new ObjectMapper().readTree(getSliderEntryOrder())));
                sortSubBlocks(getSortedBlocksFromJsonNode(new ObjectMapper().readTree(getSliderEntryOrder())));
            } catch(Exception e) {
                logger.error("SliderCollectionBlock.save() Exception: ", e);
            }
        }

        super.save();
    }

    public List<AbstractBlock> getSubSliderSubBlocks() {
        Predicate<AbstractBlock> validSliderEntityBlock = new Predicate<AbstractBlock>() {
            public boolean apply(AbstractBlock targetBlock) {
                SliderEntryBlock sliderEntryBlock = (SliderEntryBlock) targetBlock;
                return !sliderEntryBlock.getTitle().get().equals("");
            }
        };

        Collection<AbstractBlock> validSubBlocks = filter(super.getSubBlocks(), validSliderEntityBlock);

        return new ArrayList(validSubBlocks);
    }

    public String getSliderEntryOrder() {
        return sliderEntryOrder;
    }

    public void setSliderEntryOrder(String sliderEntryOrder) {
        this.sliderEntryOrder = sliderEntryOrder;
    }

    public int getBackgroundWidth() {
        return backgroundWidth;
    }

    public void setBackgroundWidth(int backgroundWidth) {
        this.backgroundWidth = backgroundWidth;
    }

    public int getBackgroundHeight() {
        return backgroundHeight;
    }

    public void setBackgroundHeight(int backgroundHeight) {
        this.backgroundHeight = backgroundHeight;
    }
}

