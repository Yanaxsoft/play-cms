/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.cms.blocks.reviewblock.controllers;

import ch.insign.cms.blocks.reviewblock.models.ReviewSubmission;
import ch.insign.cms.controllers.GlobalActionWrapper;
import ch.insign.cms.models.AbstractBlock;
import ch.insign.cms.models.CMS;
import ch.insign.cms.models.PageBlock;
import ch.insign.cms.permissions.ReviewPermission;
import ch.insign.cms.permissions.aop.RequiresBackendAccess;
import ch.insign.commons.db.Model;
import ch.insign.commons.db.Paginate;
import ch.insign.commons.db.SecureForm;
import ch.insign.commons.db.SmartForm;
import ch.insign.commons.i18n.Language;
import ch.insign.playauth.PlayAuth;
import com.google.common.primitives.Ints;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import javax.persistence.TypedQuery;

import com.google.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import play.data.Form;
import play.db.jpa.JPAApi;
import play.db.jpa.Transactional;
import play.libs.F;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.With;

@With(GlobalActionWrapper.class)
@Transactional
@RequiresBackendAccess
public class ReviewBackendController extends Controller {

	private final static Logger logger = LoggerFactory.getLogger(ReviewBackendController.class);

	private JPAApi jpaApi;

	@Inject
	public ReviewBackendController(JPAApi jpaApi) {
		this.jpaApi = jpaApi;
	}


	public Result list(String term, int page, int itemsPerPage) {
		PlayAuth.requirePermission(ReviewPermission.BROWSE);

		F.Tuple<TypedQuery<ReviewSubmission>, Long> result = ReviewSubmission.find.byUserKeywords(term);
		Paginate<ReviewSubmission> paginate = new Paginate<>(result._1, itemsPerPage, Ints.checkedCast(result._2));

		return ok(ch.insign.cms.blocks.reviewblock.view.admin.html.list.render(paginate, page, term));
	}

	public Result approve(String id) {
		PlayAuth.requirePermission(ReviewPermission.APPROVE);

		ReviewSubmission submission = ReviewSubmission.find.byId(id);

		if (null == submission) {
			return notFound("Review submission with id " + id + " not found");
		}

		submission.approve();
		submission.save();

		return redirect(ch.insign.cms.blocks.reviewblock.controllers.routes.ReviewBackendController.list(null, 1, 0));
	}

	public Result delete(String id) {
		PlayAuth.requirePermission(ReviewPermission.DELETE);

		ReviewSubmission submission = ReviewSubmission.find.byId(id);

		if (null == submission) {
			return notFound("Review submission with id " + id + " not found");
		}

		submission.getReview().removeSubmission(submission);
		submission.delete();
		submission.getReview().refreshStatistic();
		submission.getReview().getTarget()
				.ifPresent(block -> block.markModified());

		return redirect(ch.insign.cms.blocks.reviewblock.controllers.routes.ReviewBackendController.list(null, 1, 0));
	}

	public Result reply(String id) {
		PlayAuth.requirePermission(ReviewPermission.REPLY);

		ReviewSubmission submission = ReviewSubmission.find.byId(id);

		if (submission == null) {
			return notFound("Review submission with id " + id + " not found");
		}

		Form<ReviewSubmission> reviewForm = SmartForm
				.form(ReviewSubmission.class)
				.fill(submission);

		return ok(SecureForm.signForms(ch.insign.cms.blocks.reviewblock.view.admin.html.reply.render(id, reviewForm, submission)));
	}

	public Result doReply(String id) {
		PlayAuth.requirePermission(ReviewPermission.REPLY);

		ReviewSubmission submission = ReviewSubmission.find.byId(id);

		if (submission == null) {
			return notFound("Review submission with id " + id + " not found");
		}

		Form<ReviewSubmission> form = SmartForm
				.form(ReviewSubmission.class)
				.fill(submission)
				.bindFromRequest();

		if (form.hasErrors()) {
			submission.refresh();
			return badRequest(SecureForm.signForms(ch.insign.cms.blocks.reviewblock.view.admin.html.reply.render(id, form, submission)));
		}

		form.get().setAdminAnswerDate(new Date());
		form.get().save();
		jpaApi.em().flush();

		// If the reviewer's email is known, send a notification email about the answer
		if (submission.getEmail() != null) {
			sendReplyNotificationMail(submission);
		}

		return redirect(ch.insign.cms.blocks.reviewblock.controllers.routes.ReviewBackendController.list(null, 1, 0));
	}

	private void sendReplyNotificationMail(ReviewSubmission submission) {
		SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy HH:mm");

		HashMap<String, String> emailData = new HashMap<>();
		emailData.put("reviewEmail", submission.getEmail());
		emailData.put("reviewTitle", submission.getReview()
				.getTarget()
				.filter(b -> b instanceof PageBlock)
				.map(b -> ((PageBlock) b).getPageTitle().get())
				.orElse(submission.getReview().getTitle().get()));
		emailData.put("reviewDate", sdf.format(submission.getCreatedOn()));
		emailData.put("reviewReply", submission.getAdminAnswer());

		CMS.getEmailService().send("reviews.reply", submission.getEmail(), emailData, Language.getDefaultLanguage());
	}

}
