/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.cms.blocks.horizontalcollection;

import ch.insign.cms.models.BlockFinder;
import ch.insign.cms.models.CollectionBlock;
import ch.insign.cms.models.ContentBlock;
import org.slf4j.LoggerFactory;
import play.data.Form;
import play.mvc.Controller;
import play.twirl.api.Html;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Created by bachi on 30.06.16.
 */
@Entity
@Table(name = "cms_block_horizontalcollection")
@DiscriminatorValue("HorizontalCollectionBlock")
public class HorizontalCollectionBlock extends CollectionBlock {
	private final static org.slf4j.Logger logger = LoggerFactory.getLogger(HorizontalCollectionBlock.class);

	public static BlockFinder<HorizontalCollectionBlock> find = new BlockFinder<>(HorizontalCollectionBlock.class);

	@Override
	public Html render() {
		return ch.insign.cms.blocks.horizontalcollection.html.horizontalCollectionShow.render(this);
	}

	@SuppressWarnings("rawtypes")
	@Override
	public Html editForm(Form editForm) {
		String backURL = Controller.request().getQueryString("backURL");
		return ch.insign.cms.blocks.horizontalcollection.html.horizontalCollectionEdit.render(this, editForm, backURL, null);
	}



}
