/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.cms.models;

import ch.insign.playauth.controllers.RouteResolver;
import play.api.mvc.Call;
import ch.insign.cms.controllers.routes;

/**
 * Created by anton on 25.07.14.
 */
public class DefaultRouteResolver implements RouteResolver {
    @Override
    public Call login() {
        return routes.AuthController.login();
    };

    @Override
    public Call logout(String backURL) {
        return routes.AuthController.logout(backURL != null ? backURL : "/");
    }

    @Override
    public Call listParties() {
        return routes.PartyController.list();
    }

    @Override
    public Call editParty(String id) {
        return routes.PartyController.editParty(id);
    }

    @Override
    public Call updateParty(String id) {
        return routes.PartyController.updateParty(id);
    }

    @Override
    public Call listRole(String id) {
        return routes.PartyController.listRole(id);
    }

    @Override
    public Call updatePartyRoles(String id) {
        return routes.PartyController.updatePartyRoles(id);
    }

    @Override
    public Call changePassword(String id) {
        return routes.PartyController.changePassword(id);
    }

    @Override
    public Call doChangePassword(String id) {
        return routes.PartyController.doChangePassword(id);
    }

    @Override
    public Call createParty() {
        return routes.PartyController.createParty();
    }

    @Override
    public Call saveParty() {
        return routes.PartyController.saveParty();
    }
}
