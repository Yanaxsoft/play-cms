/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.cms.models;

import ch.insign.cms.views.admin.utils.AdminContext;
import ch.insign.playauth.party.support.DefaultParty;
import play.twirl.api.Html;
import play.data.Form;
import play.data.validation.ValidationError;

import java.util.List;

/**
 * Created by anton on 04.08.14.
 */
public class DefaultPartyHandler implements PartyFormManager, PartyEvents {

    @Override
    public Html editForm(Form partyForm, DefaultParty party) {
        return ch.insign.cms.views.html.admin.party.editForm.render(new AdminContext(), partyForm, party, null, null);
    }

    @Override
    public Html createForm(Form partyForm) {
        return ch.insign.cms.views.html.admin.party.createForm.render(partyForm, null);
    }


    @Override
    public void onCreate(Form form, DefaultParty party) {

    }

    @Override
    public void onUpdate(Form form, DefaultParty party) {

    }

    @Override
    public void onDelete(DefaultParty party) {

    }

    @Override
    public void onPasswordUpdate(Form form, DefaultParty party) {
    }
}
