/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.cms.models;

import ch.insign.commons.i18n.Language;
import ch.insign.commons.util.Configuration;
import ch.insign.playauth.PlayAuth;
import ch.insign.playauth.authz.SecurityIdentity;
import net.sf.ehcache.*;
import net.sf.ehcache.config.CacheConfiguration;
import net.sf.ehcache.config.MemoryUnit;
import net.sf.ehcache.config.Searchable;
import net.sf.ehcache.config.SizeOfPolicyConfiguration;
import net.sf.ehcache.search.Query;
import net.sf.ehcache.search.Results;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import play.twirl.api.Html;
import play.mvc.Controller;

import java.util.Date;
import java.util.Optional;
import java.util.function.Supplier;

/**
 * Caching for cms blocks. Use block.cache() to retrieve a block's cache instance.
 *
 * Note: Currently, Play's default Cache configuration is used. We could easily create our own BlockCache
 * to allow custom configuration and size limits etc.
 * See: http://ehcache.org/documentation/code-samples#Using-the-CacheManager
 *
 * @author bachi
 */
public class BlockCache {
	private final static Logger logger = LoggerFactory.getLogger(BlockCache.class);

	private static final String PREFIX = "block-";
	private static final String MANAGER_NAME = "play-cms-manager";
	private static final String CACHE_NAME = "play-cms-blockcache";
	protected AbstractBlock block;

	// Assume conservative defaults
	private boolean enabled = Configuration.getOrElse("cms.blockcache.enabled", false); // (default for this block instance, not global)
	private boolean handleGET = Configuration.getOrElse("cms.blockcache.handleGET", false);
	private boolean handlePOST = Configuration.getOrElse("cms.blockcache.handlePOST", false);
	private boolean ignoreUser = Configuration.getOrElse("cms.blockcache.ignoreUser", false);
	private boolean ignoreGroups = Configuration.getOrElse("cms.blockcache.ignoreGroups", false);
	private boolean ignoreLanguage = Configuration.getOrElse("cms.blockcache.ignoreLanguage", false);

	{   // Initialize the cache early.
		BlockCache.cache();
	}

	public BlockCache(AbstractBlock block) {
		this.block = block;
	}

	public static CacheManager manager() {
		CacheManager manager = CacheManager.getCacheManager(MANAGER_NAME);

		if (manager == null) {

			net.sf.ehcache.config.Configuration config = new net.sf.ehcache.config.Configuration();
			config.setName(MANAGER_NAME);
			manager = CacheManager.newInstance(config);
		}

		return manager;
	}

	public static Ehcache cache() {
		CacheManager manager = manager();
		synchronized (manager) {
			Cache cache = manager.getCache(CACHE_NAME);

			if (cache == null) {

				Searchable searchable = new Searchable();
				searchable.keys(true);

				SizeOfPolicyConfiguration sizeOfPolicyConfiguration = new SizeOfPolicyConfiguration();
				sizeOfPolicyConfiguration.setMaxDepth(1000000);
				sizeOfPolicyConfiguration.setMaxDepthExceededBehavior("abort");

				CacheConfiguration config = new CacheConfiguration()
						.name(CACHE_NAME)
						.timeToLiveSeconds(Configuration.getOrElse("cms.blockcache.ttl", 7 * 24 * 3600))
						.diskPersistent(false)
						.maxBytesLocalHeap(Configuration.getOrElse("cms.blockcache.size", 16), MemoryUnit.MEGABYTES)
						.searchable(searchable);
				config.addSizeOfPolicy(sizeOfPolicyConfiguration);

				cache = new Cache(config);
				manager.addCache(cache);
			}

			return cache;
		}
	}

	/**
	 * Get the cached content.
	 *
	 * Hint: Use useCache() first to determine if the cached version should be used in this request.
	 */
	public Html get() {
		if (cache().getStatus() != Status.STATUS_ALIVE) return null;
		return (Html) Optional
				.ofNullable(cache().get(getKey()))
				.map(Element::getObjectValue)
				.orElse(null);
	}

	/**
	 * Get the cached html content or the result of the render function.
	 * If the content is rendered, the result is then added to the cache.
	 */
	public Html getOrElse(Supplier<Html> render) {
		if (useCache()) {

			if (isCached()) {
				logger.debug("Using cached " + getKey());
				return get();
			} else {
				logger.debug("Rendering " + getKey());
				Html result = render.get();
				set(result);
				return result;
			}

		} else {
			return render.get();
		}

	}

	public void set(Html data) {
		if (cache().getStatus() != Status.STATUS_ALIVE) return;
		String key = getKey();
		logger.debug("Caching " + key);

		// calculate nearest expiration date for block and all it's subBlocks
		Date expireOn = getCacheTreeExpirationDate(block);
		if (expireOn != null) {
			Date now = new Date();
			Integer timeToLive = (int) (Math.abs(expireOn.getTime() - now.getTime()) / 1000);
			cache().put(new Element(key, data, null, null, timeToLive));
		} else {
			cache().put(new Element(key, data));
		}
	}

	/**
	 * Get nearest date when cache element for this block should expired
	 *
	 * @return either displayFrom or displayTo if they are in the future
	 */
	public Date getCacheExpirationDate(AbstractBlock targetBlock) {
		Date now = new Date();
		if (targetBlock.getDisplayFrom() != null && targetBlock.getDisplayFrom().after(now)) {
			return targetBlock.getDisplayFrom();
		}

		if (targetBlock.getDisplayTo() != null && targetBlock.getDisplayTo().after(now)) {
			return targetBlock.getDisplayTo();
		}
		return null;
	}

	/**
	 * Get nearest date when cache element for this block and all it's subBlocks should expired
	 */
	public Date getCacheTreeExpirationDate(AbstractBlock rootBlock) {
		Date blockExpireOn = getCacheExpirationDate(rootBlock);
		Date now = new Date();

		for (AbstractBlock subBlock: rootBlock.getSubBlocks()) {
			Date subBlockExpireOn = getCacheTreeExpirationDate(subBlock);
			if (subBlockExpireOn != null && subBlockExpireOn.after(now)) {
				if (blockExpireOn == null) {
					blockExpireOn = subBlockExpireOn;
				} else if (subBlockExpireOn.before(blockExpireOn)) {
					blockExpireOn = subBlockExpireOn;
				}
			}
		}

		return blockExpireOn;
	}

	public void invalidate() {

		if (cache().getStatus() != Status.STATUS_ALIVE) return;

		String key = getFixedKeyPart()
			.append("*")
			.toString();

		Results results = cache().createQuery()
			.includeKeys()
			.addCriteria(Query.KEY.ilike(key))
			.execute();

		logger.debug("Invalidating " + results.all().size() + " cache entries for " + key);
		results.all().forEach(r -> cache().remove(r.getKey()));
	}

	/**
	 * Remove all block cache entries.
	 */
	public static void flush() {
		if (cache().getStatus() != Status.STATUS_ALIVE) return;
		logger.info("Flushing the entire cache (" + cache().getSize() + " entries)");
		cache().removeAll();
	}

	public boolean isCached() {
		if (cache().getStatus() != Status.STATUS_ALIVE) return false;
		return cache().isKeyInCache(getKey()) && cache().get(getKey()) != null;
	}

	/**
	 * Determines if caching should be used (according to the cache configuration and request)
	 * (use isCached() to check if a cached version is available)
	 */
	public boolean useCache() {

		if (!enabled ||
			Template.isDebugMode() ||
			(Controller.request().queryString().containsKey("cache") && Controller.request().getQueryString("cache").equals("false") /** FIXME && isAdmin **/)
			) {

			return false;
		}

		if (!handleGET && Controller.request().queryString().size()>0) {
			return false;
		}

		if (!handlePOST && Controller.request().method().equals("POST")) {
			return false;
		}

		return true;
	}

//	public Html refresh(AbstractBlock block) {
//		return null;
//	}

	/**
	 * Get a unique cache key for this block
	 * Note: The block must be persisted.
	 */
	public String getKey() {
		if (block == null || block.getId() == null) {
			logger.error("Cannot use BlockCache with null or not persisted block!");
			return "";
		}

		// TODO: More and configurable and dependencies, (partially) cached key

		StringBuilder sb = getFixedKeyPart();

		if (!ignoreLanguage) {
			sb.append(Language.getCurrentLanguage());
		}

		if (!(ignoreUser && ignoreGroups)) {
			Object currentUser = Optional
					.ofNullable((Object) PlayAuth.getCurrentParty())
					.orElse(SecurityIdentity.ALL);
			sb
				.append("-")
				.append(PlayAuth.getAuthorizationHash(currentUser));
		}

		return sb.toString();
	}

	/**
	 * The only-block-specific part of the key
	 * Used to remove all cache entries created from this block.
	 */
	private StringBuilder getFixedKeyPart() {
		return new StringBuilder()
			.append(PREFIX)
			.append(block.getId())
			.append("-");
	}

	/**
	 * Enable caching?
	 */
	public BlockCache setEnabled(boolean enabled) {
		this.enabled = enabled;
		return this;
	}

	/**
	 * Cache responses to POST requests?
	 */
	public BlockCache handlePOST(boolean handlePOST) {
		this.handlePOST = handlePOST;
		return this;
	}

	/**
	 * Cache responses to requests that contain GET parameters?
	 */
	public BlockCache handleGET(boolean handleGET) {
		this.handleGET = handleGET;
		return this;
	}

	public BlockCache ignoreUser(boolean ignoreUser) {
		this.ignoreUser = ignoreUser;
		return this;
	}

	public BlockCache ignoreGroups(boolean ignoreGroups) {
		this.ignoreGroups = ignoreGroups;
		return this;
	}

	public BlockCache ignoreLanguage(boolean ignoreLanguage) {
		this.ignoreLanguage = ignoreLanguage;
		return this;
	}
}
