/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.cms.models;

import ch.insign.commons.db.MString;
import ch.insign.commons.db.Model;
import play.data.validation.Constraints;
import play.data.validation.ValidationError;
import play.i18n.Messages;

import javax.persistence.*;
import java.util.*;

/**
 * EmailTemplate model provides storing common multilingual email templates and using them in sending emails
 */
@Entity
@Table(name = "cms_email_template", indexes = {@Index(name = "template_key", columnList = "templateKey")})
@Inheritance(strategy = InheritanceType.JOINED)
@DiscriminatorValue("EmailTemplate")
public class EmailTemplate extends Model {

    @Constraints.Required
    private String templateKey;

    private String site;

    private String sender;
    private String bcc;

    @MString.MStringRequired
    @OneToOne(cascade= CascadeType.ALL)
    private MString subject = new MString();

    @MString.MStringRequired
    @OneToOne(cascade= CascadeType.ALL)
    private MString description = new MString();

    @OneToOne(cascade= CascadeType.ALL)
    private MString content = new MString();

	@Constraints.Required
	@Enumerated(EnumType.STRING)
	private EmailTemplateCategory category;

    public static final EmailTemplateFinder find = new EmailTemplateFinder();

    public MString getDescription() {
	    return description;
    }

    public void setDescription(MString description) {
        this.description = description;
    }

    public String getTemplateKey() {
        return templateKey;
    }

    public void setTemplateKey(String templateKey) {
        this.templateKey = templateKey;
    }

    public MString getContent() {
        return content;
    }

    public void setContent(MString content) {
        this.content = content;
    }

    public MString getSubject() {
        return subject;
    }

    public void setSubject(MString subject) {
        this.subject = subject;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getBcc() {
        return bcc;
    }

    public void setBcc(String bcc) {
        this.bcc = bcc;
    }

	public EmailTemplateCategory getCategory() {
		return category;
	}

	public void setCategory(EmailTemplateCategory category) {
		this.category = category;
	}

    public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site;
    }

    public enum EmailTemplateCategory {
		INTERN("INTERN"),
		EXTERN("EXTERN");

		private final String id;

		EmailTemplateCategory(String id) {
			this.id = id;
		}

        public String getId() {
            return id;
        }

        public static Map<String, String> options() {
			LinkedHashMap<String, String> values = new LinkedHashMap<>();
			for (EmailTemplateCategory category : EmailTemplateCategory.values()) {
				values.put(category.getId(), category.getEmailTemplateCategoryAsString());
			}
			return values;
		}

		public String getEmailTemplateCategoryAsString() {
			return Messages.get("backend.user.email.category." + name());
		}
	}

    public List<ValidationError> validate() {
        List<ValidationError> errors = new ArrayList<>();
        EmailTemplate emailTemplateByKey = EmailTemplate.find.byKey(getTemplateKey(), getSite());

        if (EmailTemplate.find.byKey(getTemplateKey(), getSite()) != null) {
            if (getId() == null || !getId().equals(emailTemplateByKey.getId())) {
                errors.add(new ValidationError("templateKey", Messages.get("backend.mail.templates.error.keyExists")));
            }
        }

        return errors.isEmpty() ? null : errors;
    }

    public static class EmailTemplateFinder extends Model.Finder<EmailTemplate> {
        public EmailTemplateFinder() {
            super(EmailTemplate.class);
        }

        public EmailTemplate byKey(String templateKey) {
            try {
                return byKey(templateKey, CMS.getSites().current().key);
            } catch(NoResultException e) {
                return null;
            }
        }

        public EmailTemplate byKey(String templateKey, String site) {
            try {
                return query().andEquals("templateKey", templateKey).andEquals("site",site).getSingleResult();
            } catch(NoResultException e) {
                return null;
            }
        }

        public List<EmailTemplate> bySite(String site) {
            return query().andEquals("site", site).getResultList();
        }

    }
}
