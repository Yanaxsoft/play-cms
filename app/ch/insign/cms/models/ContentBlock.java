/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.cms.models;

import ch.insign.commons.db.MString;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

import ch.insign.cms.views.html.admin.contentBlockEdit;
import ch.insign.cms.views.html.contentBlock;
import play.twirl.api.Html;
import play.data.Form;
import play.mvc.Controller;

import javax.persistence.*;

/**
 * A simple content block providing (multi-language) title and content properties.
 * TODO: Tidy up, remove unneccessary props
 */
@Entity
@Table(name = "cms_block_content")
@DiscriminatorValue("ContentBlock")
public class ContentBlock extends BaseBlock  {
	public static final Integer DEFAULT_WIDTH = 4;

	private final static Logger logger = LoggerFactory.getLogger(ContentBlock.class);

	public static BlockFinder<ContentBlock> find = new BlockFinder<>(ContentBlock.class);

    @OneToOne (cascade=CascadeType.ALL)
    private MString title = new MString();

    @OneToOne (cascade=CascadeType.ALL)
    private MString content = new MString();

	private Integer colWidth = DEFAULT_WIDTH;

    public MString getTitle() {
        return title;
    }

    public void setTitle(MString title) {
        this.title = title;
    }

	public MString getContent() {		
		return content;
	}

	public void setContent(MString content) {
		this.content = content;
	}

	public Integer getColWidth() {
		return colWidth;
	}

	public void setColWidth(Integer colWidth) {
		this.colWidth = colWidth;
	}

	public String getColWidthCssClass() {
		return String.format("col-md-%d", getColWidth());
	}

	@Override
	public Html render() {
		return contentBlock.render(this, null);
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public Html editForm(Form editForm) {		
		return contentBlockEdit.render(this, (Form<ContentBlock>) editForm, Controller.request().getQueryString("backURL"), null, null, null);
	}

}
