/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.cms.models;

import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

import ch.insign.cms.views.html.admin.contentBlockEdit;
import ch.insign.cms.views.html.contentBlock;

import javax.persistence.*;

/**
 * A base content block without any properties.
 * You can extend from this block for all your content blocks that are not pages.
 *
 * FIXME: we should treat all block types the same whereever we can.. maybe this baseBlock is not needed.
 *
 */
@Deprecated
@Entity
@DiscriminatorValue("BaseBlock")
public abstract class BaseBlock extends AbstractBlock  {
	private final static Logger logger = LoggerFactory.getLogger(BaseBlock.class);

	public static BlockFinder<BaseBlock> find = new BlockFinder<>(BaseBlock.class);

	@Override
	public AbstractBlock.BlockType getType() {
		return BlockType.BLOCK;
	}
}
