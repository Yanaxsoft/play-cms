/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.cms.models;

import ch.insign.commons.db.Model;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import play.data.validation.Constraints;
import play.libs.F;

import javax.annotation.Nullable;
import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Entity
@Table(name = "cms_navigation_items",
    indexes = {
        @Index(columnList="vpath"),
        @Index(columnList = "vpathHistory")},
    uniqueConstraints = {@UniqueConstraint(name="vpath-site", columnNames = {"vpath" , "site"})}
)
@NamedQueries(value = {
        @NamedQuery(
                name = "NavigationItem.findByVpath",
                query = "SELECT n FROM NavigationItem n WHERE " +
                        "n.site = :site " +
                        "AND n.vpath = :vpath"),
        @NamedQuery(
                name = "NavigationItem.findByVpathHistory",
                query = "SELECT DISTINCT n FROM NavigationItem n WHERE " +
                        "n.site = :site " +
                        "AND n IN (SELECT n2 FROM NavigationItem n2, IN (n2.vpathHistory) old_vpath WHERE n2.site = :site AND old_vpath LIKE :vpath)"),
        @NamedQuery(
                name = "NavigationItem.findByVpathOrVpathHistory",
                query = "SELECT n FROM NavigationItem n WHERE "
                        + "n.site = :site "
                        + "AND (n.vpath LIKE :vpath "
                        + "  OR n IN (SELECT n2 FROM NavigationItem n2, IN (n2.vpathHistory) old_vpath WHERE n2.site = :site AND old_vpath LIKE :vpath))"),
        @NamedQuery(
                name = "NavigationItem.findByVpathOrVpathHistory.count",
                query = "SELECT COUNT(n) FROM NavigationItem n WHERE "
                        + "n.site = :site "
                        + "AND (n.vpath LIKE :vpath "
                        + "  OR n IN (SELECT n2 FROM NavigationItem n2, IN (n2.vpathHistory) old_vpath WHERE n2.site = :site AND old_vpath LIKE :vpath))"),
        @NamedQuery(
                name = "NavigationItem.findByVpathOrVpathHistoryOnAllSites",
                query = "SELECT n FROM NavigationItem n WHERE "
                        + "n.vpath LIKE :vpath "
                        + "OR n IN (SELECT n2 FROM NavigationItem n2, IN (n2.vpathHistory) old_vpath WHERE old_vpath LIKE :vpath)"),
        @NamedQuery(
                name = "NavigationItem.findByVpathOrVpathHistoryOnAllSites.count",
                query = "SELECT COUNT(n) FROM NavigationItem n WHERE "
                        + "n.vpath LIKE :vpath "
                        + "OR n IN (SELECT n2 FROM NavigationItem n2, IN (n2.vpathHistory) old_vpath WHERE old_vpath LIKE :vpath)")

})
public class NavigationItem extends Model  {
	private final static Logger logger = LoggerFactory.getLogger(NavigationItem.class);

    public static NavigationItemFinder<NavigationItem> find = new NavigationItemFinder<>(NavigationItem.class);

    private String language;

    private boolean visible = false;

    @Constraints.Pattern(value = "^\\/.*", message = "vpath.syntax")
    private String vpath;

    private String site;

    @ElementCollection
    @CollectionTable(name = "cms_vpath_history")
    private List<String> vpathHistory = new ArrayList<>();

    @ManyToOne
    private PageBlock page;


    public class VpathNotValidException extends Exception  {
        private static final long serialVersionUID = 1L;
        public VpathNotValidException() { super(); }
        public VpathNotValidException(String message) { super(message); }
        public VpathNotValidException(String message, Throwable cause) { super(message, cause); }
        public VpathNotValidException(Throwable cause) { super(cause); }
    }

    public class VpathNotAvailableException extends Exception  {
        private static final long serialVersionUID = 1L;
        public VpathNotAvailableException() { super(); }
        public VpathNotAvailableException(String message) { super(message); }
        public VpathNotAvailableException(String message, Throwable cause) { super(message, cause); }
        public VpathNotAvailableException(Throwable cause) { super(cause); }
    }


    /**
     * Get the link to this navigation item.
     * As used in <a href="@getURL">.
     * This is the vpath if set, or the fallback via page/{id}
     *
     * @return the url to this item
     */
    public String getURL() {

        // The page block can override the default handling
        Optional<String> urlFromHander =  Optional.ofNullable(page).flatMap(p -> p.onGetUrl(this));
        if (urlFromHander.isPresent()) {
            return urlFromHander.get();
        }

        if (this.getVirtualPath() != null) {
            return this.getVirtualPath();

        } else {
            return CMS.getConfig().defaultUrlPrefix() + this.getId();
        }
    }

    public PageBlock getPage() {
        return page;
    }

    public void setPage(PageBlock page) {
        this.page = page;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    /**
     * Get the virtual path of this navigation entry.
     */
    @Nullable
    public String getVirtualPath() {
        return vpath;
    }

    public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site;
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    /**
     * Get the list of all previous vpaths that were added to the history.
     */
    public List<String> getVpathHistory() {
        if (vpathHistory == null) {
            vpathHistory = new ArrayList<>();
        }
        return vpathHistory;
    }

    /**
     * Set the vpath. Does validate the input.
     *
     * @param vpath
     * @throws VpathNotAvailableException
     * @throws VpathNotValidException
     */
    public void setVirtualPath(String vpath) throws VpathNotAvailableException, VpathNotValidException {
        checkVirtualPath(vpath);
        this.vpath = vpath;
        // current vpath should not be treated as previous entry
        getVpathHistory().remove(vpath);
    }

    /**
     * Try to set vpath with validation but returning an exception if it occured instead of throwing it.
     *
     * @return Either Left(Exception e) or Right(String vpath)
     */
    public F.Either<Exception, String> trySetVirtualPath(String vpath) {
        try {
            setVirtualPath(vpath);
            return F.Either.Right(vpath);
        } catch (VpathNotAvailableException | VpathNotValidException e) {
            return F.Either.Left(e);
        }
    }

    public String validate() {
        try {
            checkVirtualPath(this.vpath);
        } catch(VpathNotValidException e) {
            return "vpath.syntax";
        } catch(VpathNotAvailableException e) {
            return "vpath.unique";
        }
        return null;
    }

    private void checkVirtualPath(String vpath) throws VpathNotAvailableException, VpathNotValidException {

        if (vpath == null) {
            return;
        }

        if (!vpath.matches("^/[a-zA-Z0-9_/-]*$")) {
            throw new VpathNotValidException("The virtual path " + vpath + " is not valid.");
        }

        // Check if the vpath is alreay used by another nav item
        // Note: If saving from a play form, this method is called when the instance is detached and all fields except id are null
        NavigationItem other = find.byVpath(vpath,
                Optional.ofNullable(getSite()).orElse(CMS.getSites().current().key));

        if (other != null) {
            if (!other.getId().equals(getId())) {
                logger.warn("The vpath '" + vpath + "' is already used by antoher nav item:" + other);
                throw new VpathNotAvailableException("The vpath '" + vpath + "' is already used by " + other);
            }
        }
    }

    /**
     * Add a new entry to the history of previous vpaths for this item
     * @param vpath
     */
    public void addVPathHistoryEntry(String vpath) {
        // don't make duplicated items in vpath history
        if (NavigationItem.find.byVpathHistory(vpath) == null) {
            getVpathHistory().add(vpath);
        }
    }

    public String toString() {
        return String.format(getClass().getSimpleName() + " (url: %s, lang: %s, id: %s)", getURL(), getLanguage(), getId());
    }
}
