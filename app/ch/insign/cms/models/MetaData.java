/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.cms.models;

import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

import ch.insign.commons.db.Model;

import javax.persistence.Column;
import javax.persistence.Entity;
import java.util.Date;

@Entity
public class MetaData extends Model  {
	private final static Logger logger = LoggerFactory.getLogger(MetaData.class);

	@Column
	private Date created;
	
	@Column
	private Date modified;
	
	@Column
	private String createdBy;
	
	@Column
	private String modifiedBy;

	public MetaData() {
		created = new Date();
		createdBy = "Heinz Kreator Sr.";
	}
	
	public void setModified() {
		modified = new Date();
		modifiedBy = "Hans Wurst Jr.";
		
		System.out.println("Object modified by " + modifiedBy + " at " + modified);
		
	}
	
}
