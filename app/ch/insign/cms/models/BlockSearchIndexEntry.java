/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.cms.models;

import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

import ch.insign.commons.db.SearchIndexEntry;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * Deprecated - use ElasticSearch - based fulltext search instead.
 *
 */
@Deprecated
@Entity
@Table(name = "cms_search_index_entry_block")
public class BlockSearchIndexEntry extends SearchIndexEntry  {
	private final static Logger logger = LoggerFactory.getLogger(BlockSearchIndexEntry.class);

    @OneToOne(mappedBy = "searchIndexEntry", orphanRemoval = true)
    private AbstractBlock block;

    public AbstractBlock getBlock() {
        return this.block;
    }

    public void setBlock(AbstractBlock block) {
        this.block = block;
    }

}
