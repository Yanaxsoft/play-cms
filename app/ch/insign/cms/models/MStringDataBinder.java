/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.cms.models;

import ch.insign.commons.db.DataBinding;
import ch.insign.commons.db.MString;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import play.data.validation.ValidationError;
import play.i18n.Messages;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.*;

/**
 * @author Timo Schmid <timo.schmid@gmail.com>
 */
public class MStringDataBinder implements DataBinding.DataBinder {
	private final static Logger logger = LoggerFactory.getLogger(MStringDataBinder.class);

    @Override
    public Object getFromData(String fieldName, Object object, Map<String, String> data) {
        MString mstring = Optional.ofNullable(object).map(o -> (MString) o).orElse(new MString());
        // Retrieve supported frontend languages
        List<String> supportedLanguages = CMS.getConfig().frontendLanguages();
        //Retrieve data by composing of the field name and supported language
        supportedLanguages.stream().forEach(lang -> mstring.set(lang, data.get(fieldName + "." + lang)));

        return mstring;
    }

    @Override
    public Map<String, String> toData(String fieldName, Object mstring) {
        Optional<MString> maybeMString = Optional.ofNullable(mstring).map(o -> (MString) o);
        Map<String,String> data = new HashMap<>();

        if (maybeMString.isPresent()) {
            List<String> supportedLanguages = CMS.getConfig().frontendLanguages();
            // Bind MString fields for each of lang
            supportedLanguages.stream().forEach(lang -> data.put(fieldName + "." + lang, maybeMString.get().get(lang)));
        }

        return data;
    }

    @Override
    public Map<String, List<ValidationError>> validate(String fieldName, Object object, Object formEntityObject,
                                                       Map<String, String> formData) {
        MString mstringObj = (MString)object;
        Map<String,List<ValidationError>> errors = new HashMap<>();

        java.lang.reflect.Field field = getInheritedField(formEntityObject.getClass(), fieldName);
        if (field == null) {
            // no such field name in form object
            return new HashMap<>();
        }

        // check for known constraint annotation
        for(Annotation annotation : field.getDeclaredAnnotations()) {
            if (annotation instanceof MString.MStringRequired) {
                String message = ((MString.MStringRequired) annotation).message();
                List<String> supportedLanguages = CMS.getConfig().frontendLanguages();
                supportedLanguages.stream()
                        .filter(lang -> mstringObj.get(lang).trim().isEmpty())
                        .forEach(lang -> errors.put(fieldName + "." + lang,
                                Collections.singletonList(new ValidationError("errors.required", Messages.get(message)))));
            }

            if (annotation instanceof MString.MStringRequiredForVisibleTab ) {
                String message = ((MString.MStringRequiredForVisibleTab) annotation).message();
                List<String> supportedLanguages = CMS.getConfig().frontendLanguages();
                supportedLanguages.stream()
                        .filter(lang -> mstringObj.get(lang).trim().isEmpty())
                        // Is lang visible in form data
                        .filter(lang -> formData.containsKey("visible." + lang))
                        .forEach(lang -> errors.put(fieldName + "." + lang,
                                Collections.singletonList(new ValidationError("errors.required.visible", Messages.get(message)))));
            }
        }

        return errors;
    }


    /**
     * Access to private inherited fields via reflection
     * http://stackoverflow.com/questions/3567372/access-to-private-inherited-fields-via-reflection-in-java
     *
     * @param type
     * @param fieldName
     * @return
     */
    private Field getInheritedField(Class<?> type, String fieldName) {
        Class<?> i = type;
        while (i != null && i != Object.class) {
            try {
                return i.getDeclaredField(fieldName);
            } catch (NoSuchFieldException e) {
                // skip
            }

            i = i.getSuperclass();
        }

        return null;
    }

}
