/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.cms.models;

import ch.insign.cms.attributeset.AttributeService;
import ch.insign.cms.controllers.routes;
import ch.insign.cms.permissions.BlockAuthorizer;
import ch.insign.cms.permissions.BlockHierarchy;
import ch.insign.cms.permissions.BlockIdentifier;
import ch.insign.cms.permissions.BlockPermission;
import ch.insign.cms.views.admin.utils.BackUrl;
import ch.insign.commons.db.Model;
import ch.insign.commons.i18n.Language;
import ch.insign.playauth.authz.WithAuthorizer;
import ch.insign.playauth.authz.WithHierarchy;
import ch.insign.playauth.authz.WithIdentifier;
import com.fasterxml.jackson.databind.JsonNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import play.api.Play;
import play.api.mvc.Call;
import play.data.Form;
import play.data.format.Formats.DateTime;
import play.data.validation.Constraints.Required;
import play.mvc.Controller;
import play.twirl.api.Html;

import javax.persistence.*;
import java.util.*;

import static ch.insign.playauth.PlayAuth.*;

@Entity
@Table(name = "cms_blocks",
    indexes = {@Index(name = "block_type", columnList = "block_type")},
    uniqueConstraints={@UniqueConstraint(name="key_site", columnNames = {"key" , "site"})}
)
@Inheritance (strategy=InheritanceType.JOINED)
@DiscriminatorColumn(name="block_type")

@NamedQueries(value = {
    @NamedQuery(
        name = "Block.findByKey",
        query = "SELECT b FROM AbstractBlock b WHERE b.key = :key AND (b.site = :site OR b.site = '*')"),
    @NamedQuery(
            name = "Block.findByKeyAndSite",
            query = "SELECT b FROM AbstractBlock b WHERE b.key = :key AND b.site = :site"),
    @NamedQuery(
        name = "Block.findByParentAndSlot",
        query = "SELECT b FROM AbstractBlock b WHERE b.parentBlock = :parentBlock AND b.slot = :slot AND (b.site = :site OR b.site = '*')")
})

@WithIdentifier(BlockIdentifier.class)
@WithHierarchy(BlockHierarchy.class)
@WithAuthorizer(BlockAuthorizer.class)
public abstract class AbstractBlock extends Model  {
	private final static Logger logger = LoggerFactory.getLogger(AbstractBlock.class);

    public static final String CONTEXT_FRONTEND = "frontend";
    public static final String CONTEXT_BACKEND = "backend";

    public enum BlockType {
        PAGE,
        BLOCK
    }

    // NOTE: this is needed to be able to access the DiscriminatorColumn in queries directly.
    // The field gets usually hidden by JPA and is only indirectly accessible using TYPE. (Which expects a class Value)
    @Column(insertable = false, updatable = false)
    private String block_type;

	// Cache some block status (reset when parent is modified)
	@Transient
	private Boolean isRoot;
	@Transient
	private Boolean isTrashed;
	@Transient
	private Boolean isDraft;

    public static BlockFinder<AbstractBlock> find = new BlockFinder<>(AbstractBlock.class);

    private String site = "";

    @Required
    private BlockType type = getType();

    private String name;

    @Column(name="\"KEY\"", unique=false) // unique per site, but no longer whole table
    private String key;

    private String slot;

    @Transient
    private MetaData metaData;

    @ManyToOne(cascade = CascadeType.MERGE)
    private AbstractBlock parentBlock;

    @OneToMany(mappedBy = "parentBlock", cascade = CascadeType.ALL)
    @OrderColumn
    private List<AbstractBlock> subBlocks = new ArrayList<>();

    @DateTime(pattern = "yyyy-MM-dd HH:mm")
    @Temporal(TemporalType.TIMESTAMP)
    private Date displayFrom;

    @DateTime(pattern = "yyyy-MM-dd HH:mm")
    @Temporal(TemporalType.TIMESTAMP)
    private Date displayTo;

    @DateTime(pattern = "yyyy-MM-dd HH:mm")
    @Temporal(TemporalType.TIMESTAMP)
    private Date modified;

    @ManyToMany (mappedBy="inboundLinks", cascade=CascadeType.ALL)
    private List<AbstractBlock> outboundLinks = new ArrayList<>();

    @ManyToMany (cascade=CascadeType.PERSIST)
    @JoinTable(name="cms_page_links")
    private List<AbstractBlock> inboundLinks = new ArrayList<>();

    private String accessRestriction;

    @OneToOne(cascade = { CascadeType.PERSIST, CascadeType.REMOVE }, optional = true)
    private BlockSearchIndexEntry searchIndexEntry;

    @Transient
    private String context;

	@Transient
	protected BlockCache cache;


    public AbstractBlock() {
        metaData = new MetaData();
    }

    /**
     * Add a given block as child
     *
     * @param block
     */
    public void addSubBlock(AbstractBlock block) {
        block.setParentBlock(this);
        getSubBlocks().add(block);
    }

    /**
     * Create and add a sub-block of the given class.
     */
    public AbstractBlock addSubBlock(Class<? extends AbstractBlock> newBlockClass) throws ClassNotFoundException {

        // Instantiate from the selected class
        AbstractBlock newBlock = null;
        try {
            newBlock = newBlockClass.newInstance();

        } catch (Exception e) {
            String msg = "Could not instantiate new block of type: " + newBlockClass.getName();
            logger.error(msg, e);
            throw new ClassNotFoundException(msg);
        }

        addSubBlock(newBlock);
        newBlock.save();

        return newBlock;
    }

    public AbstractBlock addSubBlock(String blockClassName) throws ClassNotFoundException {
        Class<? extends AbstractBlock> newBlockClass = null;
        try {
            //newBlockClass = findBlockClassForName(blockClassName);
            newBlockClass = (Class<? extends AbstractBlock>) Class.forName(blockClassName, true, Play.current().classloader());

        } catch (ClassNotFoundException e) {
            String msg = "Could not find class for new block: " + blockClassName;
            logger.error(msg, e);
            throw new ClassNotFoundException(msg);
        }
        return addSubBlock(newBlockClass);
    }

    /**
     * Get a new (detached) block instance of the class indicated by its full name.
     *
     * @throws ClassNotFoundException
     **/
    public static AbstractBlock newInstanceOf(String blockClassName) throws ClassNotFoundException {
        Class<? extends AbstractBlock> newBlockClass = null;
        try {
            //newBlockClass = findBlockClassForName(blockClassName);
            newBlockClass = (Class<? extends AbstractBlock>) Class.forName(blockClassName, true, Play.current().classloader());

        } catch (ClassNotFoundException e) {
            String msg = "Could not find class for new block: " + blockClassName;
            logger.error(msg, e);
            throw new ClassNotFoundException(msg);
        }

        AbstractBlock newBlock = null;
        try {
            newBlock = newBlockClass.newInstance();

        } catch (Exception e) {
            String msg = "Could not instantiate new block of type: " + newBlockClass.getName();
            logger.error(msg, e);
            throw new ClassNotFoundException(msg);
        }

        return newBlock;
    }

    /**
     * Can be used to set a context tag (eg. "frontend", "leftcol", "backend") to the template
     * which is then used to load the matching configuration, e.g selection of available subblock classes.
     *
     * @param context a context string which is used for configuration
     */
    public void setContext(String context) {
        this.context = context;
    }

    /**
     * Can be used to set a context tag (eg. "frontend", "leftcol", "backend") to the template
     * which is then used to load the matching configuration, e.g selection of available subblock classes.
     *
     * @param context a context string which is used for configuration
     * @return the block
     */
    public AbstractBlock context(String context) {
        this.context = context;
        return this;
    }

    /**
     * Get the (previously set) context tag. It is usually set from the template that adds this block.
     */
    public String getContext() {
        return context;
    }


    /**
     * Move this block to the trash bin, if available.
     * @return
     */
    public boolean moveToTrash() {
        if (!PageBlock.KEY_TRASH.equals(getRoot().getKey())) {

            AbstractBlock trash = find.byKey(PageBlock.KEY_TRASH);
            if (trash != null) {

				markModified();
                if (getParentBlock()!=null) {
                    getParentBlock().getSubBlocks().remove(this);
                }

                setParentBlock(trash);
                trash.getSubBlocks().add(this);
                isTrashed = true;

                logger.info("Moved to trash: " + this);
                return true;

            } else {
                logger.warn("Could not find the trash pageblock (key '{}', site '{}') - deleting block without trash!", PageBlock.KEY_TRASH, CMS.getSites().current().name);
            }

        } else {
            logger.info("Already in the trash: " + this);
        }

        return false;
    }

    /**
     * Is this block in the trash?
     */
    public boolean isTrashed() {
        if (isTrashed == null) {
            if (PageBlock.KEY_TRASH.equals(key)) {
                isTrashed = true;
            } else if (null != getParentBlock()) {
                isTrashed = getParentBlock().isTrashed();
            } else {
                isTrashed = false;
            }
        }
        return isTrashed;
    }

    /**
     * Is this block in the drafts?
     */
    public boolean isDraft() {
        if (isDraft == null) {
            if (PageBlock.KEY_DRAFTS.equals(key)) {
                isDraft = true;
            } else if (null != getParentBlock()) {
                isDraft = getParentBlock().isDraft();
            } else {
                isDraft = false;
            }
        }
        return isDraft;
    }


    /**
     * Get the root block of this block.
     * Note: This should always be an instance of PageBlock or a of a subclass
     *
     * (a block is identified as root if it has the invisible rootParent node set as its parent)
     *
     * @return This block's root block
     */
    public AbstractBlock getRoot() {

        if (isRoot()) {
            return this;
        } else {
            return getParentBlock().getRoot();
        }
    }

    /**
     * Returns true if the block is a root node (has the rootParent as its parent - or a null parent)
     */
    public boolean isRoot() {
        if (isRoot == null) {
            isRoot = (getParentBlock() == null || getParentBlock().equals(find.rootParent()));
        }
        return isRoot;
    }

    /**
     * Get the page block this block belongs to. Returns itself if it's a page.
     * Moves up the tree recursively, returning null if no page block is found.
     */
    public PageBlock getMyPage() {
        if (getType() == BlockType.PAGE) return (PageBlock) this;
        if (isRoot()) return null;
        return getParentBlock().getMyPage();
    }

    // was (but lead to concurrent modification erros on page deletion):
    // @PreRemove
    // public void onBeforeDelete() {

    @Override
    public void delete() {

	    // Ensures parent gets marked as modified (cache eviction, searchindex)
	    markModified();

	    // Detach from parent
        if (parentBlock != null) parentBlock.getSubBlocks().remove(this);
	    setParentBlock(null);

        // Removing in- and outbound links
        for (AbstractBlock inboundLink : getInboundLinks()) {
            inboundLink.getOutboundLinks().remove(this);
            logger.warn("Deleting " + this + " will leave a dead link on page " + inboundLink.getMyPage());
        }
        getInboundLinks().clear();

        for (AbstractBlock outboundLink : getOutboundLinks()) {
            outboundLink.getInboundLinks().remove(this);
        }
        getOutboundLinks().clear();

        super.delete();
        logger.info("Deleted: " + this);
    }

    /**
     * Get the list of outbound link targets.
     * These are site-wide links that are point to another cms page (no external or non-cms lins).
     *
     * Links are extracted from content when persisting through the LinkContentFilter.
     * If you have data that is not filtered, you need to add those links manually on both sides.
     */
    public List<AbstractBlock> getOutboundLinks() {
        return outboundLinks;
    }

    /**
     * Get the list of blocks that contain links to this page.
     * These are site-wide links that are point to another cms page (no external or non-cms lins).
     *
     * Links are extracted from content when persisting through the LinkContentFilter.
     * If you have data that is not filtered, you need to add those links manually on both sides.
     */
    public List<AbstractBlock> getInboundLinks() {
        return inboundLinks;
    }

    /**
     * Remove all links from this block to another page on both sides.
     * Use this after user content edits, before saving to ensure links removed from content are also removed from the db
     * (all existing links will be added (again) when the (filterable) properties are persisted)
     */
    public void removeOutboundLinks() {
        for (AbstractBlock targetPage : getOutboundLinks()) {
            targetPage.getInboundLinks().remove(this);
        }
        outboundLinks.clear();
    }

	public boolean canRead() {
		return canRead(getCurrentParty());
	}

    /**
     * Returns true if the current user is allowed to see this block
     * (permission and time checks are applied).
     * Note: If the user has modify-permissions, he may always see the block.
     */
    public boolean canRead(Object authority) {

        // RNGR-1766 If user can modify block, he should always have possibility to see it,
        // otherwise the block will be invisible and unreachable.
        if (!checkTimeRestriction() && !canModify()) {
            return false;
        }

	    boolean isAllowed = getAuthorizer().isPermitted(
			    authority,
			    getPermissionManager().applyTarget(BlockPermission.READ, this));

        return isAllowed;
    }

    /**
     * Returns true if the currently logged in user has write rights to this block.
     */
	public boolean canModify() {
		return canModify(getCurrentParty());
	}

    /**
     * Returns true if the currently logged in user has write rights to this block.
     */
    public boolean canModify(Object authority) {

	    boolean isAllowed = getAuthorizer().isPermitted(
			    authority,
			    getPermissionManager().applyTarget(BlockPermission.MODIFY, this));

        return isAllowed;
    }


    public abstract BlockType getType();


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    /**
     * Get the site key this block belongs to.
     * "" usually means the default site, and "*" means belongs to all sites.
     */
    public String getSite() {
        return site;
    }

    /**
     * Set the site key this block belongs to.
     * "" usually means the default site, and "*" means belongs to all sites.
     *
     * Make sure to manually update linked entities with the same site value.
     */
    public void setSite(String site) {
        this.site = site;
    }

    public MetaData getMetaData() {
        return metaData;
    }

    public AbstractBlock getParentBlock() {
        return parentBlock;
    }

    /**
     * Sets the parent block.
     * And also inherits the parent's site if no specific one was set before.
     * Don't forget to also add the block to the parent: 'parentBlock.getSubBlocks().add(newBlock)'
     *
     * @param parentBlock
     */
    public void setParentBlock(AbstractBlock parentBlock) {
        this.parentBlock = parentBlock;

        // Reset cached block state flags
        isDraft = null;
        isRoot = null;
        isTrashed = null;

        // Inherit the site from parent, unless we're a root node (those that define sites)
        if (!isRoot()) {
            setSite(parentBlock.getSite());
        }
    }

    public List<AbstractBlock> getSubBlocks() {
        return subBlocks;
    }

    public void setSubBlocks(List<AbstractBlock> subBlocks) {
        this.subBlocks = subBlocks;
    }


    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }


    public String getSlot() {
        return slot;
    }

    public void setSlot(String slot) {
        this.slot = slot;
    }

    public void setDisplayFrom(Date displayFrom) {
        this.displayFrom = displayFrom;
    }

    public void setDisplayTo(Date displayTo) {
        this.displayTo = displayTo;
    }

    public Date getDisplayFrom() {
        return displayFrom;
    }

    public Date getDisplayTo() {
        return displayTo;
    }

    /**
     * Get the last modification date of this block.
     * Note: The modified date is set manually.
     *
     * @return last modification date
     */
    public Date getModified() {
        return modified;
    }

    /**
     * Set the modified date.
     * Note: Set this to mark the object as modified.
     *
     * @param date
     */
    public void setModified(Date date) {
        this.modified = date;
    }

    /**
     * Set the modified date to now and removes cache entries for this block
     * and if it's a block, the parent block as well - until we've reached the containing page.
     *
     * Note: Call this (manually) to mark the object as modified
     */
    public void markModified() {
        setModified(Calendar.getInstance().getTime());
	    cache().invalidate();

        logger.debug("Marking " + this + " modified: " + this.modified);
        if (getType() != BlockType.PAGE && getParentBlock() != null) {
            getParentBlock().markModified();
        }
    }

    public void setAccessRestriction(String accessRestriction) {
        this.accessRestriction = accessRestriction;
    }

    public BlockSearchIndexEntry getSearchIndexEntry() {
        return this.searchIndexEntry;
    }

    public void setSearchIndexEntry(BlockSearchIndexEntry searchIndexEntry) {
        this.searchIndexEntry = searchIndexEntry;
    }

    /**
     * Return the rendered representation of this block.
     * @return
     */
    public abstract Html render();

    /**
     * Returns the edit form html of this entity. The form parameter can either be a fresh
     * object or contain data from the previously submitted form (if it has validation errors)
     *
     * Note: Filters and form securing will be applied by the block controller.
     *
     * @param editForm
     * @return Result showing the edit form
     */
    public abstract Html editForm(Form editForm);


    /**
     * Defines the base context the block is loaded in. Default is either 'backend' (if request path starts with '/admin'),
     * 'frontend' otherwise. Does not modify a previously set context.
     *
     * This is called by the FrontendController before calling render()
     * Note: Templates can refine the context when adding blocks, e.g. 'rightcol' etc.
     */
    public void setBaseContext() {
        if (getContext()!=null) return;

        try {
            if (Controller.request() !=  null && Controller.request().path() != null &&
                    Controller.request().path().startsWith(CMS.getConfig().backendPath())) {
                context(CONTEXT_BACKEND);
            } else {
                context(CONTEXT_FRONTEND);
            }
        } catch (Exception e) {}
    }

    /**
     * Helper method to get the edit-url of the block. Could be used inside a template to render a link.
     * @return
     */
    public Call getEditUrl(){
        return routes.BlockController.edit(getId(), BackUrl.get(), Language.getCurrentLanguage());
    }

    /**
     * Helper method to get the save-url of the block. Could be used inside a template to render a link.
     * @return
     */
    public Call getSaveUrl(Form<?> form){
        String parentId = form.data().get("parentId");
        return routes.BlockController.save(getId(), this.getClass().getName(), parentId, BackUrl.getPrevious());
    }

    /**
     * Get the list of block classes that can be added as sub-blocks to this block.
     * By default, blocks of type page can only have page-subblocks, while block types can only have block types.
     *
     * Override to control what classes are allowed to add.
     * @return list of allowed abstract block subclasses
     */
    public List<Class<? extends AbstractBlock>> getAllowedSubBlocks() {
        //return CMS.getBlockManager().getBlocks(getType());
        return CMS.getBlockManager().getAllowedSubblocks(getType(), getContext());
    }

    public void sortSubBlocks(SortedBlock[] children) {
        for(SortedBlock child : children) {
            AbstractBlock childBlock = AbstractBlock.find.byId(child.id);
            if(childBlock == null) {
                logger.warn("Child block with id " + child.id + " was not found.");
            } else {
                // remove the childBlock from previous parent if it was changed
                if (!this.getId().equals(childBlock.getParentBlock().getId())) {
                    childBlock.getParentBlock().getSubBlocks().remove(childBlock);
                }

                // Remove child Blocks one by one because subBlocks can contain many
                // other type of blocks, which can be not present in navigation list.
                getSubBlocks().remove(childBlock);
                getSubBlocks().add(childBlock);
                childBlock.setParentBlock(AbstractBlock.find.byId(getId()));
                childBlock.sortSubBlocks(child.children);
            }
        }
    }

    public static SortedBlock[] getSortedBlocksFromJsonNode(JsonNode parent) {
        Iterator<JsonNode> itr = parent.elements();
        SortedBlock[] childNodes = new SortedBlock[parent.size()];
        for(int i = 0;itr.hasNext();i++) {
            JsonNode child = itr.next();
            childNodes[i] = new SortedBlock(child.path("id").asText(), getSortedBlocksFromJsonNode(child.path("children")));
        }
        return childNodes;
    }

    public static final class SortedBlock  {
	private final static Logger logger = LoggerFactory.getLogger(SortedBlock.class);
        public final String id;
        public final SortedBlock[] children;
        public SortedBlock(final String id, final SortedBlock[] children) {
            this.id = id;
            this.children = children;
        }
    }

    /**
     * Returns true if at this time no time restrictions apply.
     */
    public boolean checkTimeRestriction() {
        Date now = new Date();
        Date from = getDisplayFrom();
        Date to = getDisplayTo();

        if (from != null && from.after(now)) return false;
        if (to != null && to.before(now)) return false;

        return true;
    }

    @Override
    public void save() {
        super.save();
        // FIXME: Binding attributes in save method relies on HTTP Context which may not exists.
        AttributeService.save(getId());
    }

	/**
	 * Get the default BlockCache instance
	 */
	public BlockCache cache() {
		if (cache == null) cache = new BlockCache(this);
		return cache;
	}

	/**
	 * Get the cached content of the rendered template.
	 * If not cached, calls render() and stores the result in the cache.
	 *
	 * @return rendered html, cached if possible
	 */
	public Html cached() {

		if (cache().useCache()) {

			if (cache().isCached()) {
				logger.debug("cached: Using cached " + cache().getKey());
				return cache().get();
			} else {
				logger.debug("cached: Rendering " + cache().getKey());
				Html html =  render();
				cache().set(html);
				return html;
			}

		} else {
			logger.debug("cached: Not using cache.");
			return render();
		}

		// FIXME: Once EclipseLink understands Java8 ...
//        return cache().getOrElse(
//	        () -> render()
//        );
	}

	public String toString() {
        return String.format(getClass().getSimpleName() + " (id: %s)", getId());
    }
}
