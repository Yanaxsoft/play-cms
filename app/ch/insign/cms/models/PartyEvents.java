/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.cms.models;

import ch.insign.playauth.party.support.DefaultParty;
import play.data.Form;
import play.data.validation.ValidationError;

import java.util.List;

/**
 * Created by anton on 16.10.14.
 */
public interface PartyEvents {

    /**
     * Called when a new user is created.
     * Return list of validation errors.
     */
    public void onCreate(Form form, DefaultParty party);

    /**
     * Called when user is updated.
     * Return list of validation errors.
     */
    public void onUpdate(Form form, DefaultParty party);

    /**
     * Called when user is deleted.
     */
    public void onDelete(DefaultParty party);

    /**
     * Called when user pessword is changed.
     */
    public void onPasswordUpdate(Form form, DefaultParty party);
}
