/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.cms.models;

import ch.insign.cms.CMSApi;
import ch.insign.cms.blocks.jotformpageblock.service.JotFormService;
import ch.insign.cms.service.EmailService;
import ch.insign.commons.filter.FilterManager;
import ch.insign.commons.search.SearchManager;
import ch.insign.playauth.controllers.RouteResolver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Holds various cms configuration objects.
 * Customize your project by subclassing and setting any of these objects.
 *
 * @deprecated inject {@link CMSApi} instead.
 */
@Deprecated
public class CMS  {
	private final static Logger logger = LoggerFactory.getLogger(CMS.class);

    @com.google.inject.Inject
    private static CMSApi cmsApi;

    public static CmsConfiguration getConfig() {
        return cmsApi.getConfig();
    }

    public static Sites getSites() {
        return cmsApi.getSites();
    }

    public static JotFormService getJotFormService() {
        return cmsApi.getJotFormService();
    }

    public static void setConfig(CmsConfiguration configInstance) {
        cmsApi.setConfig(configInstance);
    }

    public static void setRouteResolver(RouteResolver routeResolver) {
        cmsApi.setRouteResolver(routeResolver);
    }

    public static RouteResolver getRouteResolver() {
        return cmsApi.getRouteResolver();
    }

    public static PartyFormManager getPartyFormManager() {
        return cmsApi.getPartyFormManager();
    }

    public static PartyEvents getPartyEvents() {
        return cmsApi.getPartyEvents();
    }

    public static void setPartyEvents(PartyEvents partyEvents) {
        cmsApi.setPartyEvents(partyEvents);
    }

    public static void setPartyFormManager(PartyFormManager partyFormManager) {
        cmsApi.setPartyFormManager(partyFormManager);
    }

    public static BlockManager getBlockManager() {
        return cmsApi.getBlockManager();
    }

    public static FilterManager getFilterManager() {
        return cmsApi.getFilterManager();
    }

    public static SearchManager getSearchManager() {
        return cmsApi.getSearchManager();
    }

    public static UncachedManager getUncachedManager() {
        return cmsApi.getUncachedManager();
    }

    public static EmailService getEmailService() {
        return cmsApi.getEmailService();
    }

    public static void setBlockManager(BlockManager blockManagerInstance) {
        cmsApi.setBlockManager(blockManagerInstance);
    }

    public static void setJetFormSetup(JotFormService jotFormServiceInstance) {
        cmsApi.setJetFormSetup(jotFormServiceInstance);
    }

    public static void registerEmailService(EmailService emailServiceInstance) {
        cmsApi.registerEmailService(emailServiceInstance);
    }
}
