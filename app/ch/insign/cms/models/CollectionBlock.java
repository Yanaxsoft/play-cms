/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.cms.models;

import ch.insign.cms.views.html.collectionBlock;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import play.data.Form;
import play.twirl.api.Html;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.List;

@Entity
@Table(name = "cms_block_collection")
@DiscriminatorValue("CollectionBlock")
//@SecuredEntity(name = "CollectionBlock", actions = { "read", "modify" })
public class CollectionBlock extends AbstractBlock  {
	private final static Logger logger = LoggerFactory.getLogger(CollectionBlock.class);

	public static BlockFinder<CollectionBlock> find = new BlockFinder<>(CollectionBlock.class);

	@Override
	public AbstractBlock.BlockType getType() {
		return AbstractBlock.BlockType.BLOCK;
	}

    @Override
	public Html render() {
		return collectionBlock.render(this);
	}

	@SuppressWarnings("rawtypes")
	@Override
	public Html editForm(Form editForm) {
		return null;
	}

	@Override
	public List<Class<? extends AbstractBlock>> getAllowedSubBlocks() {
		List<Class<? extends AbstractBlock>> allowedSubBlocks = super.getAllowedSubBlocks();

		// CollectionBlock can't be added dynamically, because it has no edit form
		allowedSubBlocks.remove(CollectionBlock.class);
		return allowedSubBlocks;
	}

}
