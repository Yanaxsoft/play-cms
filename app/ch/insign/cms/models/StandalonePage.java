/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.cms.models;

import ch.insign.commons.db.MString;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

import play.i18n.Lang;
import play.i18n.Messages;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * If you have cms and non-cms pages, you can use this class for your non-cms pages
 * to pass values to shared templates.
 *
 */
public class StandalonePage implements Page  {
	private final static Logger logger = LoggerFactory.getLogger(StandalonePage.class);

    protected String key;
    protected String titleKey;
    protected String descriptionKey;
    protected HashMap<String, String> alternateLanguageUrls = new HashMap<>();

    public StandalonePage(String titleKey, String descriptionKey) {
        this.titleKey = titleKey;
        this.descriptionKey = descriptionKey;
    }

    public StandalonePage(String titleKey) {
        this.titleKey = titleKey;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public void setAlternateLanguageUrls(HashMap<String, String> alternateLanguageUrls) {
        this.alternateLanguageUrls = alternateLanguageUrls;
    }

    public HashMap<String, String> getAlternateLanguageUrls(){
        return alternateLanguageUrls;
    }

    public StandalonePage addAlternateLanguageUrl(String lang, String url) {
        alternateLanguageUrls.put(lang, url);
        return this;
    }

    @Override
    public MString getMetaTitle() {
        return fromMessage(titleKey);
    }

    @Override
    public MString getPageTitle() {
       return fromMessage(titleKey);
    }

    @Override
    public MString getPageDescription() {
        return fromMessage(descriptionKey);
    }

    private MString fromMessage(String msgKey) {
        MString ms = new MString();
        if (msgKey != null) {
            for (String lang : CMS.getConfig().frontendLanguages()) {
                String trans = Messages.get(Lang.forCode(lang), msgKey);
                if (!msgKey.equals(trans)) ms.set(lang, trans);
            }
        }
        return ms;
    }

    @Override
    public List<PageBlock> getSubPages() {
        return new ArrayList<PageBlock>();
    }

    @Override
    public String alternateLanguageUrl(String lang) {
        if(alternateLanguageUrls.get(lang) != null) {
            return alternateLanguageUrls.get(lang);

        } else {
            try {
                return PageBlock.find.byKey(PageBlock.KEY_HOMEPAGE).getNavItem(lang).getURL();

            } catch (Exception e) {
                logger.error("alternateLanguageUrl: Error while trying to find homepage with key " + PageBlock.KEY_HOMEPAGE + " in language: " + lang + ". This means your setup is not correct.", e);
                e.printStackTrace();
                return "/"; // return a fail-safe url
            }
        }
    }

    @Override
    public String getKey() {
        return key;
    }
}
