/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.cms.models;

import ch.insign.cms.blocks.backendlinkblock.BackendMenuItem;
import ch.insign.cms.blocks.horizontalcollection.HorizontalCollectionBlock;
import ch.insign.cms.permissions.BlockPermission;
import ch.insign.cms.blocks.groupingblock.GroupingBlock;
import ch.insign.commons.db.MString;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

import ch.insign.commons.i18n.Language;
import ch.insign.playauth.PlayAuth;
import org.apache.commons.lang3.StringUtils;
import play.api.mvc.Call;
import play.twirl.api.Html;
import play.i18n.Messages;
import play.mvc.Controller;
import play.mvc.Http;

import java.util.*;

/**
 * This is a helper class for templates to work with cms objects.
 *
 */
public class Template  {
	private final static Logger logger = LoggerFactory.getLogger(Template.class);

    /**
     * Add a content block to a slot in a template. If the slot was previously filled,
     * the existing block is returned, if the slot is empty, a new block is created and returned.
     *
     * @param blockClass
     * @param parentBlock
     * @param slot
     * @return block
     */
    public static AbstractBlock addBlockToSlot(Class<? extends AbstractBlock> blockClass, AbstractBlock parentBlock, String slot) {
        AbstractBlock newBlock = AbstractBlock.find.byParentAndSlot(parentBlock, slot);

		if (parentBlock == null) {
			throw new RuntimeException("Cannot add new block to slot: Parent is null!");
		}

        if (newBlock == null) {

            try {
				logger.info("Creating a new " + blockClass.getSimpleName() + " for parent " + parentBlock.getId() + " and slot " + slot);
                newBlock = blockClass.newInstance();

            } catch (Exception e) {
                logger.error("Could not instantiate new block of type: " + blockClass.getName(), e);
                e.printStackTrace();
            }
            newBlock.setSlot(slot);
            newBlock.setParentBlock(parentBlock);
            newBlock.setSite(parentBlock.getSite());
            parentBlock.getSubBlocks().add(newBlock);
            newBlock.save();
            parentBlock.save();

        } else {
            logger.debug("Adding existing " + blockClass.getName() + " for parent " + parentBlock.getId() + " and slot " + slot);
        }

        return newBlock;
    }

    /**
     * Add a block by its key. If no block with this key exists, it is created.
     *
     * @param blockClass
     * @param key
     * @return the block
     */
    public static AbstractBlock addBlockByKey(Class<? extends AbstractBlock> blockClass, String key) {
        AbstractBlock newBlock = AbstractBlock.find.byKey(key);

        if (newBlock == null) {
            logger.info("Creating a new " + blockClass.getName() + " with key " + key);

            try {
                newBlock = blockClass.newInstance();

				newBlock.setKey(key);
				newBlock.setSite(CMS.getSites().current().key);
				newBlock.save();

            } catch (Exception e) {
                logger.error("Could not create new block of type: " + blockClass.getName(), e);
                e.printStackTrace();
            }

        } else {
            logger.debug("Adding existing " + blockClass.getName() + " with key " + key);
        }

        return newBlock;
    }

    public static boolean loggedIn() {
        //return true;
        return Controller.session().containsKey("username");
    }

    public static String getUsername() {
        return Controller.session("username");
    }

    public static void setDebugMode(boolean state) {
        Controller.session("debugMode", state ? "true":"false");
    }

    public static boolean isDebugMode() {
        return Controller.session().containsKey("debugMode")
                && Controller.session("debugMode").equals("true");
    }

    /**
     * Get the current language as a 2-character code (i.e. en_US is returned as en)
     */
    public static String getLanguage() {
        return Language.getCurrentLanguage().substring(0, 2);
    }

    /**
     * Add the data as html to the template (tags are then rendered, not escaped)
     * @param input
     * @return html
     */
    public static Html html(String input) {
        if (input == null) {
            input = "";
        }
        return Html.apply(input);
    }

    /**
     * Add the data as html to the template (tags are then rendered, not escaped)
     * @param input
     * @return html
     */
    public static Html html(MString input) {
        if (input == null) {
            return Html.apply("");
        } else {
            return Html.apply(input.get());
        }
    }

    /**
     * Return the first not null, not empty string.
     * Returns "" if none was found.
     */
    public static String nonEmpty(Object... args) {
        for (int i = 0; i < args.length; i++) {
            if (args[i] != null && !args[i].toString().equals("")) {
                return args[i].toString();
            }
        }
        return "";
    }

    /**
     * Returns whether obj is an instance of clazz
     */
    public static boolean isInstanceOf(Object obj, Class clazz) {
        return clazz.isInstance(obj);
    }


    /**
     * Filter a block list and return only those that should be shown
     * to the current user (applying time- and right restrictions
     * and for pages also nav item visible flag checks)
     *
     * Note: If the user has write/modify rights for a block (i.e. an admin),
     * then it is always returned, regardless of time- or read rights or visible flag.
     *
     * @param input the raw input list, typically parent.getSubblocks()
     * @return the filtered list
     */
    public static <T extends AbstractBlock> List<T> filterPermitted(List<T> input) {
        List<T> result = new ArrayList<>();
        for (T block : input) {
            if (block.canRead()) {

	            // For pages, also check if the nav item for the current lang is visible
	            if (block instanceof PageBlock && !(block instanceof GroupingBlock)) {
		            PageBlock page = (PageBlock) block;
		            NavigationItem navItem = page.getNavItem();

		            if (navItem == null || (!navItem.isVisible() && !page.canModify())) {
			            continue;
		            }
	            }

                result.add(block);
            }
        }
        return result;
    }

    /**
     * Time restriction states
     */
    public enum TR {NONE, PAST, PRESENT, FUTURE}


    /**
     * Get the time restriction state of a block.
     */
    public static TR hasTimeRestriction (AbstractBlock block) {

        Date now = new Date();
        Date from = block.getDisplayFrom();
        Date to = block.getDisplayTo();

        if (from == null && to == null) {
            return TR.NONE;
        }
        if (from != null && from.after(now)) {
            return TR.FUTURE;
        }
        if (to != null && to.before(now)) {
            return TR.PAST;
        }
        return TR.PRESENT;
    }

    /**
     * Returns {@code true} if {@code block} is NOT accessible for unauthenticated users
     * and users without {@code AbstractBlock.PERMISSION_READ} permission.
     */
    public static boolean hasAccessRestriction (AbstractBlock block) {
        return PlayAuth.isRestricted(BlockPermission.READ, block);
    }

    /**
     * If a PageBlock has no visible navigation item, the page is considered invisible and false is returned.
     * If at least one nav item is set to visible, returns true.
     * Non-page-items have no visible flag thus are always considered visible.
     */
    public static boolean isVisible(AbstractBlock block) {

        if (block instanceof PageBlock) {
            return ((PageBlock) block).isVisible();
        }
        return true;
    }

    /**
     * Converts any collection of String to a sorted list
     */
    @SuppressWarnings({ "unchecked", "rawtypes" })
    public static <T extends Comparable> List<T> asSortedList(Collection<T> collection) {
        T[] array = collection.toArray((T[]) new Comparable[collection.size()]);
        Arrays.sort(array);
        return Arrays.asList(array);
    }

    /**
     * Check whether this is an internal search index request.
     * Usage: @if(!Template.isIndexer) {some not to be indexed template parts}
     * Use &indexer=true to test output.
     *
     * Warning: Never show sensitive output in such a block, use it to remove unwanted parts only.
     *
     * @return true if the current request is an internal search index request.
     */
    public static boolean isIndexer() {
        try {
            String indexer = Controller.request().getQueryString("indexer");
            return ("true".equals(indexer));

        } catch (Exception e) {
            // If we're inside tests and have no request(), return false
            return false;
        }
    }

    /**
     * Get the localized display name of the passed block class, or the java class name if no translation was found.
     * (Hint: Translate with keys like cms.blocks.myClassName)
     */
    public static String blockDisplayName(Class<? extends AbstractBlock> blockClass) {

        return Messages
                .get("cms.blocks." + blockClass.getSimpleName())
                .replace("cms.blocks.", "");
    }

    /**
     * Add GET parameter(s) to an existing url, it will add them using ? or & depending on the url
     * @param param format is "key=value"
     */
    public static String addGetParm(String url, String... param) {
        return new StringBuilder(url)
                .append(url.contains("?") ? "&" : "?")
                .append(StringUtils.join(param, "&"))
                .toString();
    }

    /**
     * Returns the default edit bar for pages if the page is a cms page (and an empty string otherwise)
     * Can be used in main layout templates to place the page-level edit bar outside / on top of the page template.
     * @param page either a PageBlock (shows the edit bar) or a Page instance (does not show the edit bar since it's no cms page then)
     */
    public static Html editBar(Page page) {
        if (page instanceof AbstractBlock) {
            // Unfortunately play does not generate a render() method for mandatory params only..
            return ch.insign.cms.views.html._blockBase.render((AbstractBlock) page, true, true, true, false, "", "", false, "", Html.apply(""));
        } else {
            return Html.apply("");
        }
    }

    /**
     * Used from _blockBase: Tries to return the block template's class name (via stacktrace).
     * Important: Use this for debugging purpose only (it's an expensive call)
     * @return Class name the method calling the method where this call is placed
     */
    public static String getCallerName() {
        String name = new Exception().getStackTrace()[3].getClassName();
        if (name.equals("ch.insign.cms.views.html._blockBase$")) {
            name = new Exception().getStackTrace()[8].getClassName(); // wild guess
        }
        return name;
    }

	/**
	 * Add an uncached partial to the template. If the template is cached, the
	 * uncached partial will still get rendered on each request.
	 *
	 * @param key the uncached partial key (as registered in CMS.getUncachedManager.register())
	 * @return a [uncached: <key>] tag (which is parsed later on) or the rendered partial if no cache is used
	 */
	public static Html addUncached(String key) {
		if (CMS.getUncachedManager().contains(key)) {
			return Html.apply(CMS.getUncachedManager().getTag(key));

		} else {
			logger.error("Uncached partial not found: " + key);
			return Html.apply("");
		}
	}

	/**
	 * Add an uncached partial to the template. If the template is cached, the
	 * uncached partial will still get rendered on each request.
	 *
	 * @param key the uncached partial key (as registered in CMS.getUncachedManager.register())
	 * @param block the block instance for this template
	 * @return a [uncached: <key>] tag (which is parsed later on) or the rendered partial if no cache is used
	 */
	public static Html addUncached(String key, AbstractBlock block) {
		if (block.cache().useCache()) {
			return addUncached(key);
		} else {
			return CMS.getUncachedManager().render(key);
		}
	}

	/**
	 * Add an uncached partial to the template. If the template is cached, the
	 * uncached partial will still get rendered on each request.
	 *
	 * @param key the uncached partial key (as registered in CMS.getUncachedManager.register())
	 * @param page the Page instance for this template
	 * @return a [uncached: <key>] tag (which is parsed later on) or the rendered partial if no cache is used
	 */
	public static Html addUncached(String key, Page page) {
		if (page instanceof PageBlock) {
			return addUncached(key, (AbstractBlock) page);
		} else {
			return CMS.getUncachedManager().render(key);
		}
	}

	/**
	 * Returns secured route (https in the url)
	 * @param route
	 */
	public static Call getSecuredRoute(Call route) {
		String securedUrl = route.absoluteURL(Http.Context.current.get().request(), true);
		return new Call(route.method(), securedUrl, null);
	}

	/**
	 * Returns unsecured route (http in the url)
	 * @param route
	 */
	public static Call getUnsecuredRoute(Call route) {
		String securedUrl = route.absoluteURL(Http.Context.current.get().request(), false);
		return new Call(route.method(), securedUrl, null);
	}

    /**
     * Return the icon of the pageBlock if it is an instance of BackendMenuItem
     * @param block pageBlock to get icon from
     * @return
     */
    public static String getLinkIconOf(PageBlock block) {
        if (block instanceof BackendMenuItem) {
            String linkIcon = ((BackendMenuItem) block).getLinkIcon();
            return linkIcon != null ? linkIcon : "";
        } else {
            return "";
        }
    }

	/**
	 * Determines if the block is horizontally positioned,
	 * which means is instance of ContentBlock and subblock of HorizontalCollectionBlock
	 * @param block
	 * @return
	 */
	public static Boolean isHorizontallyPositioned(AbstractBlock block) {
		return block instanceof ContentBlock
				&& block.getParentBlock() != null
				&& block.getParentBlock() instanceof HorizontalCollectionBlock;
	}

	/**
	 * If the parent block is a HorizontalCollectionBlock, add the required col width css class
	 * Only applies for ContentBlock
	 * @param block
	 */
	public static String addColWidthClass(AbstractBlock block) {
		return Optional.ofNullable(block)
				.filter(Template::isHorizontallyPositioned)
				.map(b -> ((ContentBlock) b).getColWidthCssClass())
				.orElse("");
	}
}
