/*
 * Copyright 2017 insign gmbh
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.insign.cms.forms;

import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

import play.data.validation.Constraints.Required;

public class MyLogin extends MyIdentity  {
	private final static Logger logger = LoggerFactory.getLogger(MyLogin.class);

    @Required(message = "error.login.password.required")
    public String password;

    public String rememberme;

    @Override
    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public String getRememberme() {
        return rememberme;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setRememberme(String rememberme) {
        this.rememberme = rememberme;
    }
}